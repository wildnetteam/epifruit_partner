//
//  AppDelegate.h
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Reachability.h>
#import "SwiftySideMenuViewController.h"
#import "UIViewController+SwiftySideMenu.h"
#import "LSUtils.h"
#import <CoreLocation/CoreLocation.h>
#import "LocationModel.h"
#import <firebase/Firebase.h>
@class LeftMenuViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate , CLLocationManagerDelegate >

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic ,strong) Reachability * internetReachability;
@property (nonatomic ,strong) Reachability *hostReachable;
@property(nonatomic) UINavigationController *navigationController;
@property(nonatomic) SwiftySideMenuViewController *SwiftySideMenu;
@property(nonatomic) LeftMenuViewController *leftMenu;

@property(nonatomic , strong )FIRDatabaseReference *rootRef ;
@property(nonatomic , strong )FIRDatabaseReference  *newref;
@property(nonatomic , strong ) FIRDatabase *database;
@property(nonatomic , strong ) FIRAuth *FAuth;
@property (strong, nonatomic) NSString *displayName_;
-(void)LoadNotificationCount;

//@property (strong, nonatomic) CLLocationManager *locationManager_;
@property Boolean hasOrientated_;

@end

