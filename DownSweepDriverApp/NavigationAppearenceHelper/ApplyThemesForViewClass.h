//
//  ApplyThemesForViewClass.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ApplyThemesForViewClass : NSObject
//+ (void)applyThemeToSegmentedControl;
//+ (void)applyThemeToStatusBar;
+ (void)applyThemeToNavigationBar;

@end
