//
//  UINavigationItem+BarButtonItemSpacingSupport.h

//

#import <UIKit/UIKit.h>

@interface UINavigationItem (BarButtonItemSpacingSupport)

- (void)addLeftBarButtonItem:(UIBarButtonItem *)leftBarButtonItem;
- (void)addRightBarButtonItem:(UIBarButtonItem *)rightBarButtonItem;
- (void)addLeftBarButtonItemCancel:(UIBarButtonItem *)leftBarButtonItem;

@end
