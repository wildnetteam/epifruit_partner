//
//  NotificationTableViewCell.h
//  DownSweepDriverApp
//
//  Created by Arpana on 06/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelNotification;
@property (weak, nonatomic) IBOutlet UIImageView *notificationIcon;

@end
