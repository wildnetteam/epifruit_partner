//
//  orderHistoryTableViewCell.h
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orderhistory.h"
@protocol MapDelegate <NSObject>

@required
- (void)mapClicked:(UIButton*)mapButton :(int)tableTag;
@optional
- (void)withdrawClicked:(UIButton*)withdrawButton :(int)tableTag;

@end

@interface orderHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTrailingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *pickUpTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverBackGroundHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *transportlabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *contentViewsubview;
@property (weak, nonatomic) IBOutlet UILabel *statuslabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionlabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceTrailingContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vehicletrailingConstraint;
@property (nonatomic, weak) id <MapDelegate> cellDelagte;

@property (weak, nonatomic) IBOutlet UIView *moreView;
@property(nonatomic,weak) IBOutlet UILabel* moreImagesLabel;

@property (weak, nonatomic) IBOutlet UIButton *withDrawButton;

@property (nonatomic) int tTag;

-(void)setOrderDetails:(Orderhistory*)orderObjectAtIndex;

//Used for bid
@property (weak, nonatomic) IBOutlet UILabel *maxPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *minPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *hyphenLabel;
@property (weak, nonatomic) IBOutlet UILabel *jumpPriceLabel;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
-(void)setBidsOrderDetails:(Orderhistory*)orderObjectAtIndex;
@end
