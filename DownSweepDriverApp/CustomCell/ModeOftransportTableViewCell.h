//
//  ModeOftransportTableViewCell.h
//  DownSweepDriverApp
//
//  Created by Arpana on 15/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModeOftransportTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectionlabel;
@property (weak, nonatomic) IBOutlet UITextField *licenseTextField;
@property (nonatomic, strong) NSIndexPath *indexPath;

//Called to the delegate whenever the text in the textfield is changed

@end
