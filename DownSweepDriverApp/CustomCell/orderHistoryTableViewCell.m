//
//  orderHistoryTableViewCell.m
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "orderHistoryTableViewCell.h"
#import "LSUtils.h"
@implementation orderHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    [self.withDrawButton.layer setShadowOffset:CGSizeMake(0, 4)];
    [self.withDrawButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.withDrawButton.layer setShadowOpacity:0.2];
    self.withDrawButton.layer.cornerRadius = self.withDrawButton.frame.size.width/20;
    self.withDrawButton.layer.masksToBounds= true;
}

- (IBAction)mapButtonClicked:(id)sender {
    
    orderHistoryTableViewCell *cell = (orderHistoryTableViewCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(mapClicked: :)]) {
        
        [self.cellDelagte mapClicked:cell.mapButton :_tTag];
    }
}

- (IBAction)withdrawButtonClicked:(id)sender {
    
    orderHistoryTableViewCell *cell = (orderHistoryTableViewCell *)self;
    
    if ([self.cellDelagte respondsToSelector:@selector(withdrawClicked: :)]) {
        
        [self.cellDelagte withdrawClicked:cell.withDrawButton :_tTag];
    }
}

-(void)setOrderDetails:(Orderhistory*)orderObjectAtIndex {
    
    _addressLabel.text =orderObjectAtIndex.pickupAddress;
    _productTitleLabel.text = orderObjectAtIndex.productName;
    _descriptionlabel.text = orderObjectAtIndex.productDesc;
    _distanceLabel.text = [NSString stringWithFormat:@"%@ miles",orderObjectAtIndex.productDistance];
    [_statuslabel setAttributedText:[sharedUtils getOrderStatus:orderObjectAtIndex.orderStatus]];
    _orderNumberLabel.text = [NSString stringWithFormat:@"Order no.%@",orderObjectAtIndex.orderNumber];
    
    self.mapButton.hidden = false;
    if(orderObjectAtIndex.imageCount > 1){
        
        _moreImagesLabel.text = [NSString stringWithFormat:@"+%lu", (unsigned long)orderObjectAtIndex.imageCount -1 ];
        
        _moreView.hidden = false;
    }else
        _moreView.hidden = true;
    
    switch (orderObjectAtIndex.orderStatus) {

        case COMPLETED:
        case REJECTED:
        case CANCELED:
        {
            self.mapButton.hidden = true;
            
        }
            break;
            
        default: self.mapButton.hidden = false;
            
            break;
    }
    
    if(orderObjectAtIndex.jumpPrice){
        _jumpPriceLabel.hidden = false;
    }else
        _jumpPriceLabel.hidden = true;
    
    
    if([orderObjectAtIndex.price isEqualToString:@""] || ![LSUtils checkNilandEmptyString:orderObjectAtIndex.price] || [orderObjectAtIndex.price isEqualToString:@"(null)"]){
        _priceTitleLabel.hidden = true;
        _priceLabel.hidden = true;

    }else{
        _priceLabel.hidden = false;
        _priceTitleLabel.hidden = false;
        _priceLabel.text = [NSString stringWithFormat:@"$%.02f",[orderObjectAtIndex.price floatValue]];

    }
}

-(void)setBidsOrderDetails:(Orderhistory*)orderObjectAtIndex {
    
    _addressLabel.text =orderObjectAtIndex.pickupAddress;
    _productTitleLabel.text = orderObjectAtIndex.productName;
    _priceLabel.text = [NSString stringWithFormat:@"$%@",orderObjectAtIndex.price];
    _orderNumberLabel.text = [NSString stringWithFormat:@"Order no.%@",orderObjectAtIndex.orderNumber];
    _descriptionlabel.text = orderObjectAtIndex.productDesc;
    _distanceLabel.text = [NSString stringWithFormat:@"%@ miles",orderObjectAtIndex.productDistance];
    if(orderObjectAtIndex.productImageArray.count > 1){
        
        _moreImagesLabel.text = [NSString stringWithFormat:@"+%lu", (unsigned long)orderObjectAtIndex.productImageArray.count -1 ];
        
        _moreView.hidden = false;
    }else
        _moreView.hidden = true;
    
    _priceView.hidden = false;
    _maxPriceLabel.hidden = false;
    _minPriceLabel.hidden = false;
    _hyphenLabel.hidden = false;
    _priceTitleLabel.hidden = false;

        if(([orderObjectAtIndex.minRate isEqualToString:@""] && [orderObjectAtIndex.maxrate isEqualToString:@""])){
        //If both values are unspecified
        _priceView.hidden = true;
        _maxPriceLabel.hidden = true;
        _minPriceLabel.hidden = true;
        _hyphenLabel.hidden = true;
        _priceTitleLabel.hidden = true;

    }
    else if((![orderObjectAtIndex.minRate isEqualToString:@""] && ![orderObjectAtIndex.maxrate isEqualToString:@""])){
        
        //If both values are specified
        
        _minPriceLabel.text = [orderObjectAtIndex.minRate isEqualToString:@""]?@"NA":[NSString stringWithFormat:@"$%@",orderObjectAtIndex.minRate];
        _maxPriceLabel.text = [orderObjectAtIndex.maxrate isEqualToString:@""]?@"NA": [NSString stringWithFormat:@"$%@",orderObjectAtIndex.maxrate];
        
    }else{
        //Any one is specified , can be max or min
        _hyphenLabel.hidden = true;
        _minPriceLabel.hidden = true;
        _maxPriceLabel.text = [orderObjectAtIndex.maxrate isEqualToString:@""]?[NSString stringWithFormat:@"$%@",orderObjectAtIndex.minRate]: [NSString stringWithFormat:@"$%@",orderObjectAtIndex.maxrate];
    }
    if(orderObjectAtIndex.imageCount > 1){
        
        _moreImagesLabel.text = [NSString stringWithFormat:@"+%lu", (unsigned long)orderObjectAtIndex.imageCount -1 ];
        _moreView.hidden = false;
    }else
        _moreView.hidden = true;
    
    if(orderObjectAtIndex.jumpPrice){
        _jumpPriceLabel.hidden = false;
       // _contentViewsubview.layer.borderColor = COLOUR_HELPTEXT_GREEN.CGColor;
       // _contentViewsubview.layer.borderWidth = 2.0;
    }else {
        _jumpPriceLabel.hidden = true;
     //   _contentViewsubview.layer.borderColor = [UIColor clearColor].CGColor;
      //  _contentViewsubview.layer.borderWidth = 0.0;

    }
}

@end
