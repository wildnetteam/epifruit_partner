//
//  AppDelegate.m
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "AppDelegate.h"
#import "ApplyThemesForViewClass.h"
#import "UserManager.h"
#import "LoginViewController.h"
#import "MyProposalViewController.h"
#import "AddressViewController.h"
#import "AcceptDeclineJobViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
@import UserNotifications;
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Check if a pathway to a random host exists
    _hostReachable = [Reachability reachabilityWithHostName: @"www.google.com"];
    [_hostReachable startNotifier];
    
    BOOL internetActive =  ([_internetReachability currentReachabilityStatus] != NotReachable);
    BOOL hostActive =  ([_hostReachable currentReachabilityStatus] != NotReachable);
    
    if (internetActive || hostActive)
    {
        [LSUtils updateNetworkStatus:YES];
    }else
    {
        [LSUtils updateNetworkStatus:NO];
    }

    // Use Firebase library to configure APIs
    [FIRApp configure];
    [self authToFirebase];
    //Set up crash analytics
    [Fabric with:@[[Crashlytics class]]];
    [self RegisterNotification];
    [self startLocationUpdates];
    //Set up for notification
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    //Provide key for google place api
    //map related credential
    [GMSServices provideAPIKey:@"AIzaSyBnjdWrasEIojt9La5JdQJOkGwy5U3A904"];
    
    [GMSPlacesClient provideAPIKey:@"AIzaSyBnjdWrasEIojt9La5JdQJOkGwy5U3A904"];

    [sharedUtils getCurrentLocationWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
    }];
    _leftMenu = (LeftMenuViewController*)[sharedUtils.mainStoryboard
                                          instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    LoginViewController *login = (LoginViewController*)[sharedUtils.mainStoryboard
                                                        instantiateViewControllerWithIdentifier: @"LoginViewController"];
    MyProposalViewController *dashboard = (MyProposalViewController*)[sharedUtils.mainStoryboard
                                                                    instantiateViewControllerWithIdentifier:@"MyProposalViewController"];
    
    _SwiftySideMenu = (SwiftySideMenuViewController*)[sharedUtils.mainStoryboard
                                                      instantiateViewControllerWithIdentifier: @"SwiftySideMenuViewController"];
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (localNotif) {
        
         [self application:application didReceiveRemoteNotification:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]];
    } else {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ISLOGGEDIN"] == true) {
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:dashboard];
            
        }
        else
        {
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:login];
            
        }
    }
    _SwiftySideMenu.centerViewController = self.navigationController;
    _SwiftySideMenu.leftViewController = _leftMenu;
    
    self.window.rootViewController = _SwiftySideMenu;
    
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
        
    [ApplyThemesForViewClass applyThemeToNavigationBar];

    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

    // Add any custom logic here.
    NSURL *simpleURL = [NSURL URLWithString:kGoogleMapsScheme];
    NSURL *callbackURL = [NSURL URLWithString:kGoogleMapsCallbackScheme];
    return ([[UIApplication sharedApplication] canOpenURL:simpleURL] ||
            [[UIApplication sharedApplication] canOpenURL:callbackURL]);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)locationManager:(CLLocationManager* )manager didUpdateToLocation:(CLLocation* )newLocation fromLocation:(CLLocation *)oldLocation
{
    sharedUtils.CURRENTLOC= newLocation.coordinate;
    [sharedUtils.locationManager stopUpdatingHeading];
    [sharedUtils.locationManager stopUpdatingLocation];
}

// this function executes once per location update
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *loc = locations[0];
    
    sharedUtils.CURRENTLOC = loc.coordinate;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"ISLOGGEDIN"]){
        
        // if the user has logged in, update firebase with the new location
        LocationModel *newmodel = [[LocationModel alloc]init];
        newmodel.userToken = [UserManager getUserID];
        newmodel.email = [UserManager getEmail];
        newmodel.currentLat = [NSNumber numberWithDouble:loc.coordinate.latitude];
        newmodel.currentLog = [NSNumber numberWithDouble:loc.coordinate.longitude];
        
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
        //Create the date assuming the given string is in GMT
        dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSDictionary *value = @{
                                @"userToken" : [UserManager getUserID],
                                @"currentLat" : [NSNumber numberWithDouble:loc.coordinate.latitude],
                                @"currentLog" : [NSNumber numberWithDouble:loc.coordinate.longitude],
                                @"timestamp" : [dateFormatter stringFromDate:[NSDate date]]
                                };
        FIRDatabaseReference  *newref = [_rootRef child:[UserManager getUserID]];
        [newref updateChildValues:value];
        
    }
}
//  notify firebase that user has logged in
- (void)authToFirebase {
    
    // Note that your Database URL is automatically determined from your GoogleService-Info.plist file so you don't need to specify it.
    _rootRef= [[[FIRDatabase database] reference] child:@"User"];
    _database = [_rootRef database];
    [[FIRAuth auth] signInAnonymouslyWithCompletion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error on login %@", error);
        }else{
            self.displayName_ = [NSString stringWithFormat:@"%@_id%@",user.uid,[UserManager getUserID]];//user.uid;
        [self startLocationUpdates];
        }
   }];
    
}

 // start updating location
 - (void)startLocationUpdates
 {
     // Create the location manager if this object does not
     // already have one.
     if (!sharedUtils.locationManager) {
         sharedUtils.locationManager = [[CLLocationManager alloc] init];
     }
 
     sharedUtils.locationManager.delegate = self;
     sharedUtils.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
 
     // Set a movement threshold for new events.
     sharedUtils.locationManager.distanceFilter = 5; // meters
 
     self.hasOrientated_ = false;
 
     [sharedUtils.locationManager requestWhenInUseAuthorization];
     [sharedUtils.locationManager startUpdatingLocation];
 }
 
 - (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
     if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
         [sharedUtils.locationManager startUpdatingLocation];
     } else if (status == kCLAuthorizationStatusAuthorizedAlways) {
         // iOS 7 will redundantly call this line.
        [sharedUtils.locationManager startUpdatingLocation];
     } else if (status > kCLAuthorizationStatusNotDetermined) {
         NSLog(@"Could not fetch correct authorization status.");
     }
 }
 //
 //// stop updating location
 - (void)stopLocationUpdates
 {
     if (sharedUtils.locationManager) {
         [sharedUtils.locationManager stopUpdatingLocation];
         sharedUtils.locationManager = nil;
     }
 }

- (void)handleNetworkChange:(NSNotification *)note
{
    BOOL internetActive = NO;
    BOOL hostActive = NO;
    // Called after network status changes
    NetworkStatus internetStatus = [_internetReachability currentReachabilityStatus];
    
    switch (internetStatus)
    {
            // Case: No internet
        case NotReachable:
        {
            internetActive = NO;
            //NSLog(@"The internet is down.");
            break;
        }
        case ReachableViaWiFi:
        {
            internetActive = YES;
            //NSLog(@"The internet is working via WIFI.");
            break;
        }
        case ReachableViaWWAN:
        {
            internetActive = YES;
            //NSLog(@"The internet is working via WWAN.");
            break;
        }
    }
    // Check if the host site is online
    NetworkStatus hostStatus = [_hostReachable currentReachabilityStatus];
    //NSLog(@"HostStatus--> %d",hostStatus);
    switch (hostStatus)
    {
        case NotReachable:
        {
            hostActive = NO;
            //NSLog(@"A gateway to the host server is down.");
            break;
        }
        case ReachableViaWiFi:
        {
            hostActive = YES;
            //NSLog(@"A gateway to the host server is working via WIFI.");
            break;
        }
        case ReachableViaWWAN:
        {
            hostActive = YES;
            //NSLog(@"A gateway to the host server is working via WWAN.");
            break;
        }
    }
    
    if (internetActive && hostActive)
    {
        [LSUtils updateNetworkStatus:YES];
    }
    else
    {
        [LSUtils updateNetworkStatus:NO];
    }
}

#pragma mark- Notification Delegates

-(void)RegisterNotification{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound +UNAuthorizationOptionBadge;
    [center requestAuthorizationWithOptions:options
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!granted) {
                                  NSLog(@"Something went wrong");
                              }
                          }];
    
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", deviceToken);
    NSString * token = [[[[deviceToken description]
                          stringByReplacingOccurrencesOfString: @"<" withString: @""]
                         stringByReplacingOccurrencesOfString: @">" withString: @""]
                        stringByReplacingOccurrencesOfString: @" " withString: @""];
    [UserManager saveUserCredentials:token withvar:PushToken];
    NSLog(@"The generated device token string is : %@",token);
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    
}

bool isNotification = false;

- (void)application:(UIApplication* )application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSLog(@"Notification:%@",userInfo);
    
    if ([UserManager getNotificationCount]){
        int k=[[UserManager getNotificationCount] intValue]+1;
        
        [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",k]];
        [UIApplication sharedApplication].applicationIconBadgeNumber =[[UserManager getNotificationCount] integerValue];
    } else{
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",1]];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NSdidGetUnReadNotificationCount object:nil];
 
}

-(void)LoadNotificationCount{
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"ISLOGGEDIN"]){
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@",getNotificationCount_Service,[UserManager getUserID]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                       options:0
                                                                                                         error:NULL];
                                              if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  if (![[Response objectForKey:@"count"] isEqualToString:@"0"]) {
                                                      [UserManager saveNotificationCount:[Response  objectForKey:@"count"]];
                                                      [UIApplication sharedApplication].applicationIconBadgeNumber =[[UserManager getNotificationCount] integerValue];
                                                  }
                                                  else{
                                                      [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",0]];
                                                      [UIApplication sharedApplication].applicationIconBadgeNumber =0;
                                                  }
                                              }
                                              [[NSNotificationCenter defaultCenter] postNotificationName:NSdidGetUnReadNotificationCount object:nil];
                                          }
                                          else{
                                          }
                                          });
                                      }];
        [task resume];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [self LoadNotificationCount];
    [[NSNotificationCenter defaultCenter] postNotificationName:NSdidRecieveNotification object:nil];
    
}



@end
