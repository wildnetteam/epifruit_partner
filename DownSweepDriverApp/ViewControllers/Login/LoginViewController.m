//
//  LoginViewController.m
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "MyProposalViewController.h"
#import "UserManager.h"
#import "ForgotpasswordViewController.h"
#import "NotificationManager.h"
#import "WaitingViewController.h"
#import <Firebase.h>

@interface LoginViewController ()
@end

@implementation LoginViewController
{
    UIImage *downloadedImage;
    NSString* profileWithBaseString;
    BOOL isPasswordVisible;
    __weak IBOutlet UIButton *emailButton;
    __weak IBOutlet UIButton *passwordButton;
    AppDelegate *del;

}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIElements];
    self.view.userInteractionEnabled = true;
    
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.navigationItem.leftBarButtonItem  = nil;
    self.username.text=@"";
    self.password.text=@"";
    [self placeholderSize:_username :@"Enter email or mobile number..."];
    [self placeholderSize:_password:@"Enter your password..."];
    sharedUtils.locationManager.delegate = del;
    [self setNeedsStatusBarAppearanceUpdate];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUIElements {
    
    self.title=@"";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    
    [_signUpButton setBackgroundColor:REGISTERBUTTON_BACKGROUND_COLOR];
    _signUpButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0);
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_username];
    [LSUtils addPaddingToTextField:_password];
    
    [_loginButton.layer setShadowOffset:CGSizeMake(0, 4)];
    [_loginButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_loginButton.layer setShadowOpacity:0.1];
    del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //Change Clear Button Image
    
    if (!IS_IPHONE5) {
        self.EmailTextFieldAndPasswordLabelSpace.constant = 30;
        self.LoginButtonAndDontHaveLableSpace.constant = -20;
        self.TopImageViewAndEmailLabelSpace.constant = -50;
        self.TopLayoutAndTopImagaeViewSpace.constant = -20;
        self.textfieldForgotPassSpace.constant = 7;
        self.forgotPassLoginButtonSpace.constant = -28;
    }
}

//Function to make placeholder color and font different from textfield font and color
-(void)placeholderSize:(UITextField*)textfield :(NSString*)text{
    
    UIColor *color = [UIColor whiteColor];
    textfield.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Light" size:15.0]
                                                 }
     ];
}

- (IBAction)loginButtonTapped:(id)sender {
    
    [[self view] endEditing:true];

    if([self validateLoginCredentials]) {

        NSString *params ;
        if([LSUtils isNetworkConnected])
        {
            if([UserManager getDeviceToken]!=nil){
                
                params = [NSString stringWithFormat:@"%@&email_id=%@&password=%@&device_type=2&device_token=%@",Login_Service,_username.text,_password.text,[UserManager getDeviceToken]];
            }else
                params = [NSString stringWithFormat:@"%@&email_id=%@&password=%@&device_type=2",Login_Service,_username.text,_password.text];
            
            [sharedUtils  startAnimator:self];
            _loginButton.enabled = false;
            NSURL *url = [NSURL URLWithString:Base_URL];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:30.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [sharedUtils stopAnimator:self];

                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                        options:0
                                                                                                                          error:NULL];
                                                      _loginButton.enabled = true;
                                                      
                                                      UIAlertController *controller;
                                                      if(responseDictionary ==nil){
                                                          
                                                          controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      }
                                                      else{
                                                          if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                              
                                                              NSDictionary *arrayOfdata;
                                                              NSArray   *details = [responseDictionary valueForKey:@"data"];
                                                              arrayOfdata = [details objectAtIndex:0];
                                                              
                                                              // Clear the credentials if any from "UserManager" class
                                                              [UserManager removeUserCredential];
                                                              
                                                              [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"id"] withvar:k_LoggedInUserID];
                                                              [UserManager saveUserCredentials:@"" withvar:k_LoginTokenID];
                                                              [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"deliveryboy_name"] withvar:k_UserName];
                                                              [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"deliveryboy_email"] withvar:k_UserEmail];
                                                              [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"deliveryboy_mobile"] withvar:k_UserPhoneNumber];
                                                              [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"deliveryboy_licenceno"] withvar:k_LiscenseNo];
                                                              [UserManager saveUserCredentials:[arrayOfdata valueForKey:@"Refrence_code"] withvar:k_UserReferenceCode];
                                                              if([LSUtils checkNilandEmptyString:[arrayOfdata valueForKey:@"deliveryboy_image"]]){
                                                                  
                                                                  [UserManager saveUserCredentials:[NSString stringWithFormat:@"%@/%@",[responseDictionary valueForKey:@"deliveryboy_imageurl"], [arrayOfdata valueForKey:@"deliveryboy_image"]] withvar:k_UserImageURL];
                                                              }
                                                              
                                                              [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"ISLOGGEDIN"];
                                                              [(AppDelegate*)[[UIApplication sharedApplication] delegate] LoadNotificationCount];
                                                              [self updateLocationOnFirebase];
                                                              MyProposalViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"MyProposalViewController"];
                                                              
                                                              vc.swiftySideMenu.centerViewController = self.navigationController;
                                                              
                                                              [self.navigationController pushViewController:vc animated:YES];
                                                              
                                                              controller = [sharedUtils showAlert:@"Successfull" withMessage:@"You have logged in successfully."];
                                                              
                                                          }
                                                          else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"205"]) {
                                                              
                                                              WaitingViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"WaitingViewController"];
                                                              
                                                              vc.swiftySideMenu.centerViewController = self.navigationController;
                                                              
                                                              [self.navigationController pushViewController:vc animated:YES];
                                                              
                                                          }
                                                          else {
                                                              controller = [sharedUtils showAlert:@"Sorry" withMessage:[responseDictionary objectForKey:@"message"]];
                                                          }
                                                      }
                                                      if(controller!=nil)
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  
                                                 else if(error!=nil){
                                                    UIAlertController*    controller = [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              });
                                          }];
            [task resume];
            
        }else{
            
            [sharedUtils  stopAnimator:self];

            UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

- (void)updateLocationOnFirebase
{
        [sharedUtils getCurrentLocationWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error!=nil){

                }
                if(success){
                    //loction fetched
                    NSDateFormatter *dateFormatter = [NSDateFormatter new];
                    [dateFormatter setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
                    //Create the date assuming the given string is in GMT
                    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
                    
                    NSDictionary *value = @{
                                            @"userToken" : [UserManager getUserID],
                                            @"currentLat" : [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.latitude],
                                            @"currentLog" : [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.longitude],
                                            @"timestamp" : [dateFormatter stringFromDate:[NSDate date]]
                                            };
                    
                    if([[NSUserDefaults standardUserDefaults] boolForKey:@"ISLOGGEDIN"]){
                        
                        FIRDatabaseReference  *newref = [del.rootRef child:[UserManager getUserID]];
                        [newref setValue:value];
                    }
                }
            });
            
        }];
}

- (IBAction)signUpButton:(id)sender {
    
    RegistrationViewController *regVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"RegistrationViewController"];
    [self.navigationController pushViewController:regVC animated:YES];
}

- (IBAction)forgotPasswordButtonClicked:(id)sender {
    
    ForgotpasswordViewController *forgotPasswordVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"ForgotpasswordViewController"];
    forgotPasswordVC.swiftySideMenu.centerViewController = self.navigationController;
    [self.navigationController pushViewController:forgotPasswordVC animated:YES];
}

#pragma mark - Validate Login credential

-(BOOL)validateLoginCredentials{
    
    if(![LSUtils checkNilandEmptyString:_username.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"User name or mobile number must be filled." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_password.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Password can not be blank." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        return false;
    }
    NSCharacterSet* validCharecterSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    
    if(  [_username.text rangeOfCharacterFromSet:validCharecterSet].location != NSNotFound){
       //check for email validateion
        
        if(![LSUtils validateEmail:_username.text]){
            
            [[NotificationManager notificationManager] displayMessage:@"Email id is wrong." withType:Negative];
            return false;
            
        }
        if([LSUtils validateEmail:_username.text]){
            NSArray *Array = [_username.text componentsSeparatedByString:@"@"];
            if (Array.count>0) {
                
                NSString *localPartEmail = [Array firstObject];
                if(localPartEmail.length<3 || localPartEmail.length >= 28){
                    
                    [[NotificationManager notificationManager] displayMessage:@"Email id length must be range in 3 to 28." withType:Negative];
                    return false;
                }
            }
        }
        return true;
    }
    if(_password.text.length<6 || _password.text.length >=16){
        
        [[NotificationManager notificationManager] displayMessage:@"Password must be range in 6 to 16." withType:Negative];
        return false;
    }
    return true;
}

#pragma mark - UITextField Delegate Methods

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField ==_username){
        
        [_password becomeFirstResponder];
    }else if (textField == _password){
        
        [textField resignFirstResponder];
        [_loginButton becomeFirstResponder];
        
    }
    return true;
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Animate text field
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_username) {
        
        _username.placeholder = @"";
        emailButton.enabled = true;
        
        [self.emailImageView setImage:[UIImage imageNamed:@"cross_white"]];
        
        _emailImageView.contentMode = UIViewContentModeCenter;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - _username.frame.size.height +20, self.view.frame.size.width, self.view.frame.size.height);
        }];
        textField.returnKeyType = UIReturnKeyNext;
    } else if (textField==_password)
    {
        _password.placeholder = @"";
        passwordButton.enabled = true;
        
        [self.passwordImageView setImage:[UIImage imageNamed:@"cross_white"]];
        _passwordImageView.contentMode = UIViewContentModeCenter;
        
        
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -2.5*_username.frame.size.height + 64, self.view.frame.size.width, self.view.frame.size.height);
        }];
        textField.returnKeyType = UIReturnKeyDone;
 
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_username)
    {
        if(!([_username.text length]>0))
        {
            [self placeholderSize:_username :@"Enter email or mobile number..."];
        }
        [self.emailImageView setImage:[UIImage imageNamed:@"email_white"]];
        emailButton.enabled = false;

    }
    
    if (textField==_password)
    {
        if(!([_password.text length]>0))
        {
            [self placeholderSize:_password :@"Enter your password..."];
            
        }
        [self.passwordImageView setImage:[UIImage imageNamed:@"password_white"]];
        passwordButton.enabled = false;
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    if(textField == self.username)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
        }
        if (([self.username.text length] == 1) && [string isEqualToString:@""]) {
        }
    }
    if(textField == self.password)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
        }
        
        if (([self.password.text length] == 1) && [string isEqualToString:@""]) {
        }
        
    }
    return true;
}

- (IBAction)emailTextClear:(id)sender {
    
    _username.text =@"";
}
- (IBAction)passwordTextClear:(id)sender {
    
    _password.text = @"";
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}

-(void)dealloc{
    
}
@end
