//
//  ReferralViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 02/08/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "ReferralViewController.h"

@interface ReferralViewController ()
{
    CAShapeLayer  *border;
}
@end

@implementation ReferralViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
  
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    _referralCodeLabel.text = [LSUtils checkNilandEmptyString:[UserManager getUserReferenceCode]]?[UserManager getUserReferenceCode]: @"Not Available";
    
    border = [CAShapeLayer layer];
    border.strokeColor = COLOR_Border.CGColor;//[UIColor colorWithRed:67/255.0f green:37/255.0f blue:83/255.0f alpha:1].CGColor;
    border.fillColor = nil;
    border.lineDashPattern = @[@4, @2];
    [self.referralCodeLabelView.layer addSublayer:border];

}

-(void)viewDidLayoutSubviews{
    
    border.path = [UIBezierPath bezierPathWithRect:self.referralCodeLabelView.bounds].CGPath;
    border.frame = self.referralCodeLabelView.bounds;
}

- (IBAction)cpButtonPressed:(id)sender{
    
    NSString *texttoshare = _referralCodeLabel.text; //this is your text string to share
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

@end
