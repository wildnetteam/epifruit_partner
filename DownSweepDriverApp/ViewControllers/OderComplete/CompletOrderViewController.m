//
//  CompletOrderViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 01/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "CompletOrderViewController.h"
#import "HomeViewController.h"
@interface CompletOrderViewController ()

@end

@implementation CompletOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpUIElements{
    
    self.title =@"";
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}
-(void)backButtonClicked:(id)sender{
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (HomeViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[HomeViewController class]]) {
            aViewController.isFromNotificationScreen = true;
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
}

- (IBAction)okButtonPressed:(id)sender {
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[HomeViewController class]]) {
            CATransition *animation = [CATransition animation];
            [animation setType:kCATransitionMoveIn];
            [animation setDuration:.5];
            [animation setSubtype:kCATransitionFromBottom];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            HomeViewController* popToController = (HomeViewController*)aViewController;popToController.isFromNotificationScreen = true;
            [[aViewController.view layer] addAnimation:animation forKey:nil];
            [self.navigationController popToViewController:aViewController animated:YES];
        }
    }
}
@end
