//
//  CompletedListViewController.h 
//  DownSweepDriverApp
//
//  Created by Arpana on 22/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLWeeklyCalendarView.h"
#import "orderHistoryTableViewCell.h"

@interface CompletedListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate ,MapDelegate >
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageForCalender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *completedOrderlabel;
@property (weak, nonatomic) IBOutlet UIView *calenderBaseView;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIView *orderCompletedView;
@property (nonatomic, strong) CLWeeklyCalendarView* calendarView;
@property(nonatomic)int priviousScreen;
@end
