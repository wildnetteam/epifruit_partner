//
//  CompletedListViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 22/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "CompletedListViewController.h"
#import "LSUtils.h"
#import "AddressViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OrderDetailViewController.h"
#import "UIView+XIB.h"
#import "Orderhistory.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+CL.h"
@interface CompletedListViewController ()
{
    NSString *todaysDateString;
    UIButton *dimView;
    NSMutableArray *orderArray;
    NSString* imageBaseURL;
    NSString* selectedDate;
    UIRefreshControl *btmefreshControl;
}
@end

@implementation CompletedListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    _completedOrderlabel.text = [NSString stringWithFormat:@"%d", (int)orderArray.count ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)checkLocationPermission{
    
    if([LSUtils checkIfLocationServicesAreEnabledOrNot]){
        
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""  message:@"Please enable location service from settings."preferredStyle:    UIAlertControllerStyleAlert];
        
        UIAlertAction *closeAction = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"Ok", @"")
                                      style:UIAlertActionStyleCancel
                                      handler:^(UIAlertAction *action)
                                      {
                                          
                                      }];
        
        [alertController addAction:closeAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    if(_priviousScreen == 1){
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    }else{
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
        
    }
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;

    _backGroundImageForCalender.image = [UIImage calendarBackgroundImage:CGRectGetHeight(_backGroundImageForCalender.frame)];
    [self.calenderBaseView addSubview:self.calendarView];
    _calendarView.delegate = (id)self;

    btmefreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview: btmefreshControl];
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    self.tableView.alwaysBounceVertical = YES;
    
}

-(void)refresh:(id)sender{
    
    if([LSUtils isNetworkConnected]){
        [self getAllOrders:selectedDate];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        } ];
    }
}
//Initialize
-(CLWeeklyCalendarView *)calendarView
{
    if(!_calendarView){
        _calendarView = [[CLWeeklyCalendarView alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width-20, _calenderBaseView.frame.size.height)];
        _calendarView.delegate = (id)self;
    }
    return _calendarView;
}

- (IBAction)mapButtonClicked:(id)sender {
    AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
    [self presentViewController:vc animated:true completion:nil];
}
-(void)calculateCostAndOrderNumber{
    
    _completedOrderlabel.text = [NSString stringWithFormat:@"%d", (int)orderArray.count ];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(btmefreshControl.isRefreshing == true){
        [btmefreshControl endRefreshing ];
    }
    
    orderHistoryTableViewCell *cell = (orderHistoryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifierForOrder"];

    //  dispatch_async(dispatch_get_main_queue(), ^{
    
    if(orderArray.count > 0){
        Orderhistory* orderObjectAtIndex = [orderArray objectAtIndex:indexPath.row];
        [cell setOrderDetails:orderObjectAtIndex];
        cell.pickUpTimeLabel.text =[self getFormatedDate:orderObjectAtIndex.pickupTime];
        cell.mapButton.tag = indexPath.row;
        // Here we use the new provided sd_setImageWithURL: method to load the web image
        [cell.activityIndicator startAnimating];
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",orderObjectAtIndex.imageBaseURL, orderObjectAtIndex.randomImageString ]]  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        [cell.activityIndicator stopAnimating];
        cell.productImageView.layer.cornerRadius = 10;
        cell.productImageView.layer.masksToBounds = true;
        cell.cellDelagte = (id)self;
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
        [cell updateConstraintsIfNeeded];
    }
    return cell;
}

- (void)mapClicked:(UIButton*)mapButton :(int)tableTag{
    
    Orderhistory* orderObjectAtIndex = [orderArray objectAtIndex:mapButton.tag];
    AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
    vc.orderObj = orderObjectAtIndex;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  orderArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Orderhistory*  orderObjectAtIndex  = [orderArray objectAtIndex:indexPath.row];
    OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
    vc.orderID = orderObjectAtIndex.productId;
    vc.orderStatus = orderObjectAtIndex.orderStatus;
    vc.swiftySideMenu.centerViewController = self.navigationController;
    [self.navigationController pushViewController:vc animated:YES];

}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"%@ %@",onlyDate,[df stringFromDate:date]];
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:true];
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)getAllOrders :(NSString*)dateString{
    
    if([LSUtils isNetworkConnected]){
        
        orderArray=nil;
        orderArray = [[NSMutableArray alloc]init];
        NSURL *url = [NSURL URLWithString:Base_URL];//"2016-11-21
        [sharedUtils startAnimator:self];
        
        NSString *params = [NSString stringWithFormat:@"%@&driver_id=%@&orderDate=%@",GetCompletdOrders,[UserManager getUserID],selectedDate];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];

                                              if (data.length > 0 && error == nil)
                                                  
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  if(responseDict == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      
                                                      NSMutableArray *arrayOfDict;
                                                      arrayOfDict = [responseDict valueForKey:@"data"];
                                                      
                                                      for(int index =0 ; index<arrayOfDict.count ; index++){
                                                          
                                                          NSDictionary * orderDictionary= [arrayOfDict objectAtIndex:index];
                                                          
                                                          //Set OrderHistory Model
                                                          
                                                          Orderhistory *model = [[Orderhistory alloc]init];
                                                          model.pickupAddress = [orderDictionary objectForKey:@"pickup_address"];
                                                          model.productName = [orderDictionary objectForKey:@"product_title"];
                                                          model.jumpPrice = [[orderDictionary objectForKey:@"jump_price"] intValue];
                                                          model.price = [orderDictionary objectForKey:@"accepted_final_order_price"] ;//accepted_order_price
                                                          model.pickupTime = [orderDictionary objectForKey:@"created_on"];
                                                          model.pickUpCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"pi_lat"], @"pi_lat",[orderDictionary valueForKey:@"pi_long"], @"pi_long",nil];
                                                          model.dropOffCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"dr_lat"], @"dr_lat",[orderDictionary valueForKey:@"dr_long"], @"dr_long",nil];
                                                          model.imageCount = [[orderDictionary objectForKey:@"image_count"] intValue];
                                                          model.randomImageString = [orderDictionary objectForKey:@"product_image"];
                                                          model.imageBaseURL = [responseDict valueForKey:@"imagepath"];
                                                          model.productId = [orderDictionary objectForKey:@"id"];
                                                          model.orderStatus = [[orderDictionary objectForKey:@"order_status"] intValue];
                                                          model.productDistance = [orderDictionary objectForKey:@"distance"];
                                                          model.orderNumber = [orderDictionary objectForKey:@"order_number"];

                                                          model.productDesc = [NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_quantity"]]?[orderDictionary objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_type"]]?[orderDictionary objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]?[orderDictionary objectForKey:@"product_weight"]:@"" ];
                                                          
                                                          if([ model.productDesc containsString:@"( )"]||![LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]){
                                                              model.productDesc = [[model.productDesc stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                          }
                                                          [orderArray addObject:model] ;
                                                          model=nil;
                                                      }
                                                      _completedOrderlabel.text = [NSString stringWithFormat:@"%d", (int)orderArray.count ];

                                                  }else {
                                                      UIViewController *controller =  [sharedUtils showAlert:@"" withMessage:[responseDict objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:true completion:nil];
                                                  }
                                                  
                                              }
                                              else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }
                                              if(btmefreshControl.isRefreshing == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                              [_tableView setContentOffset:CGPointZero animated:YES];
                                              [_tableView reloadData];

                                          });
                                      }];
        [task resume];
    }
    else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

//After getting data callback
-(void)dailyCalendarViewDidSelect:(NSDate *)date {
    
    [self getNeededDateFormat:date];
    [self getAllOrders:selectedDate];
}

-(NSString*)getNeededDateFormat :(NSDate*)dt{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:@"yyyy-MM-dd"];
    selectedDate=  [df stringFromDate:dt];
    return selectedDate;
}
- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}
@end


