//
//  NotificationVC.h
//  Dropwash
//
//  Created by Yogita on 19/07/16.
//  Copyright © 2016 Mobrill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    UIView *activityView;
    UIActivityIndicatorView *activity;
    BOOL lastPageBool;
}

@property (retain, nonatomic)id parentObject;
@property (nonatomic,strong) NSMutableArray *arrResponse;
@property (nonatomic,strong) NSArray *arrOrderStatus;
@property (nonatomic,strong) NSArray *arrOrderStatusDetail;
@property (nonatomic, strong) UIRefreshControl *btmefreshControl;

@property (weak, nonatomic) IBOutlet UITableView *tableNotification;

@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;


@end
