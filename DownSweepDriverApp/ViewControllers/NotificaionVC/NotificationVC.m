//
//  NotificationVC.m
//  Dropwash
//
//  Created by Yogita on 19/07/16.
//  Copyright © 2016 Mobrill. All rights reserved.
//

#import "NotificationVC.h"
#import "Constant.h"
#import "LSUtils.h"
#import "NotificationTableViewCell.h"
#import "AcceptDeclineJobViewController.h"
#import "OrderDetailViewController.h"
@implementation NotificationVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpUIElementsValues];

}


-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];

    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;

    _tableNotification.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    _tableNotification.rowHeight = UITableViewAutomaticDimension;
    _tableNotification.estimatedRowHeight = 100.0;
   
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    _currentPage= 0;
    lastPageBool=NO;
    
    [self getNotificationListWithCompletion:^(BOOL success) {
        
        if(_arrResponse.count > 0){
            
            UIButton* editButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [editButton setFrame:CGRectMake(0, 0, 100, 50)];
            [editButton addTarget:self action:@selector(clearList:) forControlEvents:UIControlEventTouchUpInside];
            [editButton setTitle:@"Clear all" forState:UIControlStateNormal];
            [editButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Semibold" size:15]];
            [editButton setTitleColor:AppDefaultBlueColor forState:UIControlStateNormal];
            
            UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            
            negativeSpacer.width = -25;
            
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:editButton] , nil]];
            
            [_tableNotification reloadData];
        }
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];

    if (_arrResponse.count>0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableNotification scrollToRowAtIndexPath:indexPath
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - IBAction

-(void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrResponse.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CellIdentifier";
    
    NotificationTableViewCell *cell = (NotificationTableViewCell*)[self.tableNotification dequeueReusableCellWithIdentifier:cellIdentifier];

         cell.labelNotification.text = [[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification"];
    
    int notType = [[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue];
    
    NSString* message;
    
    switch (notType) {
            
        case 0:
            message = [[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_status"] intValue] == 6 ?@"Order cancelled by retailer!":@"New order arrived!";
            break;
            
        case 1: {
            
          message =  [[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_status"] intValue] == 3 ? @"Order Completed!":@"Order Accepted!";
        }
            
            break;
        case 2:  message = @"Order Cancelled!";
            break;
    }
    
    cell.labelTitle.text = message;
    
    if ([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"isread"] isEqualToString:@"0"]) {
        cell.notificationIcon.image=[UIImage imageNamed:@"Mail_notif"];//open_mail
    }else{
        cell.notificationIcon.image=[UIImage imageNamed:@"open_mail"];
    }
    [cell layoutIfNeeded];
    [cell setNeedsLayout];
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
    
    //Create the date assuming the given string is in GMT
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [df dateFromString:[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"created_on"]];
    
    //Create a date string in the local timezone
    [df setDateFormat:SHOW_YEAR_MONTH_DATE_FORMAT];
    NSString *strServerDate=[df stringFromDate:date];
    [df setDateFormat:SHOW_YEAR_MONTH_DATE_FORMAT];
    NSString *strCurrentDate=[df stringFromDate:[NSDate date]];
    
    if ([strCurrentDate isEqualToString:strServerDate]) {
        NSTimeZone* CurrentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone* SystemTimeZone = [NSTimeZone systemTimeZone];
        NSInteger currentGMTOffset = [CurrentTimeZone secondsFromGMTForDate:date];
        NSInteger SystemGMTOffset = [SystemTimeZone secondsFromGMTForDate:date];
        NSTimeInterval interval = SystemGMTOffset - currentGMTOffset;
        NSDate* TodayDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:date];
        
        [df setDateFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    cell.labelTime.text=[[df stringFromDate:TodayDate] lowercaseString];
    }
    else{
        [df setDateFormat:SHOWING_MONTH_AND_DATE_FORMAT];
      cell.labelTime.text=[df stringFromDate:date];
    }
    
    if(lastPageBool==NO)
    {
        if(indexPath.row==[_arrResponse count]-1 )
        {
            if(_currentPage <_totalPages)
            {
                [self getNotificationListWithCompletion:^(BOOL success) {
                    


                }];
                [activity startAnimating ];
                  activity.hidden = NO;

            }
            else
            {
                lastPageBool=YES;
                activity.hidden = YES;
                [activity stopAnimating ];

            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if([[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"isread"] isEqualToString:@"0"]){
            
            [self setReadNotification:[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"id"]];
            
            int k = [[UserManager getNotificationCount] intValue]-1;
            
            [UserManager saveNotificationCount:[NSString stringWithFormat:@"%d",k]];
            [UIApplication sharedApplication].applicationIconBadgeNumber =[[UserManager getNotificationCount] integerValue];
        }
       __block UIViewController *commonViewController;
        // If you then need to execute something making sure it's on the main thread (updating the UI for example)
        dispatch_async(dispatch_get_main_queue(), ^{
            if( [[[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"] intValue] == 0){
            AcceptDeclineJobViewController *VC = (AcceptDeclineJobViewController*)[sharedUtils.mainStoryboard
                                                                                   instantiateViewControllerWithIdentifier: @"AcceptDeclineJobViewController"];
            VC.orderID = [[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"order_id"];
            VC.notification_type = [[_arrResponse objectAtIndex:indexPath.row] objectForKey:@"notification_type"];
                commonViewController = VC;
                [self.navigationController pushViewController:commonViewController animated:true];

            }
//                else{
//                OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
//                vc.swiftySideMenu.centerViewController = self.navigationController;
//               // vc.orderDetail = ;
//                commonViewController = vc;
//            }

        });
    });
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    activityView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableNotification.frame.size.width, 40)];
    activityView.backgroundColor =[UIColor clearColor];
    activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.frame = CGRectMake(CGRectGetMidX(_tableNotification.frame) -15, CGRectGetMidY(activityView.frame), 30, 30);
    [activityView addSubview:activity];
    _tableNotification.tableFooterView= activityView;
    return activityView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
   return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70;
}

-(void)menuButtonClicked{

    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

#pragma  mark - WebServices

-(void)getNotificationListWithCompletion:(void (^) (BOOL success))completion {
    
    if([LSUtils isNetworkConnected])
    {
        _currentPage=_currentPage+1;
        
        if (_currentPage==1) {
            
            [sharedUtils startAnimator:self];
        }
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&page_no=%@",getNotificationList_Service,[UserManager getUserID],[NSString stringWithFormat:@"%d",(int)_currentPage] ];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                       options:0
                                                                                                         error:NULL];
                                              [sharedUtils stopAnimator:self];
                                              
                                              if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  if (_currentPage==1) {
                                                      _arrResponse=[[NSMutableArray alloc] initWithArray:[Response objectForKey:@"data"]];
                                                  }
                                                  else{
                                                      [_arrResponse addObjectsFromArray:[Response objectForKey:@"data"]];
                                                  }
                                                  if (_arrResponse.count>0) {
                                                       //self.currentPage =  [[Response objectForKey:@"page_no"] integerValue];
                                                      
                                                      self.totalPages  = [[Response objectForKey:@"noofpage"] integerValue];
                                                  }
                                                  if(_arrResponse.count == 0 ){
                                                      
                                                      UIAlertController *alert = [sharedUtils showAlert:@"" withMessage:@"No new notification" ];
                                                      [self presentViewController:alert animated:true completion:nil];
                                                  }
                                                  
                                                  [self.tableNotification reloadData];
                                                  completion(true);
                                              }
                                              else if ([[Response  objectForKey:@"code"] isEqualToString:@"210"]) {
                                                  
                                                  if(_arrResponse.count == 0) {
                                                      
                                                      [sharedUtils showNoResultsViewWithOptionalText:NSLocalizedString(@"You do not have any notifications.", @"You do not have notifications.") xPosition:0 yPosition:0 screenWidth:0 screenHeight:64 ];
                                                      sharedUtils.noResultsView.backgroundColor = [UIColor whiteColor];
                                                      [self.tableNotification addSubview:sharedUtils.noResultsView];
                                                      
                                                      completion(false);
                                                      
                                                  }
                                              }
                                              else if ([[Response  objectForKey:@"code"] isEqualToString:@"401"]) {
                                                  completion(false);
                                                  
                                              }
                                              
                                          }
                                          else{
                                              
                                          }
                                              [sharedUtils stopAnimator:self];
                                          });
                                      }];
        [task resume];
    }
    else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}
-(void)setReadNotification:(NSString*)orderId{
    
    if([LSUtils isNetworkConnected])
    {
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *params = [NSString stringWithFormat:@"%@&notification_id=%@",setReadNotification_Service,orderId];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                       options:0
                                                                                                         error:NULL];
                                              if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  }
                                              
                                          }
                                          else{
                                              //:Error
                                          }
                                          });
                                      }];
        [task resume];
    }
    else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)clearList:(id)sender{
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    
    for(int i = 0 ; i<_arrResponse.count ; i++){
        
        NSDictionary* dict  = [_arrResponse objectAtIndex:i];
        
        //Read notifications
        if([[dict objectForKey:@"isread"] isEqualToString:@"1"]){
            
            NSIndexPath *indexNo = [NSIndexPath indexPathForRow:i inSection:0];
            [tempArray addObject:indexNo];
            [indexSet addIndex:indexNo.row];
        }
    }
    [_arrResponse removeObjectsAtIndexes:indexSet];
    [_tableNotification beginUpdates];
    [_tableNotification deleteRowsAtIndexPaths:tempArray withRowAnimation:UITableViewRowAnimationFade];
    [_tableNotification endUpdates];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

@end
