//
//  DeclineOrdersViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 22/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "DeclineOrdersViewController.h"
#import "orderHistoryTableViewCell.h"
#import "LSUtils.h"
#import "AddressViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OrderDetailViewController.h"
#import "SelectedAddressView.h"
#import "UIView+XIB.h"
#import "Orderhistory.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+CL.h"
@interface DeclineOrdersViewController ()
{
    NSString *todaysDateString;
    SelectedAddressView *view  ;
    UIButton *dimView;
    NSMutableArray *orderArray;
    NSString* imageBaseURL;
    NSString* selectedDate;

}
@end

@implementation DeclineOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    _backGroundImageForCalender.image = [UIImage calendarBackgroundImage:CGRectGetHeight(_backGroundImageForCalender.frame)];
    _declineOrderlabel.text = [NSString stringWithFormat:@"%d", (int)orderArray.count ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];

}
-(void)checkLocationPermission{
    
    if([LSUtils checkIfLocationServicesAreEnabledOrNot]){
        
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""  message:@"Please enable location service from settings."preferredStyle:    UIAlertControllerStyleAlert];
        
        UIAlertAction *closeAction = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"Ok", @"")
                                      style:UIAlertActionStyleCancel
                                      handler:^(UIAlertAction *action)
                                      {
                                          
                                      }];
        
        [alertController addAction:closeAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
   // [self.navigationItem setLeftBarButtonItem:barButtonItem];
    if(_priviousScreen == 1){
        
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        
        negativeSpacer.width = 8;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)],barButtonItem , nil]];
    }else{
        [self.navigationItem setLeftBarButtonItem:barButtonItem];
        
    }
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;
    [self.calenderBaseView addSubview:self.calendarView];
    _calendarView.delegate = (id)self;
    _calendarView.selectedDate = [NSDate date];
}

//Initialize
-(CLWeeklyCalendarView *)calendarView
{
    if(!_calendarView){
        _calendarView = [[CLWeeklyCalendarView alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width-20, _calenderBaseView.frame.size.height)];
        _calendarView.delegate = (id)self;
    }
    return _calendarView;
}

- (IBAction)mapButtonClicked:(id)sender {
    AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
    [self presentViewController:vc animated:true completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    orderHistoryTableViewCell *cell = (orderHistoryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    Orderhistory* orderObjectAtIndex = [orderArray objectAtIndex:indexPath.row];
    orderObjectAtIndex.orderStatus = REJECTED;
    [cell setOrderDetails:orderObjectAtIndex];
    cell.pickUpTimeLabel.text =[self getFormatedDate:orderObjectAtIndex.pickupTime];
    
    if(IS_IPHONE5){
        
        UILabel *label =  (UILabel*) [cell viewWithTag:98];
        UILabel *label1 =  (UILabel*) [cell viewWithTag:99];
        label.hidden = true;
        label1.hidden = true;
        
    }else{
        UILabel *label =  (UILabel*) [cell viewWithTag:198];
        UILabel *label1 =  (UILabel*) [cell viewWithTag:199];
        label.hidden = true;
        label1.hidden = true;
    }

    // Here we use the new provided sd_setImageWithURL: method to load the web image
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", imageBaseURL, [orderObjectAtIndex.productImageArray objectAtIndex:0]]]
                             placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

    cell.productImageView.layer.cornerRadius = 10;
    cell.productImageView.layer.masksToBounds = true;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  orderArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
    vc.imagebaseURL = imageBaseURL;
    vc.swiftySideMenu.centerViewController = self.navigationController;
    vc.orderDetail = [orderArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"placed on%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}
-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)getAllOrders :(NSString*)dateString {
    
    if([LSUtils isNetworkConnected]){
        
        orderArray = [[NSMutableArray alloc]init];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [df setDateFormat:@"yyyy-MM-dd"];
        
        NSURL *url = [NSURL URLWithString:Base_URL];//"2016-11-21
           NSString *params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&delivery_date=%@",GetRejectedDaydOrders,[UserManager getUserID],selectedDate];
       
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:0
                                                                                                             error:NULL];
                                              if(responseDict == nil){
                                                  UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfDict;
                                                  imageBaseURL = [responseDict valueForKey:@"productimgurl"];
                                                  arrayOfDict = [responseDict valueForKey:@"data"];
                                                  
                                                  for(int index =0 ; index<arrayOfDict.count ; index++){
                                                      
                                                      NSDictionary * orderDictionary= [arrayOfDict objectAtIndex:index];
                                                      
                                                      //Set OrderHistory Model
                                                      
                                                      Orderhistory *model = [[Orderhistory alloc]init];
                                                      model.pickupAddress = [orderDictionary valueForKey:@"pickup_address"];
                                                      model.delAddress = [orderDictionary valueForKey:@"dropoff_address"];
                                                      model.productName = [orderDictionary valueForKey:@"product_title"];
                                                      model.productDesc = [orderDictionary valueForKey:@"product_description"];
                                                      model.modeOfTransport = [orderDictionary valueForKey:@"transportation_mode"];
                                                      model.price = [orderDictionary valueForKey:@"earned_cost"];
                                                      model.orderNumber = [orderDictionary valueForKey:@"order_id"];
                                                      model.orderStatus = [[orderDictionary valueForKey:@"delivery_status"] intValue];
                                                        model.pickupTime = [orderDictionary valueForKey:@"created_on"];
                                                      [orderArray addObject:model];
                                                      
                                                      NSArray *imageArray = [orderDictionary valueForKey:@"order_images"];
                                                      NSMutableArray *imageNameArray = [[NSMutableArray alloc]init];
                                                      for(int i =0 ; i<imageArray.count ; i++){
                                                          [imageNameArray addObject:[[imageArray objectAtIndex:i] valueForKey:@"product_image" ]];
                                                      }
                                                      model.productImageArray = imageNameArray;
                                                  }
                                                  if(orderArray.count == 0){
                                                      
                                                      [sharedUtils showNoResultsViewWithOptionalText:nil xPosition:0 yPosition:IS_IPHONE5?CGRectGetMinY(_orderCompletedView.frame):CGRectGetMinY(_orderCompletedView.frame)+20 screenWidth:0 screenHeight:CGRectGetHeight(_orderCompletedView.frame) +CGRectGetHeight(_calenderBaseView.frame)+ CGRectGetHeight(_headingLabel.frame)+64 ];
                                                      sharedUtils.noResultsView.backgroundColor = [UIColor whiteColor];
                                                      [self.view addSubview:sharedUtils.noResultsView];
                                                      return;
                                                  }else{
                                                      [sharedUtils.noResultsView removeFromSuperview];
                                                  }
                                                  _declineOrderlabel.text = [NSString stringWithFormat:@"%d", (int)orderArray.count ];

                                                  [_tableView reloadData];
                                              }else  if ([[responseDict objectForKey:@"code"]isEqualToString:@"210"]) {
                                                  
                                                  if(orderArray.count == 0){
                                                      
                                                      [sharedUtils showNoResultsViewWithOptionalText:nil xPosition:0 yPosition:IS_IPHONE5?CGRectGetMinY(_orderCompletedView.frame):CGRectGetMinY(_orderCompletedView.frame)+20 screenWidth:0 screenHeight:CGRectGetHeight(_orderCompletedView.frame) +CGRectGetHeight(_calenderBaseView.frame)+ CGRectGetHeight(_headingLabel.frame)+64 ];
                                                      sharedUtils.noResultsView.backgroundColor = [UIColor whiteColor];
                                                      [self.view addSubview:sharedUtils.noResultsView];
                                                      return;
                                                  }
                                              }
                                              if(orderArray.count > 0){
                                                  [sharedUtils.noResultsView removeFromSuperview];
                                              }

                                          }
                                          else{
                                          }
                                          });
                                      }];
        [task resume];
    }
    else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

//After getting data callback
-(void)dailyCalendarViewDidSelect:(NSDate *)date {
    
    [self getNeededDateFormat:date];
    [self getAllOrders:selectedDate];
}

-(NSString*)getNeededDateFormat :(NSDate*)dt{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:@"yyyy-MM-dd"];
    selectedDate=  [df stringFromDate:dt];
    return selectedDate;
}

/*
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}
 */
- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

//-(NSDictionary *)CLCalendarBehaviorAttributes
//{
//    return @{
//             CLCalendarWeekStartDay : @2, 	//Start from Tuesday every week
//             CLCalendarDayTitleTextColor : [UIColor yellowColor],
//             CLCalendarSelectedDatePrintColor : [UIColor greenColor],
//             };
//}
@end


