//
//  DeclineOrdersViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 22/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLWeeklyCalendarView.h"
@interface DeclineOrdersViewController : UIViewController <UITableViewDelegate , UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageForCalender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *declineOrderlabel;
@property (weak, nonatomic) IBOutlet UIView *calenderBaseView;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIView *orderCompletedView;
@property (nonatomic, strong) CLWeeklyCalendarView* calendarView;
@property(nonatomic)int priviousScreen;

@end
