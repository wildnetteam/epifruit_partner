//
//  SignatureViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 20/03/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "SignatureViewController.h"
#import "UIColor+HexString.h"
#import "OTPViewController.h"
#import "LSUtils.h"
#import "UIView+XIB.h"
#import "UIImage+Scale.h"
#import "NotificationManager.h"

@interface SignatureViewController ()

@end

@implementation SignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftBarBtnItem];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}
-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    if(textField == self.customernameTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.nameImageView.hidden = true;
        }
        if (([self.customernameTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.nameImageView.hidden = false;
            return false;
        }
        return true;
    }
    return true;
    
}

- (void)setLeftBarBtnItem
{
    
    self.title = @"";

    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    UIButton* addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setFrame:CGRectMake(0, 0,100, 50)];
    [addButton addTarget:self action:@selector(clearClicked:) forControlEvents:UIControlEventTouchUpInside];
    [addButton setTitle:@"Clear" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];//[UIColor colorWithHexString:@"7d7d7d"
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -25;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:addButton] , nil]];
        
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
  
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    [LSUtils addPaddingToTextField:_customernameTextField];
    [sharedUtils placeholderSize:_customernameTextField :@"Enter customer name..."];
    
}
-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:true];
}

-(void)upadateSignature {
    
    NSString *param;
    
    if([LSUtils isNetworkConnected])
    {
        NSURL *url = [NSURL URLWithString:Base_URL];//"2016-11-21
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
    param = [NSString stringWithFormat:@"%@&deliverboy_id=%@&order_id=%@&customer_sign=%@&customer_name=%@",dropoffesignature,[UserManager getUserID],_orderId,[UIImagePNGRepresentation([[_signatureVw signatureImage] scaleToSize:CGSizeMake(80, 80)]) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength] ,[LSUtils removeLeadingSpacesfromString:_customernameTextField.text ]];
        NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self ];
                                              _doneButton.enabled = true;

                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  if(responseDict == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      OTPViewController *otpVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"OTPViewController"];
                                                      otpVC.orderId =_orderId;
                                                      otpVC.otp = [responseDict objectForKey:@"otp_code"];
                                                      [self.navigationController pushViewController:otpVC animated:true];

                                                  }else{
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:[responseDict objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        _doneButton.enabled = true;
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)clearClicked:(id)sender{
 
    [_signatureVw erase];
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
}

- (IBAction)doneButtonClicked:(id)sender {
    if(![LSUtils checkNilandEmptyString:_customernameTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Name can not be blank." withType:Negative];
        return;
    }
    
    if([LSUtils isNetworkConnected]){
        
        _doneButton.enabled = false;
   // UIImage *signImage = [[_signatureVw signatureImage] scaleToSize:ProfileImageResizeScale];
    [self upadateSignature];
    }
}


// Animate text field

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_customernameTextField) {
        
        UIButton *nameClearButton = [_customernameTextField valueForKey:@"_clearButton"];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.customernameTextField.text length]>0) {
            self.nameImageView.hidden=true;
        }
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(!([_customernameTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_customernameTextField :@"Enter customer name..."];
    }
    self.nameImageView.hidden=false;

}
@end
