//
//  SignatureViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 20/03/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPSSignatureView.h"
@interface SignatureViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet PPSSignatureView *signatureVw;
@property (retain , nonatomic) NSString *orderId;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIImageView *nameImageView;
@property (weak, nonatomic) IBOutlet UITextField *customernameTextField;
@end
