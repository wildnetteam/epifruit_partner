//
//  AddressViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 14/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Orderhistory.h"
@interface AddressViewController : UIViewController  <GMSMapViewDelegate >
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (retain, nonatomic) NSMutableArray* markersArray;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (strong, nonatomic) NSMapTable *usersToMarkers_;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickuploctionLabel;
@property (weak, nonatomic) IBOutlet UIButton *reachButton;
//@property (weak, nonatomic) NSDictionary *pickUp;
//@property (weak, nonatomic) NSDictionary *dropOff;
@property (weak, nonatomic) Orderhistory *orderObj;
@property (weak, nonatomic) IBOutlet UIView *startRideView;
- (void)updateCameraWithLocation:(CLLocation*)location;
@end
