//
//  AddressViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 14/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "AddressViewController.h"
#import "SwiftySideMenuViewController.h"
#import "UIViewController+SwiftySideMenu.h"
#import "LSUtils.h"
#import "SignatureViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface AddressViewController ()
{
    AppDelegate *del;
    int deliveryStatus;// Maintain this variable to keep track of status 'started for pickup , pickup , droppedoff
    int startRideVisibility; // Maintain this variable to hide and show 'start ride' button.If value is another than 0 hide the button

}

@property BOOL markerTapped;
@property (strong, nonatomic) GMSMarker *currentlyTappedMarker;
@end

@implementation AddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setUpUIElementsValues];
    [self getOngoingOrderStatusWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
        
        if(result == nil){
            
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
            [self presentViewController:controller animated:YES completion:nil];
        }
        if(error!=nil){
            
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            [self presentViewController:controller animated:YES completion:nil];
        }
        if(success){
            if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                NSDictionary *detailedData = [result objectForKey:@"orderdata"];
                deliveryStatus = [[detailedData objectForKey:@"order_status"] intValue];
                startRideVisibility = deliveryStatus;
                [self setButtonText];
            }
            else  if ([[result objectForKey:@"code"]isEqualToString:@"210"]) {
                UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"could not update."];
                [self presentViewController:controller animated:YES completion:nil];
            }
        }else{
            
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
            [self presentViewController:controller animated:YES completion:nil];
        }
    } ];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    _mapView.delegate = self;
    del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self plotMarkers];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

- (IBAction)startRideButton:(id)sender {
    
   // [self updateYourLocation];
    startRideVisibility = 1;
    if([LSUtils isNetworkConnected]){
        
        __block NSString* _address;
        CLLocation* eventLocation;
        switch (deliveryStatus) {
            case PENDING:
            {
                
            }
                break;
            case ACCEPTED:
            {
                eventLocation = [[CLLocation alloc] initWithLatitude:sharedUtils.CURRENTLOC.latitude  longitude:sharedUtils.CURRENTLOC.longitude];
                
            }
                break;
                
            case ONGOING:
            case DROPOFF:
            {
                eventLocation = [[CLLocation alloc] initWithLatitude:[[_orderObj.dropOffCordinate valueForKey:@"dr_lat"] floatValue] longitude:[[_orderObj.dropOffCordinate  valueForKey:@"dr_long"]floatValue]];
                
            }
            break;
            default:
                break;
        }
        if(eventLocation==nil){
            UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"could not retrieve loaction."];
            
            [self presentViewController:controller animated:YES completion:nil];
            return;
            
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            if(address) {
                _address = address;
                
                NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
                NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
                if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
                    
                    NSString *url = [NSString stringWithFormat: @"comgooglemaps-x-callback://?daddr=%@&x-success=sourceapp://?resume=true&x-source=AirApp", [_address stringByAddingPercentEncodingWithAllowedCharacters:set]];
                    NSURL *directionsURL = [NSURL URLWithString:url];
                    [[UIApplication sharedApplication] openURL:directionsURL];
                }else{
                    //If google map isnotinstalled , then user will be redirected itunes to install google navigation
                    NSURL *mapAppURL =  [NSURL URLWithString:itunesMapLink];
                    [[UIApplication sharedApplication] openURL:mapAppURL];
                    
                }
            }
        }];
    }else{
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

/*
 0=pending,
 1=accepted,
 2=pickup/ ongoing,
 3=dropoff/ completed,
 4=order canceled by vendor,
 5=auto cancelled*/

-(void)setButtonText {
    
    //  if(startRideVisibility == 0) {
//    if(startRideVisibility == 0 || (startRideVisibility == 1)) {
//        _startRideView.hidden = false;
//    }else
//        _startRideView.hidden = true;

    switch (deliveryStatus) {
        case PENDING://not responded

        case ACCEPTED://Retailer Accepted the driver
            [_pickuploctionLabel setText:@"Reached pickup location"];
            break;
            
        case ONGOING://started for pickUp
         //  _startRideView.hidden = true;
            [_pickuploctionLabel setText:@"Reached drop-off location"];
            break;
            
        case DROPOFF://started for delivery
         //   _startRideView.hidden = true;
              [_pickuploctionLabel setText:@"Ride completed, Please take signature"];
            break;
            
        default:
            break;
    }
    [self getdistance];
}
//change button text depending on status
-(void)setButtonTextManually:(int)value {
    
//    if(startRideVisibility == 0) {
//        _startRideView.hidden = false;
//    }else
//        _startRideView.hidden = true;
    
    switch (value) {
        case ACCEPTED://not responded
            [_pickuploctionLabel setText:@"Reached pickup location"];
            break;
        case ONGOING://started for pickUp
           // _startRideView.hidden = true;
            [_pickuploctionLabel setText:@"Reached drop-off location"];
            break;
            
        case DROPOFF://started for delivery
           // _startRideView.hidden = true;
            [_pickuploctionLabel setText:@"Ride completed"];
            break;
            
        default:
            break;
    }
    [self getdistance];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self getdistance];
}

// change where the camera is on the map
- (void)updateCameraWithLocation:(CLLocation*)location
{
    GMSCameraPosition *oldPosition = [self.mapView camera];
    GMSCameraPosition *position = [[GMSCameraPosition alloc] initWithTarget:[location coordinate] zoom:[oldPosition zoom] bearing:[location course] viewingAngle:[oldPosition viewingAngle]];
    [self.mapView setCamera:position];
    [self focusMapToShowAllMarkers];

}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
//NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)startDeliveryButtonClicked:(id)sender {
    
    [sharedUtils startAnimator:self];
    _reachButton.enabled = false;
    [self getOngoingOrderStatusWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
        [sharedUtils stopAnimator:self];
        if(result == nil){
            
            _reachButton.enabled = true;
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
            [self presentViewController:controller animated:YES completion:nil];
        }
        if(error!=nil){
            
            _reachButton.enabled = true;
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            [self presentViewController:controller animated:YES completion:nil];
        }
        if(success){
            if ([[result objectForKey:@"code"]isEqualToString:@"200"]) {
                NSDictionary *detailedData = [result objectForKey:@"orderdata"] ;
                deliveryStatus = [[detailedData objectForKey:@"order_status"] intValue];
                [self setButtonText];

            // update server with you location
                NSString *param;
                switch (deliveryStatus) {
                    case ACCEPTED ://0 1=accepted
                    {
                        //Your status was "Ride started just update location
                        param = [NSString stringWithFormat:@"%@&driver_id=%@&order_id=%@",pickupOrder,[UserManager getUserID],_orderObj.productId];
                    }
                        break;
                    case ONGOING:
                    {
                        //Your status was "Ride started and then reach pick up location
                       param = [NSString stringWithFormat:@"%@&driver_id=%@&order_id=%@",dropoffOrder,[UserManager getUserID],_orderObj.productId];
                    }
                        break;
                    case DROPOFF:
                    {
                        
                        //Your status was "Completed pickup and then reach drop off location
                        SignatureViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"SignatureViewController"];
                        vc.orderId = _orderObj.productId;
                        [self.navigationController pushViewController:vc animated:true];

                        return ;
                    }
                        break;
                    default:
                        break;
                }
                if([LSUtils isNetworkConnected])
                {
                    
                    [sharedUtils startAnimator:self];

                    //  location_type => 1 for pickup , 2 for dropoff , 3 for normal location update
                    NSURL *url = [NSURL URLWithString:Base_URL];//"2016-11-21
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:60.0];
                    NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
                    [request setHTTPBody:postData];
                    request.HTTPMethod = @"POST";
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                            completionHandler:
                                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [sharedUtils stopAnimator:self ];
                                                          _reachButton.enabled = true;
                                                          if (data.length > 0 && error == nil)
                                                          {
                                                              NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                           options:0
                                                                                                                             error:NULL];
                                                              if(responseDict == nil){
                                                                  UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                                  [self presentViewController:controller animated:YES completion:nil];
                                                              }
                                                              if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                                  
                                                                  NSDictionary *detailedData = [responseDict objectForKey:@"orderdata"] ;
                                                                  deliveryStatus = [[detailedData objectForKey:@"order_status"] intValue];
                                                                  
                                                                  [self setButtonTextManually:deliveryStatus];
                                                                  
                                                                  if(deliveryStatus == DROPOFF){
                                                                      
                                                                      //Your status was "started for drop off and then completed drop off so take customer sign
                                                                      SignatureViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"SignatureViewController"];
                                                                      vc.orderId = _orderObj.productId;
                                                                      [self.navigationController pushViewController:vc animated:true];
                                                                      
                                                                  }
                                                                  
                                                              }
                                                              else  if ([[responseDict objectForKey:@"code"]isEqualToString:@"210"]) {
                                                                  
                                                              }
                                                          }
                                                      });
                                                  }];
                    [task resume];
                    
                }else{
                    
                    UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
                    [self presentViewController:controller animated:YES completion:nil];
                }

            }
            else  if ([[result objectForKey:@"code"]isEqualToString:@"210"]) {
                UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"could not update."];
                [self presentViewController:controller animated:YES completion:nil];
            }
        }else{
            
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
            [self presentViewController:controller animated:YES completion:nil];
        }
    } ];
}

//Use distancematrix google api for calculating distance from source and distance
-(void )getdistance {
    
    [_spinner startAnimating];
    [self getOngoingOrderStatusWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
    }];
    NSURL *url = [NSURL URLWithString:@""] ;
    CLLocation* eventLocation;
    
    switch (deliveryStatus) {
        case ACCEPTED://0
        {
            //Get distance from current location to pickup location
            //convert CLLocationCoordinate2D to CLLocation
            eventLocation = [[CLLocation alloc] initWithLatitude:sharedUtils.CURRENTLOC.latitude longitude:sharedUtils.CURRENTLOC.longitude];
            
            if(eventLocation==nil){
                UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"could not retrieve loaction."];
                
                [self presentViewController:controller animated:YES completion:nil];
                return;
            }
               url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?&origins=%@,%@&destinations=%@,%@&key=AIzaSyAcaIsz2XGPAx1lSKMZ1LT4_E5TNEQasR4",[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.latitude],[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.longitude],[_orderObj.pickUpCordinate valueForKey:@"pi_lat"],[_orderObj.pickUpCordinate valueForKey:@"pi_long"]]];
        }
            break;
        case ONGOING:
        {
            
            url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?&origins=%@,%@&destinations=%@,%@&key=AIzaSyAcaIsz2XGPAx1lSKMZ1LT4_E5TNEQasR4",[_orderObj.pickUpCordinate valueForKey:@"pi_lat"],[_orderObj.pickUpCordinate valueForKey:@"pi_long"],[_orderObj.dropOffCordinate valueForKey:@"dr_lat"],[_orderObj.dropOffCordinate valueForKey:@"dr_long"]]];
            
        }
            break;
        default:
            break;
    }
    if(url.absoluteString.length == 0){
        [_spinner stopAnimating];
        return;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:80.0];
    
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [_spinner stopAnimating];
                                           if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0 error:NULL];
                                              if( [[responseDictionary valueForKey:@"status"] isEqualToString:@"OK"]){
                                                  NSMutableDictionary *newdict=[responseDictionary valueForKey:@"rows"];
                                                  NSArray *elementsArr=[newdict valueForKey:@"elements"];
                                                  
                                                  NSArray *arr=[elementsArr objectAtIndex:0];
                                                  NSDictionary *dict=[arr objectAtIndex:0];
                                                  [self setText:dict];
//                                                  _distanceLabel.text = deliveryStatus == ACCEPTED? [NSString stringWithFormat:@"%@( %@ )away from pickup location",[[dict valueForKey:@"distance"] valueForKey:@"text"],[[dict valueForKey:@"duration"] valueForKey:@"text"]] :[NSString stringWithFormat:@"%@( %@ )away from drop-off location",[[dict valueForKey:@"distance"] valueForKey:@"text"],[[dict valueForKey:@"duration"] valueForKey:@"text"]];                                                         //}
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

-(double)convertMeteToMiles:(long long)meterVal{
    // 1m = 0.000621371miles
    return meterVal * 0.000621371;
}

-(void)setText:(NSDictionary*)dict{
    
    //change m/Km to miles
    double valueInMeter;
    
    if([[[dict valueForKey:@"distance"] valueForKey:@"text"] containsString:@"km"]){
        //its in km
        //Fisrt convert into meter
        NSString* newstringWithoutKMText  = [[[dict valueForKey:@"distance"] valueForKey:@"text"] stringByReplacingOccurrencesOfString:@"km" withString:@""];
        
        valueInMeter = [newstringWithoutKMText floatValue] *1000;
    }else
        valueInMeter=[[[dict valueForKey:@"distance"] valueForKey:@"text"] longLongValue];
    
    NSString* finalDistance = [NSString stringWithFormat:@"%.02f miles",[self convertMeteToMiles:valueInMeter]];
    
    _distanceLabel.text = deliveryStatus == ACCEPTED? [NSString stringWithFormat:@"%@( %@ )away from pickup location",finalDistance,[[dict valueForKey:@"duration"] valueForKey:@"text"]] :[NSString stringWithFormat:@"%@( %@ )away from drop-off location",finalDistance,[[dict valueForKey:@"duration"] valueForKey:@"text"]];
}
typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
         }
     }];
}

- (IBAction)reachedButtonPressed:(id)sender {
    
}


- (void)plotMarkers
{
    if (!_markersArray) {
        
        _markersArray = [NSMutableArray array];
    }
    NSArray *locations = [self loadLocations];
    for (int i=0; i<[locations count]; i++){
        
        NSString *lat = [[locations objectAtIndex:i] objectAtIndex:0];
        NSString *lon = [[locations objectAtIndex:i] objectAtIndex:1];
        double lt=[lat doubleValue];
        double ln=[lon doubleValue];
        
        // Instantiate and set the GMSMarker properties
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.appearAnimation=YES;
        marker.position = CLLocationCoordinate2DMake(lt,ln);
        marker.map = self.mapView;
        marker.icon = [UIImage imageNamed:[[locations objectAtIndex:i] objectAtIndex:2]];//@"pin"];
        marker.userData = [locations objectAtIndex:i];
        [self.markersArray addObject:marker];
        GMSCameraUpdate *withinCurrentLocationView = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(lt,ln)];
        [_mapView animateWithCameraUpdate:withinCurrentLocationView];
    }
    [self focusMapToShowAllMarkers];
}

- (void)focusMapToShowAllMarkers
{
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (GMSMarker *marker in _markersArray){

        bounds = [bounds includingCoordinate:marker.position];
    }
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
    
}

// Since we want to display our custom info window when a marker is tapped, use this delegate method
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    // A marker has been tapped, so set that state flag
    self.markerTapped = YES;
    
    // If a marker has previously been tapped and stored in currentlyTappedMarker, then nil it out
    if(self.currentlyTappedMarker) {
        self.currentlyTappedMarker = nil;
    }
    
    // make this marker our currently tapped marker
    self.currentlyTappedMarker = marker;
    /* animate the camera to center on the currently tapped marker, which causes
     mapView:didChangeCameraPosition: to be called */
    GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:marker.position];
    [self.mapView animateWithCameraUpdate:cameraUpdate];
    
    NSArray *item = marker.userData;
    marker.title = [item lastObject];
    [mapView setSelectedMarker:marker];
    
    return YES;
}
//test purpose
-(NSArray*)loadLocations{

    NSArray *currentLocation = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.latitude ], [NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.longitude],@"bluepin",@"Current Location", nil];
    NSArray *pickupLocation = [NSArray arrayWithObjects:[_orderObj.pickUpCordinate valueForKey:@"pi_lat"], [_orderObj.pickUpCordinate valueForKey:@"pi_long"], @"greenpin", @"PickUp Location", nil];
    NSArray *deliveryLocation = [NSArray arrayWithObjects:[_orderObj.dropOffCordinate valueForKey:@"dr_lat"], [_orderObj.dropOffCordinate valueForKey:@"dr_long"],@"pin" ,@"DropOff Location", nil];
    NSArray *locArray = [NSArray arrayWithObjects:currentLocation,pickupLocation,deliveryLocation,nil];
    return locArray;
    
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(IBAction)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:true];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}

//0=pending, 1=accepted, 2=pickup/ ongoing, 3=dropoff/ completed, 4=order canceled by vendor, 5=auto cancelled
-(void)getOngoingOrderStatusWithCompletion:(void (^) (BOOL success, NSDictionary* result,NSError* error))completion {
    
    if([LSUtils isNetworkConnected])
    {
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSString *param = [NSString stringWithFormat:@"%@&deliverboy_id=%@&order_id=%@",getdeliveryboyorderstatus,[UserManager getUserID],_orderObj.productId];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self ];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  NSLog(@"Response:%@",responseDict);
                                                  
                                                  completion(true, responseDict, nil);
                                              
                                              }else if(error != nil){
                                                  
                                                  completion(true, nil, error);
                                              }else
                                                  completion(false, nil, nil);
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}
@end


