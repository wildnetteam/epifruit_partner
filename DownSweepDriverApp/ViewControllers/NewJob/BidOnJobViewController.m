//
//  BidOnJobViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 31/05/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "BidOnJobViewController.h"
#import "TransportModel.h"
#import "MyProposalViewController.h"
#import "NotificationManager.h"
@interface BidOnJobViewController (){
    
    NSMutableArray *availableTransportMode;
    __weak IBOutlet NSLayoutConstraint *minHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *maxHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *otherTextLeadingConstraint;
    __weak IBOutlet NSLayoutConstraint * otherTextHeight;
    __weak IBOutlet NSLayoutConstraint * otherTextLeadingReference;
    NSString *finalBidAmaount;
    TransportModel *finalMode;

    __weak IBOutlet NSLayoutConstraint *toolBarBottomConstraint;
}

@end

@implementation BidOnJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    availableTransportMode = [[NSMutableArray alloc]init];
    [self setUIElements];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self getModeOfTransport];
    [self setValueFromoption];
   _modePicker.hidden = true;
    _actionToolbar.hidden = true;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    availableTransportMode = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)setValueFromoption {
   
    if([_maxAmount isEqualToString:@""] && [_minAmount isEqualToString:@""]){
        _optionStackView.hidden = true;
        otherTextHeight.constant = 0.0;
        otherTextLeadingConstraint.constant = 0;
        _otherAmountTextfield.enabled = true;
        _otherLabelTitle.text =@"Enter Amount";
    }else{
        if([_maxAmount isEqualToString:@""]){
            _maxButton.hidden = true;
            _maxlabel.hidden = true;
            _maxHeight.constant = 0;
        }
        if([_minAmount isEqualToString:@""]){
            _minButton.hidden = true;
            _minLabel.hidden = true;
            _minHeight.constant = 0;
            
        }
        _minFlag = false;
        _maxFlag = false;
        _otherFlag = false;
        _otherAmountTextfield.enabled = false;
        
        _minLabel.text = [NSString stringWithFormat:@"$%@" , _minAmount];
        _maxlabel.text = [NSString stringWithFormat:@"$%@" , _maxAmount];
    }

}
- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUIElements{
    
    self.title = @"";
    
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    //UIToolbar
    UIToolbar* toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,0, sharedUtils.screenWidth, 30)];
    toolbar.backgroundColor = [UIColor colorWithHexString:@"#3fcbe4"];
    toolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPickerPad:)],
                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneSelectingPicker:)]];
    [toolbar sizeToFit];
    toolbar.barTintColor = [UIColor greenColor] ;//[UIColor colorWithHexString:@"#3fcbe4"];
    [_modePicker bringSubviewToFront:toolbar];
//    / [toolbar bringSubviewToFront:_modePicker];
    _modeTextfield.inputView = _modePicker;
  //  [_modePicker addSubview:toolbar];
    _modeTextfield.inputAccessoryView = toolbar;
    
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)minButtonClicked:(id)sender {
    
    if(_minFlag){
        //Minimum value already selected
        [_minButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
        _minFlag = false ;
        finalBidAmaount = @"";

    }else {
        [_minButton setImage:[UIImage imageNamed:@"filledBullet"] forState:UIControlStateNormal];
        _minFlag = true ;
        finalBidAmaount = @"";
        finalBidAmaount = _minAmount;
    }
    //unselect all other
    [_maxButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
    [_otherButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
    _otherFlag = false;
    _maxFlag = false ;
    _otherAmountTextfield.enabled = false;
    _otherAmountTextfield.text = @"";

}

- (IBAction)maxButtonClicked:(id)sender {
    
    if(_maxFlag){
        //Minimum value already selected
        [_maxButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
        _maxFlag = false ;
        finalBidAmaount = @"";

    }else {
        [_maxButton setImage:[UIImage imageNamed:@"filledBullet"] forState:UIControlStateNormal];
        _maxFlag = true ;
        finalBidAmaount = @"";
        finalBidAmaount = _maxAmount;
    }
    //unselect all other
    [_minButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
    [_otherButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
    _otherFlag = false;
    _minFlag = false ;
    _otherAmountTextfield.enabled = false;
    _otherAmountTextfield.text = @"";

}

- (IBAction)otherButtonClicked:(id)sender {
    
    
    if(_otherFlag){
        //Other value already selected
        [_otherButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
        _otherFlag = false ;
        _otherAmountTextfield.enabled = false;
        finalBidAmaount = @"";
        _otherAmountTextfield.text = @"";

    }else {
        [_otherButton setImage:[UIImage imageNamed:@"filledBullet"] forState:UIControlStateNormal];
        _otherFlag = true ;
        _otherAmountTextfield.enabled = true;
        finalBidAmaount = @"";
    }
    //unselect all other
    [_minButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
    [_maxButton setImage:[UIImage imageNamed:@"Bullet"] forState:UIControlStateNormal];
    _maxFlag = false;
    _minFlag = false ;
}

-(BOOL)ValidateTextField{
    
    if(![LSUtils checkNilandEmptyString:_modeTextfield.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Transport mode can not be blank." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        return false;
    }
    if(![LSUtils checkNilandEmptyString:finalBidAmaount]){
        
        if(_maxFlag == false && _minFlag == false){
            
            [[NotificationManager notificationManager] displayMessage:@"Bid amount can't be blank" withType:Negative];
  
        }else
        [[NotificationManager notificationManager] displayMessage:@"Please select bid amount." withType:Negative];
        
        return false;
    }
    return true;
}

- (IBAction)saveButtonClicked:(id)sender {
    
    if([finalBidAmaount isEqualToString:@""]){
        finalBidAmaount = _otherAmountTextfield.text;
    }
    if([self ValidateTextField]){
        [self setDriversBidRequest];
    }
}

-(void)driverBidService:(void (^) (BOOL success, NSDictionary* result, NSError* error))completion {
    

    NSString *params = [NSString stringWithFormat:@"%@&driver_id=%@&order_id=%@&mode_id=%@&bid_rate=%@",driverBid,[UserManager getUserID], _orderID, [NSString stringWithFormat:@"%ld", (long)_modeTextfield.tag ], finalBidAmaount];
    
    NSURL *url = [NSURL URLWithString:Base_URL];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              if(responseDictionary == nil){
                                                  
                                                  completion(false, nil ,nil);
                                                  
                                              }
                                              else{
                                                  
                                                  completion(true,responseDictionary,error);
                                              }
                                          }else{
                                              completion(false,nil,error);
                                          }
                                          
                                          
                                      });
                                  }];
    [task resume];
}
#pragma mark - Delegate to show seleected modeof transport

-(void)saveTransportMode:(TransportModel *)selectedMode{
    
    @try {
        
        _modeTextfield.text = selectedMode.tMode;
        [_selectedValues addObject:selectedMode];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        ;
    } @finally {
        ;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == _modeTextfield){
        if(availableTransportMode.count == 0 || availableTransportMode == nil){
            [self getModeOfTransport];
        }else{
            if(_modePicker.hidden) {
                [UIView animateWithDuration:0.3 animations:^{
                    _modePicker.frame = CGRectMake(0, _modePicker.frame.origin.y, _modePicker.frame.size.width, -_modePicker.frame.size.height);
                    _modePicker.hidden = false;
                    _actionToolbar.hidden = false;
                    toolBarBottomConstraint.constant = 200;

                } completion:^(BOOL finished){
//                }]
                    _actionToolbar.hidden = false;
                    toolBarBottomConstraint.constant = 0;


                }];
            }
        }
        return false;
    }
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    if(textField == _otherAmountTextfield ) {
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(textField == _otherAmountTextfield ) {
        finalBidAmaount = textField.text ;
    }

}
#pragma mark - UIPICKER DELEGATE METHODS
// The number of columns of data
- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

// The number of rows of data
- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return  availableTransportMode.count;
}

// The title to display in that specific row and column

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    TransportModel *object = availableTransportMode[row];
    return object.tMode;
}

// The index of the row and column selected
- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    TransportModel *object = availableTransportMode[row];
    finalMode = nil;
    finalMode = object;
}

- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    TransportModel *object = availableTransportMode[row];
    NSString *title = object.tMode;
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

-(IBAction)cancelPickerPad:(id)sender{

    _modePicker.frame = CGRectMake(0, _modePicker.frame.origin.y, _modePicker.frame.size.width, 0);
    [UIView animateWithDuration:0.3 animations:^{
        _modePicker.hidden= true;
        _actionToolbar.hidden = true;
    } completion:^(BOOL finished){
        //                }]

    }];

}
-(IBAction)doneSelectingPicker:(id)sender {
    
    _modeTextfield.text = finalMode.tMode;
    _modeTextfield.tag = [finalMode.tId intValue]; //to strore selected mode id for sending as param
    [self cancelPickerPad:sender];
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
   // _modePicker.hidden = true;
}

//Drivers Bid Apply
-(void) setDriversBidRequest {
    
    if([LSUtils isNetworkConnected]) {
        
        [sharedUtils startAnimator:self];
        
        [self driverBidService:^(BOOL success, NSDictionary *result, NSError *error) {
            
            [sharedUtils stopAnimator:self];
            
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }else{
                if(success){
                    //[result objectForKey:@"message"]
                   UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Sucess"];
                    [self presentViewController:controller animated:YES completion:^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                            for (UIViewController *aViewController in allViewControllers) {
                                if ([aViewController isKindOfClass:[MyProposalViewController class]]) {
                                    MyProposalViewController *view = (MyProposalViewController*) aViewController;
                                    view.isFromNotificationScreen = true;
                                    [self.navigationController popToViewController:view animated:NO];
                                }
                            }
                        });
                    }];

                }
                else{
                    UIViewController*  controller;
                    if(result == nil){
                        
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There is no response."];
                    }else
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There are some problem occured."];
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }
        }];
    }else{
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}


-(void) getModeOfTransport {
    
    if([LSUtils isNetworkConnected]) {
        
        [sharedUtils startAnimator:self];
        
        [self getTransportMode:^(BOOL success, NSDictionary *result, NSError *error) {
            
            [sharedUtils stopAnimator:self];
            
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }else{
                if(success){
                    if(![_modeTextfield isFirstResponder]) return ;
                    if(_modePicker.hidden) {
                        [UIView animateWithDuration:0.3 animations:^{
                            _modePicker.frame = CGRectMake(0, _modePicker.frame.origin.y, _modePicker.frame.size.width, -_modePicker.frame.size.height);
                            _modePicker.hidden = false;
                            _actionToolbar.hidden = false;
                            toolBarBottomConstraint.constant = 200;
                            
                        } completion:^(BOOL finished){
                            _actionToolbar.hidden = false;
                            toolBarBottomConstraint.constant = 0;
                        }];
                    }
                }
                else{
                    UIViewController*  controller;
                    if(result == nil){
                        
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There is no response."];
                    }else
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There are some problem occured."];
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }
        }];
    }else{
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}


#pragma mark - Get Mode of Transport
-(void)getTransportMode:(void (^) (BOOL success, NSDictionary* result, NSError* error))completion {
    
    availableTransportMode =  [[NSMutableArray alloc]init];
    
    NSString *params = [NSString stringWithFormat:@"%@&driver_id=%@",GettransMode,[UserManager getUserID]];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              if(responseDictionary == nil){
                                                  
                                                  completion(false, nil ,nil);
                                                  
                                              }
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfAddress;
                                                  arrayOfAddress = [responseDictionary valueForKey:@"data"];
                                                  
                                                  for(int index =0 ; index<arrayOfAddress.count ; index++){
                                                      
                                                      NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                      
                                                      //Set address Model
                                                      
                                                      TransportModel* modeOfTransport =[[TransportModel alloc]initWithModeName:[arrayOfdata valueForKey:@"mode_name"] ModeID:[arrayOfdata valueForKey:@"mode_id"] :0];
                                                      [availableTransportMode addObject:modeOfTransport];
                                                      [_modePicker reloadAllComponents];
                                                      finalMode = availableTransportMode[0];
                                                  }
                                                  completion(true, responseDictionary ,nil);
                                                  
                                              }else if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                  
                                                  completion(true,responseDictionary,error);
                                              }
                                          }else{
                                              completion(false,nil,error);
                                              
                                          }
                                          
                                      });
                                  }];
    [task resume];
}

@end
