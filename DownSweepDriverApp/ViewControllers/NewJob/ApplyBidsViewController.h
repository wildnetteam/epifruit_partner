//
//  ApplyBidsViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 02/06/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyBidsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *productImageCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *customernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddresslabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *proposalCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *applyButton;
@property (strong, nonatomic) NSString *orderID;
@property (strong, nonatomic) NSString * notification_type;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageCOllectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productimageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *customerNameTopConstraint;
@end
