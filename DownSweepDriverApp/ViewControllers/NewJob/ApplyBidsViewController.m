//
//  ApplyBidsViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 02/06/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "ApplyBidsViewController.h"
#import "UserManager.h"
#import "ProductImageCollectionViewCell.h"
#import "UIImage+Scale.h"
#import "LSUtils.h"
#import "BidOnJobViewController.h"
#import "FullImageViewController.h"

@interface ApplyBidsViewController ()
{
    NSMutableArray *imageArray;
    NSString *productImageBaseUrl;
    NSTimer *timer;
    int remainingCounts;
    NSDictionary *responseDict;
    UIRefreshControl *btmefreshControl;

}
@end

@implementation ApplyBidsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIElements];
    [self getOrderDetail];
   // self.definesPresentationContext = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    responseDict = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [timer invalidate];
    timer = nil;
    imageArray = nil;
    //responseDict = nil;

    [super viewWillDisappear:animated];
}

//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUIElements{
    
    self.title = @"";
    
        [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    [_productImageCollectionView registerNib:[UINib nibWithNibName:@"ProductImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    btmefreshControl = [[UIRefreshControl alloc]init];
    //[_scrollView addSubview: btmefreshControl];
    [self.scrollView insertSubview:btmefreshControl atIndex:0];
    self.scrollView.alwaysBounceVertical = true;
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

-(void)refresh:(id)sender{
    
    if([LSUtils isNetworkConnected]){
        [self getOrderDetail];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        }];
    }
}
- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)backButtonClicked:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)applyButtonClicked:(id)sender {
    
    if([LSUtils isNetworkConnected]){
        if(responseDict!=nil){
            BidOnJobViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"BidOnJobViewController"];
            vc.orderID = _orderID;
            vc.maxAmount = [responseDict objectForKey:@"max_rate"];
            vc.minAmount = [responseDict objectForKey:@"min_rate"];
            vc.swiftySideMenu.centerViewController = self.navigationController;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.navigationController pushViewController:vc animated:YES];
            });
        }else{
            
            UIViewController* controller =   [sharedUtils showAlert:@"" withMessage:@"No sufficient data found, refresh again."];
            [self presentViewController:controller animated:YES completion:nil];
        }
    }else{  UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)getOrderDetail {
    
    if([LSUtils isNetworkConnected]){
        imageArray = [[NSMutableArray alloc]init];
        [sharedUtils startAnimator:self];
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *params = [NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=viewNewJobDetail&order_id=%@&driver_id=%@",_orderID ,[UserManager getUserID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:0
                                                                                                             error:NULL];
#ifdef DEBUG
                                                  
                                                  NSLog(@"Response:%@",Response);
#endif
                                                  UIViewController*  controller;
                                                  if(Response ==nil){
                                                      
                                                      controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  
                                                  if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSDictionary *arrayOfData;
                                                      arrayOfData = [Response valueForKey:@"orderdata"] ;
                                                      responseDict = [[NSDictionary alloc]init];
                                                      responseDict  = arrayOfData ;
                                                      
                                                      NSArray *imageDataArray =[Response valueForKey:@"imagedata"];
                                                      
                                                      for(int index = 0; index < imageDataArray.count; index++){
                                                          
                                                          NSDictionary *imageDict =[imageDataArray objectAtIndex:index];
                                                          [imageArray addObject:[imageDict objectForKey:@"product_image"]];
                                                      }
                                                      _productImageCollectionView.delegate = self;
                                                      _productImageCollectionView.dataSource = self;
                                                      [_productImageCollectionView reloadData];
                                                      NSString* pickUp = [arrayOfData objectForKey:@"pickup_address"];
                                                      _pickUpAddresslabel.text = pickUp;
                                                      _deliveryAddressLabel.text = [arrayOfData objectForKey:@"dropoff_address"];
                                                      _productTitleLabel.text = [arrayOfData objectForKey:@"product_title"];
                                                      _productDescriptionLabel.text = [NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[arrayOfData objectForKey:@"product_quantity"]]?[arrayOfData objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[arrayOfData objectForKey:@"product_type"]]?[arrayOfData objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[arrayOfData objectForKey:@"product_weight"]]?[arrayOfData objectForKey:@"product_weight"]:@"" ];
                                                      
                                                      if([_productDescriptionLabel.text containsString:@"( )"]||![LSUtils checkNilandEmptyString:[arrayOfData objectForKey:@"product_weight"]]){
                                                          _productDescriptionLabel.text = [[_productDescriptionLabel.text stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                      }
                                                      productImageBaseUrl =[Response valueForKey:@"productImagepath"];
                                                      _proposalCountLabel .text = [arrayOfData objectForKey:@"proposalCount"];
                                                      _customernameLabel.text=  [arrayOfData objectForKey:@"fname"];
                                                      //    });
                                                      
                                                      if(imageArray.count == 0){
                                                          _imageCOllectionViewHeightConstraint.constant = 0;
                                                          _productimageHeightConstraint.constant = 0;
                                                          _customerNameTopConstraint.constant = -10;
                                                      }
                                                      [UIView animateWithDuration:0.5 animations:^{
                                                          [self.view layoutIfNeeded];
                                                      } completion:nil];                                                  }else{
                                                      controller =   [sharedUtils showAlert:@"" withMessage:[Response objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                              else{
                                                  
                                                  UIViewController*  controller ;
                                                  if(error != nil){
                                                      
                                                      controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  }else  controller =   [sharedUtils showAlert:@"Somthing went wrong." withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              if(btmefreshControl.isRefreshing  == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                              
                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}
#pragma mark - collection view delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger arrayCount = [imageArray count];
    return arrayCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
   ProductImageCollectionViewCell*   tableCell =(ProductImageCollectionViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",productImageBaseUrl,[imageArray objectAtIndex:indexPath.row]]];
    
    [tableCell.animator startAnimating];
    [tableCell bringSubviewToFront:tableCell.animator];

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [tableCell.animator stopAnimating];
                tableCell.imageViewProduct.image = image;// [image scaleToSize:CGSizeMake(75, 75)];
                tableCell.imageViewProduct.contentMode = UIViewContentModeScaleAspectFill;
                tableCell.imageViewProduct.clipsToBounds = true;
                [tableCell setNeedsLayout];
            });
        }
    });
    return tableCell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductImageCollectionViewCell*  tableCell = (ProductImageCollectionViewCell*) [_productImageCollectionView cellForItemAtIndexPath:indexPath];

            
                FullImageViewController *view = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"FullImageViewController"];
                view.imageIndex =(int) indexPath.row;
                view.image = tableCell.imageViewProduct.image;
                [self.navigationController pushViewController:view animated:YES];
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, NSData *data))completionBlock {
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                if (!error) {
                    completionBlock(YES, data);
                } else {
                    completionBlock(NO, nil);
                }
            }] resume];
}

@end

