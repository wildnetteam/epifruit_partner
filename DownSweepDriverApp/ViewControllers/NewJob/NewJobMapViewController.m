//
//  NewJobMapViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 02/06/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "NewJobMapViewController.h"
#import "SwiftySideMenuViewController.h"
#import "LSUtils.h"
#import <QuartzCore/QuartzCore.h>

@interface NewJobMapViewController ()
@property BOOL markerTapped;
@property (strong, nonatomic) GMSMarker *currentlyTappedMarker;
@end

@implementation NewJobMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setUpUIElementsValues];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    _mapView.delegate = self;
 //  AppDelegate* del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self plotMarkers];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    
   // _orderObj= nil;
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self performSelectorInBackground:@selector(getdistance) withObject:nil];
}

// change where the camera is on the map
- (void)updateCameraWithLocation:(CLLocation*)location
{
    GMSCameraPosition *oldPosition = [self.mapView camera];
    GMSCameraPosition *position = [[GMSCameraPosition alloc] initWithTarget:[location coordinate] zoom:[oldPosition zoom] bearing:[location course] viewingAngle:[oldPosition viewingAngle]];
    [self.mapView setCamera:position];
    [self focusMapToShowAllMarkers];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Use distancematrix google api for calculating distance from source and distance
-(void )getdistance {
    
    [_spinner startAnimating];
    CLLocation* eventLocation;
    
    eventLocation = [[CLLocation alloc] initWithLatitude:sharedUtils.CURRENTLOC.latitude longitude:sharedUtils.CURRENTLOC.longitude];
    
    if(eventLocation==nil){
        UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"could not retrieve loaction."];
        
        [self presentViewController:controller animated:YES completion:nil];
        return;
    }
  //  url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?&origins=%@,%@&destinations=%@,%@&key=AIzaSyAcaIsz2XGPAx1lSKMZ1LT4_E5TNEQasR4",[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.latitude],[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.longitude],[_orderObj.pickUpCordinate valueForKey:@"pi_lat"],[_orderObj.pickUpCordinate valueForKey:@"pi_long"]]];
    
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?&origins=%@,%@&destinations=%@,%@&key=AIzaSyAcaIsz2XGPAx1lSKMZ1LT4_E5TNEQasR4",[_orderObj.pickUpCordinate valueForKey:@"pi_lat"],[_orderObj.pickUpCordinate valueForKey:@"pi_long"],[_orderObj.dropOffCordinate valueForKey:@"dr_lat"],[_orderObj.dropOffCordinate valueForKey:@"dr_long"]]];
    
    if(url.absoluteString.length == 0)
        return;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:80.0];
    
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [_spinner stopAnimating];
  
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0 error:NULL];
                                              if( [[responseDictionary valueForKey:@"status"] isEqualToString:@"OK"]){
                                                  NSMutableDictionary *newdict=[responseDictionary valueForKey:@"rows"];
                                                  NSArray *elementsArr=[newdict valueForKey:@"elements"];
                                                  
                                                  NSArray *arr=[elementsArr objectAtIndex:0];
                                                  NSDictionary *dict=[arr objectAtIndex:0];
                                                  if(dict == nil)   {_distanceLabel.text = [NSString stringWithFormat:@"Could not load distance"];}else
                                                  [self setText:dict];
                                              }
                                          }
                                      });
                                  }];
    [task resume];
}

-(double)convertMeteToMiles:(long long)meterVal{
           // 1m = 0.000621371miles
    return meterVal * 0.000621371;
}
-(void)setText:(NSDictionary*)dict{
    
    //change m/Km to miles
    double valueInMeter;
    
    if([[[dict valueForKey:@"distance"] valueForKey:@"text"] containsString:@"km"]){
        //its in km
        //Fisrt convert into meter
        NSString* newstringWithoutKMText  = [[[dict valueForKey:@"distance"] valueForKey:@"text"] stringByReplacingOccurrencesOfString:@"km" withString:@""];
        
       valueInMeter = [newstringWithoutKMText floatValue] *1000;
    }else
        valueInMeter=[[[dict valueForKey:@"distance"] valueForKey:@"text"] longLongValue];
    
    NSString* finalDistance = [NSString stringWithFormat:@"%.02f miles",[self convertMeteToMiles:valueInMeter]];

    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    //[[dict valueForKey:@"distance"] valueForKey:@"text"]
    
    NSAttributedString*  dateSubString = [[NSAttributedString alloc]
                                          initWithString:[NSString stringWithFormat:@"Pick up location is %@ ",finalDistance]
                                          attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
    
    [completeTitlestring appendAttributedString:dateSubString];
    
    NSAttributedString* durationString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" (%@) ",[[dict objectForKey:@"duration"] objectForKey:@"text"] ]
                                                                    attributes:@{
                                                                                 NSForegroundColorAttributeName: [UIColor colorWithRed:73/255.0f green:206/255.0f blue:229/255.0f alpha:1],
                                                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Semibold" size:16.0]
                                                                                 }];
    [completeTitlestring appendAttributedString:durationString];
    
    NSAttributedString* PickUpString = [[NSAttributedString alloc] initWithString:@"away from drop-off location."
                                                                    attributes:@{
                                                                                 NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:16.0]
                                                                                 }];

    
    [completeTitlestring appendAttributedString:PickUpString];

  //  _distanceLabel.text = [NSString stringWithFormat:@"Pick up location is %@ ( %@ )away from drop-off location",[[dict valueForKey:@"distance"] valueForKey:@"text"],newString];
    [_distanceLabel setAttributedText:completeTitlestring];

}


typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
         }
     }];
}


- (IBAction)reachedButtonPressed:(id)sender {
    
}

- (void)plotMarkers
{
    if (!_markersArray) {
        
        _markersArray = [NSMutableArray array];
    }
    NSArray *locations = [self loadLocations];
    for (int i=0; i<[locations count]; i++){
        
        NSString *lat = [[locations objectAtIndex:i] objectAtIndex:0];
        NSString *lon = [[locations objectAtIndex:i] objectAtIndex:1];
        double lt=[lat doubleValue];
        double ln=[lon doubleValue];
        
        // Instantiate and set the GMSMarker properties
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.appearAnimation=YES;
        marker.position = CLLocationCoordinate2DMake(lt,ln);
        marker.map = self.mapView;
        marker.icon = [UIImage imageNamed:[[locations objectAtIndex:i] objectAtIndex:2]];//@"pin"];
        marker.userData = [locations objectAtIndex:i];
        [self.markersArray addObject:marker];
        GMSCameraUpdate *withinCurrentLocationView = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(lt,ln)];
        [_mapView animateWithCameraUpdate:withinCurrentLocationView];
    }
    [self focusMapToShowAllMarkers];
}

- (void)focusMapToShowAllMarkers
{
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (GMSMarker *marker in _markersArray){
        
        bounds = [bounds includingCoordinate:marker.position];
    }
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
    
}

//test purpose
-(NSArray*)loadLocations{
    
    NSArray *currentLocation = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.latitude ], [NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.longitude],@"bluepin",@"Current Location", nil];
    NSArray *pickupLocation = [NSArray arrayWithObjects:[_orderObj.pickUpCordinate valueForKey:@"pi_lat"], [_orderObj.pickUpCordinate valueForKey:@"pi_long"], @"greenpin", @"PickUp Location", nil];
    NSArray *deliveryLocation = [NSArray arrayWithObjects:[_orderObj.dropOffCordinate valueForKey:@"dr_lat"], [_orderObj.dropOffCordinate valueForKey:@"dr_long"], @"pin" ,@"DropOff Location", nil];
    NSArray *locArray = [NSArray arrayWithObjects:currentLocation,pickupLocation,deliveryLocation,nil];
    return locArray;
    
}


// Since we want to display our custom info window when a marker is tapped, use this delegate method
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    // A marker has been tapped, so set that state flag
    self.markerTapped = YES;
    
    // If a marker has previously been tapped and stored in currentlyTappedMarker, then nil it out
    if(self.currentlyTappedMarker) {
        self.currentlyTappedMarker = nil;
    }
    
    // make this marker our currently tapped marker
    self.currentlyTappedMarker = marker;
    /* animate the camera to center on the currently tapped marker, which causes
     mapView:didChangeCameraPosition: to be called */
    GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:marker.position];
    [self.mapView animateWithCameraUpdate:cameraUpdate];
    
    NSArray *item = marker.userData;
    marker.title = [item lastObject];
    [mapView setSelectedMarker:marker];
    
    return YES;
}

//- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay{
//    _selectedView.hidden = true;
//    
//}

/* If the map is tapped on any non-marker coordinate, reset the currentlyTappedMarker and remove our*/
//- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
//{
//    if(self.currentlyTappedMarker) {
//        self.currentlyTappedMarker = nil;
//    }
//    _selectedView.hidden = true;
//}


#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(IBAction)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:true];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}
@end


