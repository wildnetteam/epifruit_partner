//
//  NewJobMapViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 02/06/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Orderhistory.h"
@interface NewJobMapViewController : UIViewController <GMSMapViewDelegate >
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (retain, nonatomic) NSMutableArray* markersArray;

@property (strong, nonatomic) NSMapTable *usersToMarkers_;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickuploctionLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) Orderhistory *orderObj;
- (void)updateCameraWithLocation:(CLLocation*)location;

@end
