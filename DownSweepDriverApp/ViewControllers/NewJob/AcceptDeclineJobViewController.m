//
//  AcceptDeclineJobViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 27/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "AcceptDeclineJobViewController.h"
#import "UserManager.h"
#import "ProductImageCollectionViewCell.h"
#import "UIImage+Scale.h"
#import "UserManager.h"
#import "HomeViewController.h"

@interface AcceptDeclineJobViewController ()
{
    NSMutableArray *imageArray;
    ProductImageCollectionViewCell * tableCell;
    NSString *productImageBaseUrl;
    NSTimer *timer;
    int remainingCounts;
}
@end

@implementation AcceptDeclineJobViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIElements];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];

    if([_notification_type intValue] == 2){
        _acceptButton.hidden = true;
        _rejectButton.hidden  = true;
    }
    [self getOrderDetail];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self initializeTimer];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [timer invalidate];
    timer = nil;
    [super viewWillDisappear:animated];
}

//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUIElements{
    
    self.title = @"";
    
//    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    [_productImageCollectionView registerNib:[UINib nibWithNibName:@"ProductImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    imageArray = [[NSMutableArray alloc]init];
}

-(void)initializeTimer {
    
    timer = [NSTimer scheduledTimerWithTimeInterval:50
                                             target:self
                                           selector:@selector(countDown)
                                           userInfo:nil
                                            repeats:NO];
    remainingCounts = 20;
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}
-(void)countDown {
    
    if(timer!=nil || [timer isValid]){
        
        //Time exceeds so decline the order automatically
        NSString *params = [NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=acceptdenyorder&deliveryboy_id=%@&order_id=%@&deliveryboy_status=2",[UserManager getUserID],_orderID];
        [self acceptOrDeclineOrder:params];
    }
 }

//-(void)backButtonClicked:(id)sender{
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (IBAction)acceptButtonClicked:(id)sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"Are you sure, you want to accept order?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                                   return ;
                               }];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Accept"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    NSString *params = [NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=acceptdenyorder&deliveryboy_id=%@&order_id=%@&deliveryboy_status=1",[UserManager getUserID],_orderID];
                                    [self acceptOrDeclineOrder:params];

                                }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)deniedButtonClicked:(id)sender {
    
    
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"Are you sure, you want to reject order?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   return ;
                               }];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Reject"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    [self presentViewController:alert animated:YES completion:nil];
                                    NSString *params = [NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=acceptdenyorder&deliveryboy_id=%@&order_id=%@&deliveryboy_status=2",[UserManager getUserID],_orderID];
                                    [self acceptOrDeclineOrder:params];
                                }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)acceptOrDeclineOrder:(NSString*)params{
    
    [sharedUtils startAnimator:self];
    [timer invalidate];
    timer = nil;
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:20.0];
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];

                                          if (data.length > 0 && error == nil)
                                      {
                                          NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                   options:0
                                                                                                     error:NULL];
//#ifdef DEBUG
//
//                                          NSLog(@"Response:%@",Response);
//#endif
                                          if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                             
                                              AppDelegate *del  = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                              HomeViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
                                              vc.isFromNotificationScreen = true;
                                              del.navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
                                              del.SwiftySideMenu.centerViewController = del.navigationController;
                                              del.SwiftySideMenu.leftViewController = del.leftMenu;
                                              del.window.rootViewController = del.SwiftySideMenu;
                                              [self dismissViewControllerAnimated:YES completion:nil];
                                          }
                                      }
                                      else{
                                      }
                                      });
                                  }];
    [task resume];
}

-(void)getOrderDetail {
    
    [sharedUtils startAnimator:self];
    NSURL *url = [NSURL URLWithString:Base_URL];
    NSString *params = [NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getorderdetail&order_id=%@&get_type=1",_orderID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:20.0];
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [sharedUtils stopAnimator:self];
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                       options:0  error:NULL];
#ifdef DEBUG
                                              NSLog(@"Response:%@",Response);
#endif
                                              if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSDictionary *arrayOfData;
                                                  arrayOfData = [Response valueForKey:@"data"];
                                                  
                                                  NSString* pickUp = [[[arrayOfData valueForKey:@"order"] objectAtIndex:0] objectForKey:@"pickup_address"];
                                                  _pickUpAddresslabel.text =pickUp;
                                                  _deliveryAddressLabel.text = [[[arrayOfData valueForKey:@"order"] objectAtIndex:0] objectForKey:@"dropoff_address"];
                                                  _productTitleLabel.text = [[[arrayOfData valueForKey:@"order"] objectAtIndex:0] objectForKey:@"product_title"];
                                                  _productDescriptionLabel.text = [[[arrayOfData valueForKey:@"order"] objectAtIndex:0] objectForKey:@"product_description"];
                                                  productImageBaseUrl =[Response valueForKey:@"productimgurl"];
                                                  _crnLabel.text = _orderID;
                                                  _customernameLabel.text=  [[[arrayOfData valueForKey:@"order"] objectAtIndex:0] objectForKey:@"fname"];
                                                  NSArray *imageCountArray;
                                                  imageCountArray = [arrayOfData valueForKey:@"image"][0];
                                                  if(![imageCountArray isKindOfClass:[NSNull class]]){
                                                      for(int index = 0; index < imageCountArray.count ; index++){
                                                          
                                                          NSDictionary * dict = [imageCountArray objectAtIndex:index];
                                                          [imageArray addObject:dict];
                                                      }
                                                  }
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      _productImageCollectionView.delegate = self;
                                                      _productImageCollectionView.dataSource = self;
                                                      [_productImageCollectionView reloadData];
                                                  });
                                                  
                                              }
                                          }
                                          else{
                                          }
                                      });
                                  }];
    [task resume];

}
#pragma mark - collection view delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger arrayCount = [imageArray count];
    return arrayCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    tableCell =(ProductImageCollectionViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",productImageBaseUrl,[[imageArray objectAtIndex:indexPath.row] valueForKey:@"product_image"]]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:url];
                             
                             UIImage* image = [[UIImage alloc] initWithData:imageData];
                             if (image) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     tableCell.imageViewProduct.image = [image scaleToSize:CGSizeMake(75, 75)];
                                     [tableCell setNeedsLayout];
                                 });
                             }
                             });
    return tableCell;
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, NSData *data))completionBlock {
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                if (!error) {
                    completionBlock(YES, data);
                } else {
                    completionBlock(NO, nil);
                }
            }] resume];
}

@end
