//
//  BidOnJobViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 31/05/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSUtils.h"

@interface BidOnJobViewController : UIViewController <  UIPickerViewDelegate , UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *minLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxlabel;

@property (weak, nonatomic) IBOutlet UILabel *otherLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherLabelTitle;
@property (weak, nonatomic) IBOutlet UITextField *otherAmountTextfield;
@property (weak, nonatomic) IBOutlet UITextField *modeTextfield;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherHeight;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *bulletLabel;

@property (weak, nonatomic) IBOutlet UIButton *minButton;
@property (weak, nonatomic) IBOutlet UIButton *maxButton;
@property (weak, nonatomic) IBOutlet UIButton *otherButton;
@property (weak, nonatomic) IBOutlet  UIPickerView *modePicker;

@property (weak, nonatomic) IBOutlet UIView* minView;
@property (weak, nonatomic) IBOutlet UIView* maxView;
@property (weak, nonatomic) IBOutlet UIView* otherView;

@property (nonatomic) BOOL minFlag;
@property (nonatomic) BOOL maxFlag;
@property (nonatomic) BOOL otherFlag;

@property (strong, nonatomic) NSString *orderID;
@property (strong, nonatomic) NSString *maxAmount;
@property (strong, nonatomic) NSString *minAmount;

@property (strong, nonatomic) NSString * notification_type;

@property (retain, nonatomic) NSMutableArray *selectedValues;
@property (weak, nonatomic) IBOutlet UIStackView* optionStackView;
@property (weak, nonatomic) IBOutlet UIToolbar* actionToolbar;

@end
