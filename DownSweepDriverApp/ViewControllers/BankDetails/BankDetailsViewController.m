//
//  BankDetailsViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 27/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "BankDetailsViewController.h"
#import "NotificationManager.h"
@interface BankDetailsViewController ()

@end

@implementation BankDetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setTextValue:sharedUtils.bankDetailDictionary];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setNeedsStatusBarAppearanceUpdate];
    [self setUIElements];
    
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUIElements{
    
    self.title = @"";
    
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];

    UIButton* editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [editButton setFrame:CGRectMake(0, 0,100, 50)];
    [editButton addTarget:self action:@selector(editDetails:) forControlEvents:UIControlEventTouchUpInside];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Semibold" size:20]];
    [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -25;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:editButton] , nil]];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    [self disableTextField];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    _topViewHeightConstraint.constant = 0;
    _topView.hidden = true;
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    UIButton* editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [editButton setFrame:CGRectMake(0, 0,100, 50)];
    [editButton addTarget:self action:@selector(editDetails:) forControlEvents:UIControlEventTouchUpInside];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Semibold" size:20]];
    [editButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -25;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:editButton] , nil]];
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)editDetails:(id)sender{
    [self enableTextField];
    [_bankNameTextField becomeFirstResponder];
}

-(void)disableTextField{
    
    _usernameTextField.enabled = false;
    _usernameTextField.textAlignment = NSTextAlignmentCenter;
    _userFirtNameLabel.textAlignment = NSTextAlignmentCenter;

    _userLastnameTextField.enabled = false;
    _userLastnameTextField.textAlignment = NSTextAlignmentCenter;
    _userLastNameLabel.textAlignment = NSTextAlignmentCenter;

    _emailTextField.enabled = false;
    _emailTextField.textAlignment = NSTextAlignmentCenter;
    _emailLabel.textAlignment = NSTextAlignmentCenter;

    _phoneTextField.enabled = false;
    _phoneTextField.textAlignment = NSTextAlignmentCenter;
    _phoneLabel.textAlignment = NSTextAlignmentCenter;

    _dobTextField.enabled = false;
    _dobTextField.textAlignment = NSTextAlignmentCenter;
    _dobLabelLabel.textAlignment = NSTextAlignmentCenter;
    
    _streetTextField.editable = false;
    _streetTextField.textAlignment = NSTextAlignmentCenter;
    _streetLabel.textAlignment = NSTextAlignmentCenter;

    _localityTextField.enabled = false;
    _localityTextField.textAlignment = NSTextAlignmentCenter;
    _localityLabel.textAlignment = NSTextAlignmentCenter;

    _regionTextField.enabled = false;
    _regionTextField.textAlignment = NSTextAlignmentCenter;
    _regionameLabel.textAlignment = NSTextAlignmentCenter;
    
    _postalTextField.enabled = false;
    _postalTextField.textAlignment = NSTextAlignmentCenter;
    _postalCodeLabel.textAlignment = NSTextAlignmentCenter;
    
    _AccountNumberTextField.enabled = false;
    _AccountNumberTextField.textAlignment = NSTextAlignmentCenter;
    _accountNumberLabel.textAlignment = NSTextAlignmentCenter;

    _routingTextField.enabled = false;
    _routingTextField.textAlignment = NSTextAlignmentCenter;
    _routingNumberLabel.textAlignment = NSTextAlignmentCenter;
    _Savebutton.hidden = true;
}

-(void)enableTextField{
    
    _Savebutton.hidden = false;
    _lineLeadingConstraint.constant = -100;
    _lineTrailingConstarint.constant = -100;
    _usernameTextField.enabled = true;
    _usernameTextField.textAlignment = NSTextAlignmentLeft;
    _userFirtNameLabel.textAlignment = NSTextAlignmentLeft;
    
    _userLastnameTextField.enabled = true;
    _userLastnameTextField.textAlignment = NSTextAlignmentLeft;
    _userLastNameLabel.textAlignment = NSTextAlignmentLeft;

    _emailTextField.enabled = true;
    _emailTextField.textAlignment = NSTextAlignmentLeft;
    _emailLabel.textAlignment = NSTextAlignmentLeft;

    _phoneTextField.enabled = true;
    _phoneTextField.textAlignment = NSTextAlignmentLeft;
    _phoneLabel.textAlignment = NSTextAlignmentLeft;

    _dobTextField.enabled = true;
    _dobTextField.textAlignment = NSTextAlignmentLeft;
    _dobLabelLabel.textAlignment = NSTextAlignmentLeft;
    
    _streetTextField.editable = true;
    _streetTextField.textAlignment = NSTextAlignmentLeft;
    _streetLabel.textAlignment = NSTextAlignmentLeft;

    _localityTextField.enabled = true;
    _localityTextField.textAlignment = NSTextAlignmentLeft;
    _localityLabel.textAlignment = NSTextAlignmentLeft;

    _regionTextField.enabled = true;
    _regionTextField.textAlignment = NSTextAlignmentLeft;
    _regionameLabel.textAlignment = NSTextAlignmentLeft;

    _postalTextField.enabled = true;
    _postalTextField.textAlignment = NSTextAlignmentLeft;
    _postalCodeLabel.textAlignment = NSTextAlignmentLeft;

    _AccountNumberTextField.enabled = true;
    _AccountNumberTextField.textAlignment = NSTextAlignmentLeft;
    _accountNumberLabel.textAlignment = NSTextAlignmentLeft;

    _routingTextField.enabled = true;
    _routingTextField.textAlignment = NSTextAlignmentLeft;
    _routingNumberLabel.textAlignment = NSTextAlignmentLeft;

}

-(void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)setTextValue:(NSDictionary*)dict{
    
    _usernameTextField.text = [dict objectForKey:@"firstName"];
    _userLastnameTextField.text = [dict objectForKey:@"lastName"];
    _emailTextField.text = [dict objectForKey:@"email"];
    _dobTextField.text = [dict objectForKey:@"dob"];
    _AccountNumberTextField.text = [dict objectForKey:@"accountNumber"];
    _localityTextField.text = [dict objectForKey:@"locality"];
    _streetTextField.text = [dict objectForKey:@"streetAddress"];
    _regionTextField.text = [dict objectForKey:@"region"];
    _routingTextField.text = [dict objectForKey:@"routingNumber"];
    _phoneTextField.text = [dict objectForKey:@"phone"];
    _postalTextField.text = [dict objectForKey:@"postalCode"];
    
    _accountNumber = [dict objectForKey:@"accountNumber"];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIView *textFieldSuperView = textField.superview;
    UIResponder* nextResponder = [textFieldSuperView.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not
}

-(BOOL)validateTextField {
    
    NSCharacterSet* validCharecterSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
    
    if(![LSUtils checkNilandEmptyString:_usernameTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"First Name can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_userLastnameTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Last Name can not be blank." withType:Negative];
        return false;
    }
    
    if(  [_usernameTextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the name field." withType:Negative];
        return false;
    }
    if(  [_userLastnameTextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the name field." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_emailTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Email can not be blank." withType:Negative];
        return false;
        
    }

    if(![LSUtils checkNilandEmptyString:_AccountNumberTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Account Number can not be blank." withType:Negative];
        return false;
        
    }
    
//    if(  [_AccountNumberTextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
//        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the account number field." withType:Negative];
//        return false;
//    }
    
    if(![LSUtils checkNilandEmptyString:_routingTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Routing Number can not be blank." withType:Negative];
        return false;
        
    }
    if(![LSUtils checkNilandEmptyString:_postalTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Postal Code can not be blank." withType:Negative];
        return false;
        
    }
    if(![LSUtils checkNilandEmptyString:_localityTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Locality can not be blank." withType:Negative];
        return false;
        
    }
    if(![LSUtils checkNilandEmptyString:_regionTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Region can not be blank." withType:Negative];
        return false;
        
    }
    if(![LSUtils checkNilandEmptyString:_phoneTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Phone Number can not be blank." withType:Negative];
        return false;
    }
    return true;
}

-(void)getDetails{
    
    if([LSUtils isNetworkConnected]){
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        [sharedUtils startAnimator:self];
        
        NSString *params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@",getdeliveryboyaccountdetail,[UserManager getUserID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              if (data.length > 0 && error == nil)
                                                  
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  if(responseDict == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      
                                                     NSDictionary* arrayOfDict = [[responseDict objectForKey:@"data"] objectAtIndex:0];
                                                      
                                                      [self setTextValue:arrayOfDict];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"210"]) {
                                                      
                                                     UIViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddAccountViewController"];
                                                      [self.navigationController pushViewController:vc animated:true];

                                                  }

                                              }
                                              else{
                                              }
                                          });
                                      }];
        [task resume];
    }
    else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (IBAction)updateDetails:(id)sender {
    
    if ([self validateTextField]) {
     
        [self saveDetails];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == _dobTextField){
        
        UIDatePicker *datePicker = [[UIDatePicker alloc]init];
        [datePicker setDate:[NSDate date]];
        datePicker.backgroundColor =AppDefaultBlueColor;
        [datePicker setValue:[UIColor whiteColor] forKey:@"textColor"] ;
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
        [self.dobTextField setInputView:datePicker];
    }
    return true;
}

-(void) dateTextField:(id)sender{
    UIDatePicker *picker = (UIDatePicker*)self.dobTextField.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    self.dobTextField.text = [NSString stringWithFormat:@"%@",dateString];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

-(void)saveDetails {
    
    BOOL isAccountNumberChanged = false;
    
    if(![_accountNumber isEqualToString:_AccountNumberTextField.text]){
        
        isAccountNumberChanged = true;
    }
    if([LSUtils isNetworkConnected]){
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSString *mobile =  [_phoneTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@"(" withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@")" withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        NSString* params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&firstName=%@&lastName=%@&email=%@&phone=%@&dob=%@&streetAddress=%@&locality=%@&region=%@&postalCode=%@&isaccountupdated=%d&accountNumber=%@&routingNumber=%@",updatesubmerchant,[UserManager getUserID],[LSUtils removeLeadingSpacesfromString:_usernameTextField.text],[LSUtils removeLeadingSpacesfromString:_userLastnameTextField.text],[LSUtils removeLeadingSpacesfromString:_emailTextField.text],mobile,[LSUtils removeLeadingSpacesfromString:_dobTextField.text],[LSUtils removeLeadingSpacesfromString:_streetTextField.text],[LSUtils removeLeadingSpacesfromString:_localityTextField.text],[LSUtils removeLeadingSpacesfromString:_regionTextField.text],[LSUtils removeLeadingSpacesfromString:_postalTextField.text],isAccountNumberChanged,[LSUtils removeLeadingSpacesfromString:_AccountNumberTextField.text],[LSUtils removeLeadingSpacesfromString:_routingTextField.text]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData* data, NSURLResponse* response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                 options:0
                                                                                                                   error:NULL];
                                              [sharedUtils stopAnimator:self];
#ifdef DEBUG
                                              NSLog(@"Response:%@",responseDictionary);
#endif
                                              UIAlertController *controller;
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  controller = [sharedUtils showAlert:@"Sucess" withMessage:@"Saved successfully"];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  [self disableTextField]  ;
                                              }
                                              else
                                              {
                                                  NSDictionary *messageDict = [[responseDictionary objectForKey:@"data"] objectAtIndex:0];
                                                  controller = [sharedUtils showAlert:@"Sorry" withMessage:[messageDict objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }
                                              
                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}
@end
