//
//  AddAccountViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 01/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "AddAccountViewController.h"
#import "NotificationManager.h"
@interface AddAccountViewController ()
{
    BOOL inEditMode;
}
@end

@implementation AddAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    inEditMode = false;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(inEditMode){
        return UIStatusBarStyleDefault;
        
    }    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUpUIElementsValues{
    
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)]];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = @"";
    
    UIButton* saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveButton setFrame:CGRectMake(0, 0,100, 50)];
    [saveButton addTarget:self action:@selector(saveDetails:) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [saveButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Semibold" size:18]];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -25;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:saveButton] , nil]];

    //Add padding to text field
    [LSUtils addPaddingToTextField:_nametextField];
    [LSUtils addPaddingToTextField:_emailTextField];
    [LSUtils addPaddingToTextField:_dobTextField];
    [LSUtils addPaddingToTextField:_phonetextField];
    [LSUtils addPaddingToTextField:_streetTextField];
    [LSUtils addPaddingToTextField:_regionTextField];
    [LSUtils addPaddingToTextField:_postaltextField];
    [LSUtils addPaddingToTextField:_localityTextField];
    [LSUtils addPaddingToTextField:_SSNTextField];
    [LSUtils addPaddingToTextField:_accountNumbertextField];
    
     if(!([_nametextField.text length]>0))
     {
         [sharedUtils placeholderSize:_nametextField :@"Enter your name..."];
     }
     if(!([_emailTextField.text length]>0))
     {
         [sharedUtils placeholderSize:_emailTextField :@"Enter your email..."];
     }
     if(!([_accountNumbertextField.text length]>0))
     {
         [sharedUtils placeholderSize:_accountNumbertextField :@"Enter your account number..."];
     }
     if(!([_lastNameTextField.text length]>0))
     {
         [sharedUtils placeholderSize:_lastNameTextField :@"Enter your last name..."];
     }
    if(!([self getLength:_phonetextField.text]>0))
    {
        [sharedUtils placeholderSize:_phonetextField :@"Enter your number..."];
    }
    if(!([_localityTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_localityTextField :@"Enter your locality..."];
        
    }
    if(!([_regionTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_regionTextField :@"Enter your region..."];
        
    }
    if(!([_postaltextField.text length]>0))
    {
        [sharedUtils placeholderSize:_postaltextField :@"Enter your postal code..."];
        
    }
    if(!([_streetTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_streetTextField :@"Enter your street address..."];
        
    }
    if(!([_dobTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_dobTextField :@"Enter your  dob..."];
    }
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, sharedUtils.screenWidth, 50)];
    // numberToolbar.backgroundColor =  [UIColor colorWithHexString:@"#3fcbe4"];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad:)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad:)]];
    [numberToolbar sizeToFit];
    numberToolbar.barTintColor =[UIColor whiteColor];// [UIColor colorWithHexString:@"#3fcbe4"];
    _phonetextField.inputAccessoryView = numberToolbar;
    _SSNTextField.inputAccessoryView = numberToolbar;
    
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    inEditMode = false;
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

#pragma mark - Validation method

-(BOOL)validatetextField{
    

    NSCharacterSet* validCharecterSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
    
    if(![LSUtils checkNilandEmptyString:_nametextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Name can not be blank." withType:Negative];
        return false;
    }
    
    if(  [_nametextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the name field." withType:Negative];
        return false;
    }
    
       if(![LSUtils checkNilandEmptyString:_accountNumbertextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Account Number can not be blank." withType:Negative];
        return false;
        
    }
    
    if(  [_accountNumbertextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the account number field." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_SSNTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"SSN Number can not be blank." withType:Negative];
        return false;
        
    }
    
    if(  [_SSNTextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the SSN field." withType:Negative];
        return false;
    }
    
    if(_nametextField.text.length<2 || _nametextField.text.length >= 28){
        
        [[NotificationManager notificationManager] displayMessage:@"Name  must be range in 2 to 28." withType:Negative];
        return false;
    }
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    inEditMode = true; //To change statusbar preffered style
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
    
    _topViewHeightConstraint.constant = 0;
    _topView.hidden = true;
    self.view.backgroundColor = APP_BACKGROUND_COLOR;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    UIButton* editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [editButton setFrame:CGRectMake(0, 0,100, 50)];
    [editButton addTarget:self action:@selector(saveDetails:) forControlEvents:UIControlEventTouchUpInside];
    [editButton setTitle:@"Save" forState:UIControlStateNormal];
    [editButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
    [editButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -25;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc] initWithCustomView:editButton] , nil]];
   // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

# pragma mark - TextFieldDelegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == _dobTextField){
        
        UIDatePicker *datePicker = [[UIDatePicker alloc]init];
        [datePicker setDate:[NSDate date]];
        datePicker.backgroundColor =AppDefaultBlueColor;
        [datePicker setValue:[UIColor whiteColor] forKey:@"textColor"] ;
       // [datePicker setValue:[UIFont fontWithName:@"Lato-Regular" size:16] forKey:@"font"] ;

        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
        [self.dobTextField setInputView:datePicker];
    }
    return true;
    
}

-(void) dateTextField:(id)sender{
    UIDatePicker *picker = (UIDatePicker*)self.dobTextField.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    self.dobTextField.text = [NSString stringWithFormat:@"%@",dateString];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{

}


- (void)textFieldDidEndEditing:(UITextField *)textField{

    if(!([_emailTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_emailTextField :@"Enter your email..."];
    }
    if(!([_accountNumbertextField.text length]>0))
    {
        [sharedUtils placeholderSize:_accountNumbertextField :@"Enter your account number..."];
    }
    if(!([_lastNameTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_lastNameTextField :@"Enter your last name..."];
    }
    if(!([_nametextField.text length]>0))
    {
        [sharedUtils placeholderSize:_nametextField :@"Enter your first name..."];
    }
    if(!([_emailTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_emailTextField :@"Enter your email id..."];
    }
    if(!([self getLength:_phonetextField.text]>0))
    {
        [sharedUtils placeholderSize:_phonetextField :@"Enter your number..."];
    }
    if(!([_dobTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_dobTextField :@"Enter your  dob..."];
    }
    if(!([_localityTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_localityTextField :@"Enter your locality..."];
        
    }
    if(!([_regionTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_regionTextField :@"Enter your region..."];
        
    }
    if(!([_postaltextField.text length]>0))
    {
        [sharedUtils placeholderSize:_postaltextField :@"Enter your postal code..."];
        
    }
    if(!([_streetTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_streetTextField :@"Enter your street address..."];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIView *textFieldSuperView = textField.superview;
    UIResponder* nextResponder = [textFieldSuperView.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
 
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    if(textField == _phonetextField ) {
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
        int length = (int)[self getLength:textField.text];
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:textField.text];

            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    return YES;
}

-(void)cancelNumberPad:(id)sender {
    
    if(_phonetextField.isFirstResponder){
    [_phonetextField resignFirstResponder];
    _phonetextField.text = @"";
    }
    else if(_SSNTextField.isFirstResponder){
        [_SSNTextField resignFirstResponder];
        _SSNTextField.text = @"";
    }
}

-(void)doneWithNumberPad:(id)sender{
    
   if(_phonetextField.isFirstResponder){
    [_phonetextField resignFirstResponder];
   }
   else if(_SSNTextField.isFirstResponder){
       [_SSNTextField resignFirstResponder];
   }
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
}

-(void)saveDetails:(id)sender{
    
    [self saveDetails];
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}

-(void)doneWithNumberPad{
    
    [_phonetextField resignFirstResponder];
}

-(void)saveDetails {
    
    if([LSUtils isNetworkConnected]){
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSString *mobile =  [_phonetextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@"(" withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@")" withString:@""];
        mobile=  [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        NSString* params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&firstName=%@&lastName=%@&email=%@&phone=%@&dob=%@&streetAddress=%@&locality=%@&region=%@&postalCode=%@&accountNumber=%@&routingNumber=%@",createsubmerchant,[UserManager getUserID],[LSUtils removeLeadingSpacesfromString:_nametextField.text],[LSUtils removeLeadingSpacesfromString:_lastNameTextField.text],[LSUtils removeLeadingSpacesfromString:_emailTextField.text],mobile,[LSUtils removeLeadingSpacesfromString:_dobTextField.text],[LSUtils removeLeadingSpacesfromString:_streetTextField.text],[LSUtils removeLeadingSpacesfromString:_localityTextField.text],[LSUtils removeLeadingSpacesfromString:_regionTextField.text],[LSUtils removeLeadingSpacesfromString:_postaltextField.text],[LSUtils removeLeadingSpacesfromString:_accountNumbertextField.text],[LSUtils removeLeadingSpacesfromString:_SSNTextField.text]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData* data, NSURLResponse* response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{                                                  if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                 options:0
                                                                                                                   error:NULL];
                                              [sharedUtils stopAnimator:self];
#ifdef DEBUG
                                              NSLog(@"Response:%@",responseDictionary);
                                              
#endif
                                              UIAlertController *controller;
                                              
                                              if(responseDictionary == nil){
                                                  
                                                     controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  controller = [sharedUtils showAlert:@"Sucess" withMessage:@"Saved successfully."];
                                                  [sharedUtils performSelector:@selector(getBankDetails) withObject:nil];
                                                  [self presentViewController:controller animated:YES completion:^{
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                      });
                                                  }];
                                              }
                                              else
                                              {
                                                  NSDictionary *messageDict = [[responseDictionary objectForKey:@"data"] objectAtIndex:0];
                                                  controller = [sharedUtils showAlert:@"Sorry" withMessage:[messageDict objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              UIAlertController*  controller;
                                              if(error!=nil){
                                                  controller = [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                                              }else{
                                                  controller = [sharedUtils showAlert:@"Sorry" withMessage:@"There is no response , please try again"];
                                              }
                                              [self presentViewController:controller animated:YES completion:nil];
                                          }
                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

@end
