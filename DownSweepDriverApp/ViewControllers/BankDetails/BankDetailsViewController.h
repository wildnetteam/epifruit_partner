//
//  BankDetailsViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 27/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "LSUtils.h"

@interface BankDetailsViewController : UIViewController <UITextFieldDelegate ,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *Savebutton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineTrailingConstarint;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *userFirtNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *accountNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *AccountNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *userLastNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextView *streetTextField;
@property (weak, nonatomic) IBOutlet UITextField *regionTextField;
@property (weak, nonatomic) IBOutlet UITextField *localityTextField;
@property (weak, nonatomic) IBOutlet UITextField *postalTextField;
@property (weak, nonatomic) IBOutlet UITextField *dobTextField;
@property (weak, nonatomic) IBOutlet UITextField *userLastnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *routingTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (weak, nonatomic) IBOutlet UILabel *dobLabelLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *localityLabel;
@property (weak, nonatomic) IBOutlet UILabel *regionameLabel;
@property (weak, nonatomic) IBOutlet UILabel *postalCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *routingNumberLabel;

@property (weak, nonatomic) NSString *accountNumber;


@end
