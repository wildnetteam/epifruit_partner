//
//  RatingViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 07/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "RatingViewController.h"
#import "RatingCellTableViewCell.h"
@interface RatingViewController ()

@end

@implementation RatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpUIElementsValues{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

static NSString *cellIdentifier = @"CellIdentifier";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
        RatingCellTableViewCell *cell = (RatingCellTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
            return cell;
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exeption Description:%@",exception.description);
    }
}

@end
