//
//  RatingViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 07/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSUtils.h"
#import "TPKeyboardAvoidingTableView.h"
@interface RatingViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;

@end
