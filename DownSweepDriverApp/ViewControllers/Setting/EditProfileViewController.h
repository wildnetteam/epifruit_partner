//
//  EditProfileViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 01/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+Scale.h"
#import "TransportModel.h"
#import "SettingModeView.h"

@interface EditProfileViewController : UIViewController <UIImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource , modeDelegate , UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *NameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *profileImagaeView;
@property (weak, nonatomic) IBOutlet UIImageView *nameImageView;
@property (weak, nonatomic) IBOutlet UIView *nameBackView;
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIView *emailBackView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;
@property (weak, nonatomic) IBOutlet UIView *phoneBackView;
@property (weak, nonatomic) IBOutlet UIButton *profileImageButton;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UITextView *parmanentAdressTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedCollectionHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeOfTransportViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *selectedCollectionView;
@property (retain, nonatomic) NSMutableArray *selectedValues;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeOfTransportLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *modeOfTransportTextField;


@end
