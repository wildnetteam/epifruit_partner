//
//  EditProfileViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 01/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "EditProfileViewController.h"
#import "ChangePasswordViewController.h"
#import "NotificationManager.h"
#import "PhoneOtpViewController.h"
#import "SelectedCell.h"
@interface EditProfileViewController (){
    BOOL ismobileNumberChanged;
    BOOL isviewAppearingThroughImagePicker;
    NSString *newUrlOfImage;
    NSMutableArray *availableTransportMode;
    NSString *imageBaseURL;
    NSMutableDictionary *detailDict;

}
@end

@implementation EditProfileViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    availableTransportMode = [[NSMutableArray alloc]init];
    [self setUpUIElements];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    if(!isviewAppearingThroughImagePicker){
        
        [self fetchUserDetails];
        [self disableTextField];
    }
    isviewAppearingThroughImagePicker= false;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)getUserCredentials :(NSDictionary*)driverDetailDictionary{

    _NameTextField.text = [driverDetailDictionary objectForKey:@"deliveryboy_name"] ;
    _emailTextField.text = [driverDetailDictionary objectForKey:@"deliveryboy_email"];
    _phoneTextField.text = [sharedUtils formatNumber:[driverDetailDictionary objectForKey:@"deliveryboy_mobile"]];
    _parmanentAdressTextView.text = [driverDetailDictionary objectForKey:@"address"];
    
///////////////// get Mode of transport/////////////
    NSMutableArray *modeArray = [driverDetailDictionary valueForKey:@"mode"];
    availableTransportMode = modeArray;
    _selectedValues = [[NSMutableArray alloc]init];
    [_selectedCollectionView reloadData];
    for(int index = 0 ; index < modeArray.count ;index++){
        
        NSDictionary *dict = [availableTransportMode objectAtIndex:index];
        if([[dict objectForKey:@"isused"]intValue] == 1){
            [_selectedValues addObject:dict];
        }
    }
    [self animateSelectedCollectionView];
    _profileImagaeView.tag = 0000; // when no profile image
    if([newUrlOfImage isEqualToString:[UserManager getUserImageURL]] || [LSUtils checkNilandEmptyString:newUrlOfImage]){
        
        //don't download
    }else{
        if([LSUtils checkNilandEmptyString:[UserManager getUserImageURL]]){
            
            [_spinner startAnimating];
            _profileImagaeView.tag = 1111;
            _profileImageButton.enabled = false;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                UIImage *image =  [self downloadProfileImage:[UserManager getUserImageURL]];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if(image!= nil)
                        _profileImagaeView.image = image;
                    [_spinner stopAnimating];
                    _profileImageButton.enabled = true;
                });
            });
        }
    }
}

-(void)fetchUserDetails{

    detailDict = [[NSMutableDictionary alloc]init];
    if([LSUtils isNetworkConnected])
    {

        [sharedUtils startAnimator:self ];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *param = [NSString stringWithFormat:@"%@&deliveryboy_id=%@",getdeliveryboyprofiledetail,[UserManager getUserID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self ];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  if(responseDict == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                     imageBaseURL = [responseDict valueForKey:@"deliveryboy_imageurl"];
                                                        NSDictionary *driverDict = [[responseDict valueForKey:@"data"] objectAtIndex:0];
                                                      [UserManager saveUserCredentials:[NSString stringWithFormat:@"%@/%@",imageBaseURL, [driverDict valueForKey:@"deliveryboy_image"]] withvar:k_UserImageURL];
                                                      detailDict = [driverDict mutableCopy];
                                                      [self getUserCredentials:detailDict];

                                                  }else  {
                                                      UIViewController *controller =  [sharedUtils showAlert:@"" withMessage:[responseDict objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:true completion:nil];
                                                
                                                  }
                                              }
                                              else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }

                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)enableTextField{
    
    _NameTextField.userInteractionEnabled =true;
    _phoneTextField.userInteractionEnabled = true;
    _profileImageButton.userInteractionEnabled = true;
    _parmanentAdressTextView.userInteractionEnabled = true;
    _modeOfTransportTextField.userInteractionEnabled = true;

    _saveButton.hidden = false;
    _separatorImageView.hidden = true;
    
    //To Hide
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItems = nil;
    
    [_NameTextField becomeFirstResponder];
    //set cross grey for editable field
    [ self.nameImageView setImage:[UIImage imageNamed:@"cross_grey"]];
    _nameImageView.contentMode = UIViewContentModeCenter;
    [ self.phoneImageView setImage:[UIImage imageNamed:@"cross_grey"]];
    _phoneImageView.contentMode = UIViewContentModeCenter;
    
}

-(void)disableTextField{
    
    _NameTextField.userInteractionEnabled = false;
    _phoneTextField.userInteractionEnabled = false;
    _emailTextField.userInteractionEnabled =false;
    _profileImageButton.userInteractionEnabled = false;
    _parmanentAdressTextView.userInteractionEnabled = false;
    _modeOfTransportTextField.userInteractionEnabled = false;
    _separatorImageView.hidden = false;
    _saveButton.hidden = true;
    //To Show
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 5;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"editprofile"] style:UIBarButtonItemStylePlain target:self action:@selector(editProfile:)] , nil]];}

#pragma mark - TextFieldDelegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == _modeOfTransportTextField){
        UIButton *dimView;
        [dimView removeFromSuperview];
        dimView = nil;
        dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
        SettingModeView * viewPop= [[SettingModeView alloc] init];
        viewPop.selectionsDel = self;
        viewPop.modeOfTransportArray = [NSArray arrayWithArray:availableTransportMode];
        viewPop.selectedIndexPathArray = [[NSMutableArray arrayWithArray:_selectedValues] mutableCopy];
        viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                         }];
        [UIView commitAnimations];
        viewPop.backgroundColor=[UIColor whiteColor];
        [dimView addSubview:viewPop];
        [self.view addSubview:dimView];
        [[self view] endEditing:true];
        return false;
    }
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_NameTextField) {
        
        UIButton *nameClearButton = [_NameTextField valueForKey:@"_clearButton"];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.NameTextField.text length]>0) {
            self.nameImageView.hidden=true;
            _nameBackView.hidden = true;
        }
    }
    
    if (textField==_emailTextField) {
        UIButton *emailClearButton = [_emailTextField valueForKey:@"_clearButton"];
        [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.emailTextField.text length]>0) {
            self.emailImageView.hidden=true;
            _emailBackView.hidden = true;
        }
    }
    
    if (textField==_phoneTextField) {
        
        UIButton *phoneClearButton = [_phoneTextField valueForKey:@"_clearButton"];
        [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.phoneTextField.text length]>0) {
            self.phoneImageView.hidden=true;
            _phoneBackView.hidden = true;
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.nameImageView.hidden=false;
    _nameBackView.hidden = false;
    self.emailImageView.hidden=false;
    _emailBackView.hidden = false;
    self.phoneImageView.hidden=false;
    _phoneBackView.hidden = false;
    
    [ self.nameImageView setImage:[UIImage imageNamed:@"name"]];
    [ self.phoneImageView setImage:[UIImage imageNamed:@"phone"]];
    
    if (textField==_NameTextField)
    {
        if(!([_NameTextField.text length]>0))
        {
            [sharedUtils placeholderSize:_NameTextField :@"Enter your full name..."];
        }
    }
    if (textField==_phoneTextField)
    {
        if(!([_phoneTextField.text length]>0))
        {
            [sharedUtils placeholderSize:_phoneTextField :@"Enter your number..."];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    if(textField == self.NameTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.nameImageView.hidden = true;
            self.nameBackView.hidden = true;
        }
        
        if (([self.NameTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.nameImageView.hidden = false;
            self.nameBackView.hidden = false;
            return false;
        }
        
        if(_NameTextField.text.length > 28 ){
            
            
            [[NotificationManager notificationManager] displayMessage:@"Name must be of length 2 to 28." withType:Negative];
            
            return false;
        }
        return true;
    }
    
    if(textField == self.emailTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.emailImageView.hidden = true;
            self.emailBackView.hidden = true;
        }
        
        if (([self.emailTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.emailImageView.hidden = false;
            self.emailBackView.hidden = false;
        }
        
    }
    
    if(textField == _phoneTextField ) {
        
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.phoneImageView.hidden = true;
            self.phoneBackView.hidden =true;
        }
        
        if (([self.phoneTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.phoneImageView.hidden = false;
            self.phoneBackView.hidden = false;
            
        }
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }
        
        
        int length = (int)[sharedUtils getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [sharedUtils formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [sharedUtils formatNumber:textField.text];

            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    return YES;
}


- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == _NameTextField){
        [_phoneTextField becomeFirstResponder];
    }
    if(textField == _phoneTextField){
   //     [_notificationSwitch becomeFirstResponder];
        [_phoneTextField resignFirstResponder];
    }
    
    return true;
}
-(UIImage*)downloadProfileImage:(NSString*)picUrlString {
    
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:picUrlString]]];
    return image;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:true];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}
-(void)setUpUIElements{
    
    self.title = @"";
    
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 5;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"editprofile"] style:UIBarButtonItemStylePlain target:self action:@selector(editProfile:)] , nil]];
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_NameTextField];
    [LSUtils addPaddingToTextField:_emailTextField];
    [LSUtils addPaddingToTextField:_phoneTextField];
    [LSUtils addPaddingToTextField:_modeOfTransportTextField];
    [sharedUtils placeholderSize:_NameTextField :@"Enter your full name..."];
    [sharedUtils placeholderSize:_phoneTextField :@"Enter your number..."];
    [sharedUtils placeholderSize:_modeOfTransportTextField :@"Select mode of transportation..."];

    [self setprofileImagefarme];
}

-(void)setprofileImagefarme{
    
    self.profileImagaeView.layer.cornerRadius = self.profileImagaeView.frame.size.width /2.0;
    self.profileImagaeView.layer.masksToBounds = YES;
    _profileImagaeView.clipsToBounds = YES;
}

- (void)changePasswordTappedGesture:(UITapGestureRecognizer *)gestureRecognizer {
    
    [[self view] endEditing:true];
    ChangePasswordViewController *forgotPasswordVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"ChangePasswordViewController"];
    [self.navigationController pushViewController:forgotPasswordVC animated:YES ];
}

-(void)backButtonClicked:(id)sender{
    
    [[self view] endEditing:true];
    [self disableTextField];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)editProfile:(id)sender{
    
    [self enableTextField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)updateProfilePic:(id)sender {
    
    [self openImageOption];
}

//Give user a option if he wants photo from gallery or take fro camera
-(void)openImageOption {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //open another viewcontroller
                                                       [self goToCameraView];
                                                       
                                                   }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self getPhotoLibrary];
                                                      }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert.popoverPresentationController setPermittedArrowDirections:0];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(sharedUtils.screenWidth / 2.0 + 20, sharedUtils.screenHeight / 2.0 , 1.0, 1.0);
    
    [self presentViewController:alert animated:YES completion:nil];
}

//take a snap from cemera
//go to another viewcontroller for clicking photo
-(void)goToCameraView {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIViewController *controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Device has no camera"];
        [self presentViewController:controller animated:NO completion:nil];
        
        
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = (id)self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}

//choose photo from gallery
-(void)getPhotoLibrary {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.allowsEditing = NO;
    imagePicker.delegate = (id)self;
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage ];
    imagePicker.navigationBar.translucent = false;
    [self presentViewController:imagePicker animated:YES completion:nil];;
}

#pragma mark UIImpagePicker Delegate

//this delegate is called when photo is selected from gallery
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ( [mediaType isEqualToString:@"public.image" ]) {
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [self setprofileImagefarme];
        isviewAppearingThroughImagePicker= true;
        _profileImagaeView.image = nil;
        UIImage *resizedImage =  [image scaleToSize:ProfileImageResizeScale];
        _profileImagaeView.image = [resizedImage fixOrientation];
        _profileImagaeView.tag = 1111;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)updateAccountSetting:(id)sender {
    
    if([self validatetextField]){
        
        [self CheckIfLisenceNeeded];

    }
}
-(BOOL)validatetextField{
    
    if(![LSUtils checkNilandEmptyString:_NameTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Name can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_phoneTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Phone Number can not be blank." withType:Negative];
        return false;
        
    }
    
    if([sharedUtils getLength:_phoneTextField.text]<10){
        [[NotificationManager notificationManager] displayMessage:@"Phone Number length can not be less than 10." withType:Negative];
        return false;
        
    }
    
    if(_NameTextField.text.length<2 || _NameTextField.text.length >28){
        
        [[NotificationManager notificationManager] displayMessage:@"Name  must be range in 2 to 28." withType:Negative];
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_parmanentAdressTextView.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Address can not be blank." withType:Negative];
        return false;
        
    }
    return true;
}


-(void)updateSetting:(NSString*)params{
    
    if([LSUtils isNetworkConnected])
    {
        self.view.userInteractionEnabled = NO;
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              
                                              self.view.userInteractionEnabled = YES;
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                     options:0 error:NULL];
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
#endif
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSMutableArray *arrayOfdata ;
                                                      
                                                      arrayOfdata = [responseDictionary valueForKey:@"data"];
                                                      
                                                      if([[[arrayOfdata objectAtIndex:0]valueForKey:@"ismobilenoupdated"] intValue] == 1){
                                                          
                                                          PhoneOtpViewController *otpVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"PhoneOtpViewController"];
                                                          otpVC.updatedPhonenumber = _phoneTextField.text;
                                                          otpVC.otp = [[arrayOfdata objectAtIndex:0] valueForKey:@"otpcode"];
                                                          [self.navigationController pushViewController:otpVC animated:YES ];
                                                      }
                                                      else{
                                                          
                                                          UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Profile updated successfully."];
                                                          
                                                          [self disableTextField];
                                                          [self fetchUserDetails];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                  }
                                                  else {
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  
                                              }else {
                                              if(error!=nil){
                                                  UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }else{
                                                  UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Some error occured"];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}


#pragma mark - mode of transport related stuff
-(void)animateSelectedCollectionView{
    
    if (_selectedValues.count > 0) {
        
        self.selectedCollectionHeightConstrain.constant = 55;
        self.modeOfTransportLabelHeightConstraint.constant = -15;
        
    }else{
        self.selectedCollectionHeightConstrain.constant = 0;
        self.modeOfTransportLabelHeightConstraint.constant = 0;
        
    }
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        if(_selectedValues.count > 0){
            [sharedUtils placeholderSize:_modeOfTransportTextField :@"Change..."];
        }else{
            [sharedUtils placeholderSize:_modeOfTransportTextField :@"Select mode of transportation..."];
        }
        
    }];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  _selectedValues.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary* object = [_selectedValues objectAtIndex:indexPath.row];
    
    SelectedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SelectedCell" forIndexPath:indexPath];
    
    cell.devName.text = [[object objectForKey:@"transportation_mode"] capitalizedString];
    [cell.devName sizeToFit];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary* object = [_selectedValues objectAtIndex:indexPath.row];
    
    CGSize calCulateSizze =[[object objectForKey:@"transportation_mode"] sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:18.0]}];
    calCulateSizze.width = calCulateSizze.width+ 20;
    calCulateSizze.height = calCulateSizze.height + 20;
    return calCulateSizze;
}

#pragma mark - Delegate to show seleected modeof transport

-(void)saveTransportMode:(NSMutableArray *)selectedNewValues{
    
    //   Make a local copy of the array, because an array cannot be mutated as it is enumerated
    @try {
        
        _selectedValues = selectedNewValues;
        [self.selectedCollectionView reloadData];
        [self animateSelectedCollectionView];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        ;
    } @finally {
        ;
    }
}


-(void)CheckIfLisenceNeeded {
    
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:@"You have selected heavy vehicle"
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:13], NSFontAttributeName, nil]];
    
    NSAttributedString *alertTitle =    [[NSAttributedString alloc]
                                         initWithString:@"Enter License Number"
                                         attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#232323"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Semibold" size:16], NSFontAttributeName, nil]];
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       
                                   }];
    
   UIAlertAction* okAction = [UIAlertAction
                actionWithTitle:NSLocalizedString(@"OK", @"OK")
                style:UIAlertActionStyleDefault
                handler:^(UIAlertAction *action)
                {
                    UITextField *temp = alertController.textFields.firstObject;
                    temp.tag = 101;
                    temp.delegate = self;
                    [self updateprofile :temp.text];
                    
                }];
    
    [alertController setValue:alertmessage forKey:@"attributedMessage"];
    [alertController setValue:alertTitle forKey:@"attributedTitle"];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter license Number";
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeASCIICapable;
        textField.text = [detailDict objectForKey:@"deliveryboy_licenceno"];

        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    // okAction.enabled = NO;
    BOOL flag = false;
    for (NSDictionary*obj in _selectedValues) {
        if([[obj objectForKey:@"licence_required"]intValue]){
            flag = true;
            break;
        }else{
            flag = false;
        }
    }
    if(flag == true){
        
        [self  presentViewController:alertController animated:NO completion:Nil];
        
    }else{
        
        [self updateprofile :@""];
    }
    
}
-(void)updateprofile :(NSString*) licenseText {
    
    NSMutableString *idString = [[NSMutableString alloc]init];
    
    for ( NSDictionary *model in _selectedValues ) {
        [idString appendFormat:@"%@,",[model objectForKey:@"id"]];
    }
    
   NSString *params;
   NSString *profileBase64 = _profileImagaeView.tag == 0000?@"":[UIImagePNGRepresentation(_profileImagaeView.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
   ismobileNumberChanged = false;
   
    if([[UserManager getUserPhoneNumber] isEqualToString:[sharedUtils formatNumber:_phoneTextField.text]]){
        
        if([licenseText isEqualToString:@""]){
            
            params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&deliveryboy_name=%@&deliveryboy_mobile=""&deliveryboy_image=%@&address=%@&ismobilenoupdated=2&transport_id=%@&is_mode_updated=1&address_id=1",UpdateProfile,[UserManager getUserID],[LSUtils removeLeadingSpacesfromString:_NameTextField.text],profileBase64,[LSUtils removeLeadingSpacesfromString:_parmanentAdressTextView.text],[idString substringToIndex:idString.length-(idString.length>0)]];
            
        }else
            
            params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&deliveryboy_name=%@&deliveryboy_mobile=""&deliveryboy_image=%@&address=%@&deliveryboy_licenceno=%@&ismobilenoupdated=2&transport_id=%@&is_mode_updated=1&address_id=1",UpdateProfile,[UserManager getUserID],[LSUtils removeLeadingSpacesfromString:_NameTextField.text],profileBase64,[LSUtils removeLeadingSpacesfromString:_parmanentAdressTextView.text],licenseText,[idString substringToIndex:idString.length-(idString.length>0)]];
        
        [self updateSetting:params];
        
    }else{
        ismobileNumberChanged = true;
        
        
        NSString *mobile =  [sharedUtils formatNumber:_phoneTextField.text];
        NSString *mobileNumberWithCountryCode = [@"+01" stringByAppendingString:mobile];
        mobileNumberWithCountryCode = [mobileNumberWithCountryCode stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"];
        
        if([licenseText isEqualToString:@""]){
            
            params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&deliveryboy_name=%@&deliveryboy_mobile=%@&deliveryboy_image=%@&address=%@&ismobilenoupdated=1&transport_id=%@&is_mode_updated=1&address_id=1",UpdateProfile,[UserManager getUserID],[LSUtils removeLeadingSpacesfromString:_NameTextField.text],mobileNumberWithCountryCode,profileBase64,[LSUtils removeLeadingSpacesfromString:_parmanentAdressTextView.text],[idString substringToIndex:idString.length-(idString.length>0)]];
        }else
            params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&deliveryboy_name=%@&deliveryboy_mobile=%@&deliveryboy_image=%@&address=%@&deliveryboy_licenceno=%@&ismobilenoupdated=1&transport_id=%@&is_mode_updated=1&address_id=1",UpdateProfile,[UserManager getUserID],[LSUtils removeLeadingSpacesfromString:_NameTextField.text],mobileNumberWithCountryCode,profileBase64,[LSUtils removeLeadingSpacesfromString:_parmanentAdressTextView.text],licenseText,[idString substringToIndex:idString.length-(idString.length>0)]];
        
        NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                               initWithString:[NSString stringWithFormat:@"Is this correct phone number?\n%@\n\nWe will send you a verification code by SMS",_phoneTextField.text]
                                               attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:16], NSFontAttributeName, nil]];
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@""
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       // update data sourc
                                       [self updateSetting:params];
                                   }];
        
        [alertController setValue:alertmessage forKey:@"attributedTitle"];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self  presentViewController:alertController animated:NO completion:Nil];
        
    }
}


@end
