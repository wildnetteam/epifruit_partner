//
//  PhoneOtpViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 26/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "PhoneOtpViewController.h"
#import "OTPViewController.h"
#import "NotificationManager.h"
#import "SettingsViewController.h"
@interface PhoneOtpViewController ()

@end

@implementation PhoneOtpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)setUpUIElements{
    
    self.title = @"";
    
    UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(0, 0,100, 50)];
    [cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
        self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    UITapGestureRecognizer  *requestOtp   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(requestOtpClicked:)];
    [_resendOtpLabel addGestureRecognizer:requestOtp];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NotificationManager notificationManager] displayMessage:_otp withType:Normal andDuration:3.0];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self setNeedsStatusBarAppearanceUpdate];

}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)cancelAction:(id)sender {
    
    [[self view] endEditing:true];
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString*)getCode{
    
    NSString *code= [NSString stringWithFormat:@"%@%@%@%@",_firstNo.text,_secondNo.text,_thirdNo.text,_fourthNo.text];
    return code;
}

-(BOOL)ValidateTextField{
    
    if((![LSUtils checkNilandEmptyString:_firstNo.text]) || (![LSUtils checkNilandEmptyString:_secondNo.text]) || (![LSUtils checkNilandEmptyString:_thirdNo.text]) || (![LSUtils checkNilandEmptyString:_fourthNo.text])){
        
        [[NotificationManager notificationManager] displayMessage:@"Fill Otp fields." withType:Negative];
        return false;
    }
    return true;
    
}
- (IBAction)verifyOtp:(id)sender  {
    
    if([self ValidateTextField]) {
        
        if([LSUtils isNetworkConnected])
        {
            self.view.userInteractionEnabled = NO;
            
            NSString *params = [NSString stringWithFormat:@"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=otpcodeverification&eliverboy_id=%@&otp_code=%@",[UserManager getUserID],[self getCode]];
            
            [sharedUtils  startAnimator:self];
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  [sharedUtils stopAnimator:self];
                                                  
                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                        options:0
                                                                                                                          error:NULL];
                                                      
#ifdef DEBUG
                                                      NSLog(@"Response:%@",responseDictionary);
                                                      
                                                      
#endif
                                                      UIAlertController *controller ;
                                                      
                                                      
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          //                                                      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success"  message:@"Otp vaerified."   preferredStyle:    UIAlertControllerStyleAlert];
                                                          //
                                                          //                                                      UIAlertAction *closeAction = [UIAlertAction
                                                          //                                                                                    actionWithTitle:NSLocalizedString(@"Ok", @"")
                                                          //                                                                                    style:UIAlertActionStyleCancel
                                                          //                                                                                    handler:^(UIAlertAction *action)
                                                          //                                                                                    {
                                                          //                                                                                        [self.navigationController popViewControllerAnimated:true];
                                                          //                                                                                    }];
                                                          //
                                                          //                                                      [alertController addAction:closeAction];
                                                          //
                                                          //                                                      [self presentViewController:alertController animated:YES completion:nil];
                                                          //
                                                          [UserManager saveUserCredentials:_updatedPhonenumber withvar:k_UserPhoneNumber];
                                                          NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                                                          for (UIViewController *aViewController in allViewControllers) {
                                                              if ([aViewController isKindOfClass:[SettingsViewController class]]) {
                                                                  [self.navigationController popToViewController:aViewController animated:NO];
                                                              }
                                                          }
                                                      }
                                                      else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]) {
                                                          
                                                          controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Otp could not be not verified."];
                                                          [self presentViewController:controller animated:YES completion:nil];                                                      self.view.userInteractionEnabled = YES;
                                                          
                                                      }
                                                  }
                                              });
                                          }];
            [task resume];
            
        }else{
            
            UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
            
            [self presentViewController:controller animated:YES completion:nil];
            
        }
    }
}

-(void)requestOtpClicked:(id)sender{
    
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == _firstNo){
        [_secondNo becomeFirstResponder];
    }
    else if(textField == _secondNo){
        [_thirdNo becomeFirstResponder];
    }
    else  if(textField == _thirdNo){
        [_fourthNo becomeFirstResponder];
    }
    
    return NO;
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//
//    if(textField.text.length >= 1){
//        if(textField == _firstNo){
//            [_secondNo becomeFirstResponder];
//        }
//        else if(textField == _secondNo){
//            [_thirdNo becomeFirstResponder];
//        }
//        else  if(textField == _thirdNo){
//            [_fourthNo becomeFirstResponder];
//        }
//    }
//    return YES;
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength >= 1) {
        if(textField == _firstNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_secondNo afterDelay:0.2];
        }
        else if(textField == _secondNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_thirdNo afterDelay:0.2];
        }
        else if (textField == _thirdNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_fourthNo afterDelay:0.2];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == _fourthNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_thirdNo afterDelay:0.1];
        }
        else if (textField == _thirdNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_secondNo afterDelay:0.1];
        }
        else if (textField == _secondNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_firstNo afterDelay:0.1];
        }
    }
    return newLength <= 1;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}
// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}

@end

