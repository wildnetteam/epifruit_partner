//
//  SettingsViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 25/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "SettingsViewController.h"
#import "NotificationManager.h"
@interface SettingsViewController ()
{
   BOOL ismobileNumberChanged;
    BOOL isviewAppearingThroughImagePicker;
    NSString *newUrlOfImage;
    NSArray *array;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    [self setNeedsStatusBarAppearanceUpdate];

}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:true];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}
-(void)setUpUIElements{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];

    array = [NSArray arrayWithObjects:@"Basic Settings",@"Change Password", @"Notification" ,nil];
    
    _tableView.rowHeight = 60.0f;
}
-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.textLabel.font = [UIFont fontWithName:@"Lato-Regular" size:16];
    cell.textLabel.text =[array objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  array.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row ==0){
        
        UIViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"EditProfileViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }else  if(indexPath.row ==1){
       
        UIViewController *changePasswordVC = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"ChangePasswordViewController"];
        [self.navigationController pushViewController:changePasswordVC animated:YES ];
    }else{
        UIViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NotificationViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }

}


@end
