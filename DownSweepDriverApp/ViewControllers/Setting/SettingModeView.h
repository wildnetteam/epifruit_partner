//
//  SettingModeView.h
//  DownSweepDriverApp
//
//  Created by Arpana on 04/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol modeDelegate <NSObject>
@optional
-(void)saveTransportMode:(NSMutableArray *)selectedValues;
-(void)cancel:(id)sender;
-(void)removePopedView;
@end

@interface SettingModeView : UIView

@property (strong, nonatomic) UITableView* tableView;

@property (strong, nonatomic) UIStackView *popViewTopBar; // The bar at the top of the picker view
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UILabel *popViewTopBarTitle;
//@property (strong, nonatomic)NSString * modeString;


@property (assign , nonatomic) id<modeDelegate> selectionsDel;
@property (nonatomic, strong) NSMutableArray *selectedIndexPathArray;
@property (nonatomic, strong) NSArray *modeOfTransportArray;
@end
