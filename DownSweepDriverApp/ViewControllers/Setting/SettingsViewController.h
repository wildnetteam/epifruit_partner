//
//  SettingsViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 25/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+Scale.h"

@interface SettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource , UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
