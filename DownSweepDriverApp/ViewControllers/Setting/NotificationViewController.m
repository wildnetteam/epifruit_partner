//
//  NotificationViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 04/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "NotificationViewController.h"
#import "UserManager.h"
@interface NotificationViewController ()

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)setUpUIElements{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor blackColor];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchOnOrOffNotifications:(id)sender {
    
    UISwitch *switchBtn = (UISwitch*)sender;
    [self updateNotificationStatus:[NSString stringWithFormat:@"%d",switchBtn.on]];
}

-(void)updateNotificationStatus:(NSString*)value{
    
    NSString* params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&notification_status=%@",setnotificationstatus,[UserManager getUserID],value];
    
    if([LSUtils isNetworkConnected])
    {
        //  _notificationSwitch.userInteractionEnabled = NO;
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0 error:NULL];
#ifdef DEBUG
                                                  NSLog(@"Response:%@",responseDictionary);
#endif
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Notification status changed."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]) {
                                                      
                                                      NSLog(@"could not be updated");
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"218"]) {
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Could not update status"];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
}
-(void)backButtonClicked:(id)sender{
    
    [[self view] endEditing:true];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
