//
//  OTPViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 31/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import "OTPViewController.h"
#import "NotificationManager.h"
#import "CompletOrderViewController.h"
#import "UITextView+Placeholder.h"

@interface OTPViewController ()
{
    
    __weak IBOutlet NSLayoutConstraint *commentTextViewTopConstraint;
    __weak IBOutlet NSLayoutConstraint *verifyOTPHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *commentHeightConstraint;
    BOOL isManuual;
}
@end

@implementation OTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElements];
}

-(void)setUpUIElements{
    
    self.title = @"";
    
    UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(0, 0,100, 50)];
    [cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    UITapGestureRecognizer  *requestOtp   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(requestOtpClicked:)];
    [_resendOtpLabel addGestureRecognizer:requestOtp];
    [_commentTextView.placeholderLabel setValue:[UIFont fontWithName:@"Lato-Light" size:15.0] forKeyPath:@"font"];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NotificationManager notificationManager] displayMessage:_otp withType:Normal andDuration:3.0];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setNeedsStatusBarAppearanceUpdate];
    //To complete order manually If otp does not appear for customer.
    //Driver will complete order manually with adding reason for it
    isManuual = false;
    [_manualCompletionButton setTitle:@"" forState:UIControlStateNormal];
    commentHeightConstraint.constant = 50;
    _commentTextView.delegate = self;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)cancelAction:(id)sender {
    
    [[self view] endEditing:true];
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(NSString*)getCode{
    
    NSString *code= [NSString stringWithFormat:@"%@%@%@%@",_firstNo.text,_secondNo.text,_thirdNo.text,_fourthNo.text];
    return code;
}

-(BOOL)ValidateTextField{
    
    if(isManuual){
        
        if(![LSUtils checkNilandEmptyString:_commentTextView.text]){
            
            [[NotificationManager notificationManager] displayMessage:@"Please enter reason." withType:Negative andDuration:3.0];
            return false;
        }
        
    }else{
        if((![LSUtils checkNilandEmptyString:_firstNo.text]) || (![LSUtils checkNilandEmptyString:_secondNo.text]) || (![LSUtils checkNilandEmptyString:_thirdNo.text]) || (![LSUtils checkNilandEmptyString:_fourthNo.text])){
            
              [[NotificationManager notificationManager] displayMessage:@"Please enter otp." withType:Negative andDuration:3.0];
            return false;
        }
        if(![_otp isEqualToString:[self getCode]]){
            
            [[NotificationManager notificationManager] displayMessage:@"otp mismatch." withType:Negative andDuration:3.0];
            return false;
        }
    }
    return true;

}
- (IBAction)verifyOtp:(id)sender  {

    if([self ValidateTextField]) {

        [self makePayment];

    }
}

-(void)requestOtpClicked:(id)sender{
    
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == _firstNo){
        [_secondNo becomeFirstResponder];
    }
   else if(textField == _secondNo){
        [_thirdNo becomeFirstResponder];
    }
  else  if(textField == _thirdNo){
        [_fourthNo becomeFirstResponder];
    }
    
    return NO;
}
- (IBAction)manuallyCompleteOrder:(id)sender {
    
    if(isManuual){
        
        [_manualCompletionButton setTitle:@"" forState:UIControlStateNormal];
        isManuual = false;
        commentHeightConstraint.constant = 50;
        _commentTextView.text = @"";
        _enterOtpLabel.hidden = false;
        _textFieldView.hidden = false;
        _commentTextView.userInteractionEnabled = false;
        commentTextViewTopConstraint.constant = 60;
        [UIView animateWithDuration:0.5 animations:^{
            verifyOTPHeightConstraint.constant = 88;
            [self.view layoutIfNeeded];

        } completion:^(BOOL finished) {

        }];
        
    }else{
        [_manualCompletionButton setTitle:@"check" forState:UIControlStateNormal];
        _enterOtpLabel.hidden = true;
        _textFieldView.hidden = true;
        _commentTextView.userInteractionEnabled = true;
        isManuual = true;
        commentTextViewTopConstraint.constant = 0;
        commentTextViewTopConstraint.constant = 0;
        commentHeightConstraint.constant = 80;
        [UIView animateWithDuration:0.5 animations:^{
            verifyOTPHeightConstraint.constant = 0;
            [self.view layoutIfNeeded];
        }];
        
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if (textView == _commentTextView)
    {
        if([_commentTextView.text length] == 0){
            UIColor *color = [UIColor lightGrayColor];
            _commentTextView.placeholderLabel.textColor = color;
            
            [_commentTextView.placeholderLabel setValue:[UIFont fontWithName:@"Lato-Light" size:15.0] forKeyPath:@"font"];
            
            _commentTextView.placeholder =@"Enter your reason for manual completion...";
        }
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    _commentTextView.placeholder =@"";
    [_commentTextView setValue:[UIFont fontWithName:@"Lato-Regular" size:16.0] forKeyPath:@"font"];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength >= 1) {
        if(textField == _firstNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_secondNo afterDelay:0.2];
        }
        else if(textField == _secondNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_thirdNo afterDelay:0.2];
        }
        else if (textField == _thirdNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_fourthNo afterDelay:0.2];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == _fourthNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_thirdNo afterDelay:0.1];
        }
        else if (textField == _thirdNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_secondNo afterDelay:0.1];
        }
        else if (textField == _secondNo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_firstNo afterDelay:0.1];
        }
    }
    return newLength <= 1;
}


-(void)makePayment{

    if([LSUtils isNetworkConnected]) {
        
        [sharedUtils startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSString *params;
        
        params = isManuual == true?[NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=makedeliveryboypayment&deliveryboy_id=%@&order_id=%@&ismannual=1&deliveryboy_comment=%@",[UserManager getUserID],_orderId,_commentTextView.text]:[NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=makedeliveryboypayment&deliveryboy_id=%@&order_id=%@&ismannual=2",[UserManager getUserID],_orderId];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:0
                                                                                                             error:NULL];
#ifdef DEBUG

                                                  NSLog(@"Response:%@",Response);
#endif
                                                  if(Response == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      CompletOrderViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"CompletOrderViewController"];
                                                      [self.navigationController pushViewController:vc animated:true];
                                                  }else  if ([[Response objectForKey:@"code"]isEqualToString:@"211"]){
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:@"Vendor card detail not found or order not delivered.."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  else {
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:[Response  objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }
                                              else{
                                                  
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}
// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}

@end
