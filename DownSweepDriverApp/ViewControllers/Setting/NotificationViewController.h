//
//  NotificationViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 04/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSUtils.h"
@interface NotificationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;
@property (weak, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notificationViewHeightConstraint;

@end
