//
//  PhoneOtpViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 26/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneOtpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *firstNo;
@property (weak, nonatomic) IBOutlet UITextField *secondNo;
@property (weak, nonatomic) IBOutlet UITextField *thirdNo;
@property (weak, nonatomic) IBOutlet UITextField *fourthNo;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;
@property (weak, nonatomic) IBOutlet UIView *textFieldView;
@property (weak, nonatomic) IBOutlet UILabel *resendOtpLabel;
@property (nonatomic, retain) NSString* updatedPhonenumber;
@property (nonatomic, retain) NSString* otp;
@end
