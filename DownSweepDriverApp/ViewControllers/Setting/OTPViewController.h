//
//  OTPViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 31/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPViewController : UIViewController <UITextFieldDelegate , UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIButton *manualCompletionButton;
@property (weak, nonatomic) IBOutlet UITextField *firstNo;
@property (weak, nonatomic) IBOutlet UITextField *secondNo;
@property (weak, nonatomic) IBOutlet UITextField *thirdNo;
@property (weak, nonatomic) IBOutlet UITextField *fourthNo;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;
@property (weak, nonatomic) IBOutlet UIView *textFieldView;
@property (weak, nonatomic) IBOutlet UILabel *resendOtpLabel;
@property (weak, nonatomic) IBOutlet UILabel *enterOtpLabel;
@property (nonatomic, retain) NSString* updatedPhonenumber;
@property (nonatomic, retain) NSString* otp;
@property (retain , nonatomic) NSString *orderId;

@end
