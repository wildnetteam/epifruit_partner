//
//  WaitingViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "WaitingViewController.h"
#import "LSUtils.h"
#import "LoginViewController.h"
@interface WaitingViewController ()
@property (strong, nonatomic) IBOutlet UILabel *waitingLabel;

@end

@implementation WaitingViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    _waitingLabel.text = _waitingmessage;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUpUIElementsValues{
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationController.navigationBar.translucent = true;
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                      [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                      FONT_NavigationBar, NSFontAttributeName, nil]];
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cross_white"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor whiteColor];
    
    [self.navigationItem setRightBarButtonItem:barButtonItem];
    self.title = @"";
}

-(void)backButtonClicked:(id)sender{
    
    LoginViewController *login = [sharedUtils.mainStoryboard
                                                        instantiateViewControllerWithIdentifier: @"LoginViewController"];
    
            CATransition *animation = [CATransition animation];
            [animation setType:kCATransitionPush];
            [animation setDuration:.8];
            [animation setSubtype:kCATransitionFromTop];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [[login.view layer] addAnimation:animation forKey:nil];
    [self.navigationController pushViewController:login animated:true];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
