//
//  RegistrationViewController.h
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "UserManager.h"
#import "ModeOfTransportViewController.h"

@interface RegistrationViewController : UIViewController <UIActionSheetDelegate , UIImagePickerControllerDelegate , UITextFieldDelegate , modeSelectionDelegate , UICollectionViewDataSource, UICollectionViewDelegate ,UINavigationControllerDelegate>{
   
}

@property(nonatomic) NSString *name;
@property(nonatomic) NSString *email;
@property(nonatomic) NSString *phone;
@property(nonatomic) NSString *imgUrl;


@property (weak, nonatomic) IBOutlet UIImageView *nameImageView;
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;
@property (weak, nonatomic) IBOutlet UIImageView *confirmPasswordImageview;
@property (weak, nonatomic) IBOutlet UIImageView *refferalCodeImageview;
@property (weak, nonatomic) IBOutlet UIImageView *ssnNumberImageview;

@property (weak, nonatomic) IBOutlet UILabel *registerTitleLabel;
@property (retain, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *userEmailText;
@property (weak, nonatomic) IBOutlet UIView *userFirstNameText;

@property (weak, nonatomic) IBOutlet UIView *userPasswordText;
@property (weak, nonatomic) IBOutlet UIView *userConfirmPasswordNameText;
@property (weak, nonatomic) IBOutlet UIView *userMobileNumberText;
@property (weak, nonatomic) IBOutlet UIView *modeOfTransportView;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *nametextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *ssnNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *ssnNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *passwordlabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordtextField;
@property (weak, nonatomic) IBOutlet UILabel *confirmpasswordLabel;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextView *parmanentAdressTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedCollectionHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeOfTransportViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *selectedCollectionView;
@property (retain, nonatomic) NSMutableArray *selectedValues;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeOfTransportLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *modeOfTransportTextField;

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *refferalCodeLabel;
@property (weak, nonatomic) IBOutlet UITextField* refferalCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;

@end
