//
//  ModeOfTransportViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 15/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "ViewController.h"
#import "ModeOftransportTableViewCell.h"

@protocol modeSelectionDelegate <NSObject>
@optional
-(void)saveTransportMode:(NSMutableArray *)selectedValues;
-(void)cancel:(id)sender;
@end

@interface ModeOfTransportViewController : ViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, retain) NSMutableArray* modeOfTransportArray;
@property(nonatomic, retain) NSMutableArray* selectedIndexPathArray;
@property (assign , nonatomic) id<modeSelectionDelegate> selectionsDel;

@end
