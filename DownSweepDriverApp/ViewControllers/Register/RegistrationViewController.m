//
//  RegistrationViewController.m
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RegistrationViewController.h"
#import "UIImage+Scale.h"
#import <AVFoundation/AVFoundation.h>
#import "NotificationManager.h"
#import "UITextView+Placeholder.h"
#import "SelectedCell.h"
#import "ModeSelectionView.h"
#import "TransportModel.h"
#import "WaitingViewController.h"
#import "LicenseViewController.h"
@interface RegistrationViewController ()
{
   BOOL isProileImageSelected;
    __weak IBOutlet UIButton *acceptTermButton;
    BOOL termsAccepted;
    NSMutableArray *availableTransportMode;
    UIAlertAction *okAction;

}

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view setBackgroundColor:APP_BACKGROUND_COLOR];
    [self setUpUIElementsValues];
    self.nametextField.text = self.name;
    isProileImageSelected = false;
    _selectedValues = [[NSMutableArray alloc]init];
    if(self.imgUrl)
    {
        NSURL *url = [NSURL URLWithString:self.imgUrl];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        UIImage *tmpImage = [[UIImage alloc] initWithData:data];
        
        [self setprofileImagefarme];
        self.profileImageView.image =  tmpImage;
        isProileImageSelected = true;
    }
    self.emailTextField.text = self.email;
    self.phoneTextField.text = self.phone;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    [self startLocationUpdates];
    termsAccepted = false;
    [acceptTermButton setTitle:@"" forState:UIControlStateNormal];

    [_parmanentAdressTextView.placeholderLabel setValue:[UIFont fontWithName:@"Lato-Light" size:15.0] forKeyPath:@"font"];
    _parmanentAdressTextView.placeholderColor = [UIColor grayColor];
     _parmanentAdressTextView.textContainerInset = UIEdgeInsetsMake(5, 3.5, 0, 0);
    
    //get driver's current location
    [sharedUtils getCurrentLocationWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
        
        if(error!=nil){
            
            UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Could not find your location Please turn on location settings."];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
        if(success){
            //loction fetched
            NSLog(@"Lat: %f", sharedUtils.CURRENTLOC.latitude);
            NSLog(@"Long: %f", sharedUtils.CURRENTLOC.longitude);
        }
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)checkLocatonPermission{
    
    NSString *alertmessage ;
    if (![CLLocationManager locationServicesEnabled])
    {
        alertmessage = @"Please go to Settings and turn on Location Service for this app." ;
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Location Services Disabled!"
                                              message:alertmessage
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           //go back
                                       }];
        
        UIAlertAction *okayAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okayAction];
        
        [self  presentViewController:alertController animated:NO completion:Nil];
    }
    
}

-(void)setprofileImagefarme{
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width /2.0;
    self.profileImageView.layer.masksToBounds = YES;
     _profileImageView.clipsToBounds = YES;
}

-(void)setUpUIElementsValues{
    
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    self.title = @"";
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_nametextField];
    [LSUtils addPaddingToTextField:_emailTextField];
    [LSUtils addPaddingToTextField:_phoneTextField];
    [LSUtils addPaddingToTextField:_passwordtextField];
    [LSUtils addPaddingToTextField:_modeOfTransportTextField];
    [LSUtils addPaddingToTextField:_confirmPasswordTextField];
    [LSUtils addPaddingToTextField:_refferalCodeTextField];
    [LSUtils addPaddingToTextField:_ssnNumberTextField];

    _modeOfTransportViewHeightConstraint.constant = IS_IPHONE5?5:10;
    self.selectedCollectionHeightConstrain.constant = 0;

    if(!([_nametextField.text length]>0))
    {
        [sharedUtils placeholderSize:_nametextField :@"Enter your full name..."];
    }
        if(!([_emailTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_emailTextField :@"Enter your email id..."];
    }
    if(!([sharedUtils getLength:_phoneTextField.text]>0))
    {
        [sharedUtils placeholderSize:_phoneTextField :@"Enter your number..."];
    }
    if(!([_passwordtextField.text length]>0))
    {
        [sharedUtils placeholderSize:_passwordtextField :@"Enter your password..."];
    }
    if(!([_confirmPasswordTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_confirmPasswordTextField :@"Re enter your password..."];
    }
    if( _selectedValues.count <= 0)
    {
        [sharedUtils placeholderSize:_modeOfTransportTextField :@"Select mode of transportation..."];
    }
    if(!([_refferalCodeTextField.text length]>0)){
        
        [sharedUtils placeholderSize:_refferalCodeTextField :@"Enter your referral code..."];
    }
    if(!([_ssnNumberTextField.text length]>0)){
        
        [sharedUtils placeholderSize:_ssnNumberTextField :@"Enter your ssn number..."];
    }
    //Get it done in background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self getModeOfTransport];
    });
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, sharedUtils.screenWidth, 50)];
    // numberToolbar.backgroundColor =  [UIColor colorWithHexString:@"#3fcbe4"];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    numberToolbar.barTintColor =[UIColor whiteColor];// [UIColor colorWithHexString:@"#3fcbe4"];
    _phoneTextField.inputAccessoryView = numberToolbar;
    
}
- (IBAction)acceptTermsButtonClicked:(id)sender {
    
    if(termsAccepted){
        [acceptTermButton setTitle:@"" forState:UIControlStateNormal];
        termsAccepted = false;

    }else{
    [acceptTermButton setTitle:@"check" forState:UIControlStateNormal];
        termsAccepted = true;

    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    availableTransportMode = nil;
    [super viewWillDisappear:animated];
}

- (IBAction)uploadProfileImage:(id)sender {
    
    [self openImageOption];
}

//Give user a option if he wants photo from gallery or take fro camera
-(void)openImageOption {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //open another viewcontroller
                                                       [self goToCameraView];
                                                       
                                                   }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self getPhotoLibrary];
                                                      }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert.popoverPresentationController setPermittedArrowDirections:0];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(sharedUtils.screenWidth / 2.0 + 20, sharedUtils.screenHeight / 2.0 , 1.0, 1.0);
    
    [self presentViewController:alert animated:YES completion:nil];
}

//take a snap from cemera
//go to another viewcontroller for clicking photo
-(void)goToCameraView{

    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIViewController *controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Device has no camera"];
        [self presentViewController:controller animated:NO completion:nil];
        
        
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = (id)self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}

//choose photo from gallery
-(void)getPhotoLibrary {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.allowsEditing = NO;
    imagePicker.delegate = self;
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage ];
    imagePicker.navigationBar.translucent = false;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark UIImpagePicker Delegate

//this delegate is called when photo is selected from gallery
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ( [mediaType isEqualToString:@"public.image" ]) {
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [self setprofileImagefarme];
        isProileImageSelected = true;
        UIImage *resizedImage =  [image scaleToSize:ProfileImageResizeScale];
        _profileImageView.image= [resizedImage fixOrientation];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - Validation method

-(BOOL)validatetextField{
    
    [self checkLocatonPermission];

    if(isProileImageSelected == false){
        
        [[NotificationManager notificationManager] displayMessage:@"Please select profile photo." withType:Negative];
        return false;
    }
  
    NSCharacterSet* validCharecterSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];

    if(![LSUtils checkNilandEmptyString:_nametextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Name can not be blank." withType:Negative];
        return false;
    }
    
    if(  [_nametextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the name field." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_emailTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Email can not be blank." withType:Negative];
        return false;
        
    }
    
    if(![LSUtils validateEmail:_emailTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Email id is wrong." withType:Negative];
        return false;
        
    }
    
    if([LSUtils validateEmail:_emailTextField.text]){
        NSArray *Array = [_emailTextField.text componentsSeparatedByString:@"@"];
        if (Array.count>0) {
            
            NSString *localPartEmail = [Array firstObject];
            if(localPartEmail.length<3 || localPartEmail.length >= 28){
                
                [[NotificationManager notificationManager] displayMessage:@"Email id length must be range in 3 to 28." withType:Negative];
                return false;
            }
        }
    }
    if(![LSUtils checkNilandEmptyString:_ssnNumberTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"SSN number can not be blank." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_phoneTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Phone Number can not be blank." withType:Negative];
        return false;
        
    }
    if([sharedUtils getLength:_phoneTextField.text]<10){
        [[NotificationManager notificationManager] displayMessage:@"Phone Number length can not be less than 10." withType:Negative];
        return false;
    }
    if(_nametextField.text.length<2 || _nametextField.text.length >= 28){
        
        [[NotificationManager notificationManager] displayMessage:@"Name  must be range in 2 to 28." withType:Negative];
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_passwordtextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Password can not be blank." withType:Negative];
        return false;
    }
    if(  [_passwordtextField.text rangeOfCharacterFromSet:validCharecterSet].location == NSNotFound){
        [[NotificationManager notificationManager] displayMessage:@"Only Special characters should not be accepted by the password field." withType:Negative];
        return false;
    }
    if(_passwordtextField.text.length<6 || _passwordtextField.text.length >=16){
        
        [[NotificationManager notificationManager] displayMessage:@"Password must be range in 6 to 16." withType:Negative];
        return false;
    }
    if(![LSUtils checkNilandEmptyString:_confirmPasswordTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Confirm Password can not be blank." withType:Negative];
        return false;
    }
    if(![_passwordtextField.text isEqualToString:_confirmPasswordTextField.text ]){
        
        [[NotificationManager notificationManager] displayMessage:@"Password mismatch." withType:Negative];
        return false;
    }
    
    if(![LSUtils checkNilandEmptyString:_parmanentAdressTextView.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Parmanent address can not be blank." withType:Negative];
        return false;
    }
    
    if(_parmanentAdressTextView.text.length >= 120 ){
        
        [[NotificationManager notificationManager] displayMessage:@"Parmanent address should not be more than 120 characters." withType:Negative];
        return false;
    }
    
    if(_selectedValues.count ==0){
        
        [[NotificationManager notificationManager] displayMessage:@"Please select atleast one mode of transportation." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        return false;
    }
    
    if(!termsAccepted){
        [[NotificationManager notificationManager] displayMessage:@"Please accept Terms & Conditions." withType:Negative];
        return false;
    }
    if(sharedUtils.CURRENTLOC.latitude == 0 || sharedUtils.CURRENTLOC.longitude == 0){
        
        [[NotificationManager notificationManager] displayMessage:@"Your current location could not be identified, Please check your loaction settings." withType:Negative];
        return false;
    }
    
    return true;
}

- (IBAction)signUpButtonPressed:(id)sender {

    if([self validatetextField]) {
        
    [self registerUser];
    }
}

-(void)registerUser {

    NSMutableString *idString = [[NSMutableString alloc]init];
    
    for ( TransportModel *model in _selectedValues ) {
        [idString appendFormat:@"%@,", model.tId];
    }
    
    if([LSUtils isNetworkConnected]){
        
        NSString *mobile =  [sharedUtils formatNumber:_phoneTextField.text];
        NSString *mobileNumberWithCountryCode = [@"+01" stringByAppendingString:mobile];
        mobileNumberWithCountryCode = [mobileNumberWithCountryCode stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"];
        
        NSString *imgURL = isProileImageSelected ==true?[UIImagePNGRepresentation(_profileImageView.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]:@"";
        
        NSString* params = [NSString stringWithFormat:@"%@&deliveryboy_email=%@&deliveryboy_password=%@&deliveryboy_name=%@&deliveryboy_mobile=%@&deliveryboy_image=%@&deliveryboy_lat=%@&deliveryboy_long=%@&paddress=%@&transport_id=%@&refrence_code=%@&ssn_num=%@&is_save=1",SignUp_Service,[LSUtils removeLeadingSpacesfromString:_emailTextField.text],[LSUtils removeLeadingSpacesfromString:_passwordtextField.text],[LSUtils removeLeadingSpacesfromString:_nametextField.text],mobileNumberWithCountryCode,imgURL,[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.latitude],[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.longitude] ,_parmanentAdressTextView.text,[idString substringToIndex:idString.length-(idString.length>0)],[LSUtils removeLeadingSpacesfromString:_refferalCodeTextField.text],[LSUtils removeLeadingSpacesfromString:_ssnNumberTextField.text]];
        
        LicenseViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"LicenseViewController"];
        vc.swiftySideMenu.centerViewController = self.navigationController;
        vc.paramsForRegister = params;
        [self.navigationController pushViewController:vc animated:YES];
        
        //relase memory
        idString = nil;
        
    }else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)backButtonClicked:(id)sender{
    
    UIViewController *login = [sharedUtils.mainStoryboard  instantiateViewControllerWithIdentifier: @"LoginViewController"];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:login animated:NO];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    NSRange spaceRange = [text rangeOfString:@" "];
    if([textView.text length] == 0){
        if (spaceRange.location != NSNotFound)
        {
            return NO;
        }
    }
    
    if(textView == self.parmanentAdressTextView)
    {
        if ( [text length] >= 120 ) {
            return false;
        }
    }
    return YES;
}

#pragma mark - mode of transport related stuff
-(void)animateSelectedCollectionView{
    
    if (_selectedValues.count > 0) {
        
        self.selectedCollectionHeightConstrain.constant = 55;
        self.modeOfTransportLabelHeightConstraint.constant = -15;
        
    }else{
        self.selectedCollectionHeightConstrain.constant = 0;
        self.modeOfTransportLabelHeightConstraint.constant = 0;
        
    }
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        if(_selectedValues.count > 0){
            [sharedUtils placeholderSize:_modeOfTransportTextField :@"Change..."];
        }else{
            [sharedUtils placeholderSize:_modeOfTransportTextField :@"Select mode of transportation..."];
        }
        
    }];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  _selectedValues.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TransportModel* object = [_selectedValues objectAtIndex:indexPath.row];

    SelectedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SelectedCell" forIndexPath:indexPath];
    
    cell.devName.text = [object.tMode capitalizedString];
    [cell.devName sizeToFit];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
     TransportModel* object = [_selectedValues objectAtIndex:indexPath.row];
    
    CGSize calCulateSizze =[object.tMode sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:18.0]}];
    calCulateSizze.width = calCulateSizze.width+ 20;
    calCulateSizze.height = calCulateSizze.height + 20;
    return calCulateSizze;
}

#pragma mark - Delegate to show seleected modeof transport

-(void)saveTransportMode:(NSMutableArray *)selectedNewValues{
    
    //   Make a local copy of the array, because an array cannot be mutated as it is enumerated
    @try {
        
        _selectedValues = selectedNewValues;
        [self.selectedCollectionView reloadData];
        [self animateSelectedCollectionView];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        ;
    } @finally {
        ;
    }
}

#pragma mark - Get Mode of Transport
-(void)getTransportMode:(void (^) (BOOL success, NSDictionary* result, NSError* error))completion {
    
    availableTransportMode =  [[NSMutableArray alloc]init];
    
    NSString *params = [NSString stringWithFormat:@"%@",GetModeOfTransport];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              if(responseDictionary == nil){
                                                  
                                                  completion(false, nil ,nil);

                                              }
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfAddress;
                                                  arrayOfAddress = [responseDictionary valueForKey:@"data"];
                                                  
                                                  for(int index =0 ; index<arrayOfAddress.count ; index++){
                                                      
                                                      NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                      
                                                      //Set address Model
                                                      
                                                      TransportModel* modeOfTransport =[[TransportModel alloc]initWithModeName:[arrayOfdata valueForKey:@"transportation_mode"] ModeID:[arrayOfdata valueForKey:@"id"] :[[arrayOfdata valueForKey:@"licence_required"] boolValue]];
                                                      
                                                      [availableTransportMode addObject:modeOfTransport];
                                                      modeOfTransport = nil;
                                                      
                                                  }
                                                  completion(true, responseDictionary ,nil);
                                                  
                                              }else if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                  
                                                  completion(true,responseDictionary,nil);
                                              }
                                          }else{
                                              completion(false,nil,error);
                                              
                                          }
                                          
                                      });
                                  }];
    [task resume];
}

-(void)getModeOfTransport{
    
    if([LSUtils isNetworkConnected]) {
        
        [self getTransportMode:^(BOOL success, NSDictionary *result, NSError *error) {
        }];
    }else{
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - TextFieldDelegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == _modeOfTransportTextField){
        UIButton *dimView;
        [dimView removeFromSuperview];
        dimView = nil;
        dimView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        dimView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
        ModeSelectionView* viewPop= [[ModeSelectionView alloc] init];
        viewPop.selectionsDel = self;
        if(availableTransportMode.count ==0){
            
            if([LSUtils isNetworkConnected]) {
                
                [sharedUtils startAnimator:self];
                
                [self getTransportMode:^(BOOL success, NSDictionary *result, NSError *error) {
                    
                    [sharedUtils stopAnimator:self];
                    
                    if (error != nil) {
                        // handel error part here
                        UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                        [self presentViewController:controller animated:YES completion:nil];
                    }else{
                        if(success){
                            
                            viewPop.modeOfTransportArray = [NSArray arrayWithArray:availableTransportMode];
                            viewPop.selectedIndexPathArray = [[NSMutableArray arrayWithArray:_selectedValues] mutableCopy];
                            viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
                            
                            [UIView animateWithDuration:0.4
                                                  delay:0.0
                                                options: UIViewAnimationOptionCurveEaseIn
                                             animations:^{
                                                 viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                                                 
                                             }
                                             completion:^(BOOL finished){
                                             }];
                            [UIView commitAnimations];
                            viewPop.backgroundColor=[UIColor whiteColor];
                            [dimView addSubview:viewPop];
                            [self.view addSubview:dimView];
                        }
                        else{
                            UIViewController*  controller;
                            if(result == nil){
                                
                                controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There is no response."];
                            }else
                                controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There are some problem occured."];
                            [self presentViewController:controller animated:YES completion:nil];
                        }
                    }
                }];
            }else{
                UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
                [self presentViewController:controller animated:YES completion:nil];
            }
        }else{
        viewPop.modeOfTransportArray = [NSArray arrayWithArray:availableTransportMode];
        viewPop.selectedIndexPathArray = [[NSMutableArray arrayWithArray:_selectedValues] mutableCopy];
        viewPop.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + viewPop.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             viewPop.frame = CGRectMake(0,CGRectGetHeight(self.view.frame)*.20, self.view.frame.size.width, self.view.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                         }];
        [UIView commitAnimations];
        viewPop.backgroundColor=[UIColor whiteColor];
        [dimView addSubview:viewPop];
        [self.view addSubview:dimView];
        }
        [[self view] endEditing:true];
        return false;
    }
    return true;

}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_nametextField) {
        
        UIButton *nameClearButton = [_nametextField valueForKey:@"_clearButton"];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [nameClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];

        if ([self.nametextField.text length]>0) {
            self.nameImageView.hidden=true;
        }
 

    }
    
        if (textField==_emailTextField) {
            UIButton *emailClearButton = [_emailTextField valueForKey:@"_clearButton"];
            [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [emailClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];

            if ([self.emailTextField.text length]>0) {
                self.emailImageView.hidden=true;
            }
            
        }
    
        if (textField==_phoneTextField) {
            
            UIButton *phoneClearButton = [_phoneTextField valueForKey:@"_clearButton"];
            [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [phoneClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];

            if ([self.phoneTextField.text length]>0) {
                self.phoneImageView.hidden=true;
            }
            
        }
    
        if (textField==_passwordtextField) {
            
            UIButton *passClearButton = [_passwordtextField valueForKey:@"_clearButton"];
            [passClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [passClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
            
            if ([self.passwordtextField.text length]>0) {
                self.passwordImageView.hidden=true;
            }
            
        }
    
        if (textField==_confirmPasswordTextField) {
            
            UIButton *confirmPassClearButton = [_confirmPasswordTextField valueForKey:@"_clearButton"];
            [confirmPassClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
            [confirmPassClearButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
            

            if ([self.confirmPasswordTextField.text length]>0) {
                self.confirmPasswordImageview.hidden=true;
            }
            
        }
    if (textField==_refferalCodeTextField) {
        
        UIButton *refferalCodeButton = [_refferalCodeTextField valueForKey:@"_clearButton"];
        [refferalCodeButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateNormal];
        [refferalCodeButton setImage:[UIImage imageNamed:@"cross_grey"] forState:UIControlStateHighlighted];
        
        if ([self.refferalCodeTextField.text length]>0) {
            self.refferalCodeImageview.hidden=true;
        }
        
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField {

    if(!([_nametextField.text length]>0))
    {
        [sharedUtils placeholderSize:_nametextField :@"Enter your full name..."];
    }
    if(!([_emailTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_emailTextField :@"Enter your email id..."];
    }
    if(!([_ssnNumberTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_ssnNumberTextField :@"Enter your ssn number..."];
    }
    if(!([sharedUtils getLength:_phoneTextField.text]>0))
    {
        [sharedUtils placeholderSize:_phoneTextField :@"Enter your number..."];
    }
    if(!([_passwordtextField.text length]>0))
    {
        [sharedUtils placeholderSize:_passwordtextField :@"Enter your password..."];
        
    }
    if(!([_confirmPasswordTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_confirmPasswordTextField :@"Re enter your password..."];
    }
    
    if(!([_modeOfTransportTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_modeOfTransportTextField :@"Select mode of transportation..."];
    }
    if(!([_refferalCodeTextField.text length]>0))
    {
        [sharedUtils placeholderSize:_refferalCodeTextField :@"Enter your referral code..."];
    }
    self.nameImageView.hidden=false;
    self.emailImageView.hidden=false;
    self.phoneImageView.hidden=false;
    self.passwordImageView.hidden=false;
    self.confirmPasswordImageview.hidden=false;
    self.refferalCodeImageview.hidden=false;
    self.ssnNumberImageview.hidden=false;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if(textField == self.refferalCodeTextField){
        //check if referl code field textfield is cleared enable register button
        if ([self.refferalCodeTextField.text length] == 0){
            
            _registerButton.enabled = true;
        }else
            _registerButton.enabled = false;
    }
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIView *textFieldSuperView = textField.superview;
    UIResponder* nextResponder = [textFieldSuperView.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }

    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
    
    if(textField == self.nametextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.nameImageView.hidden = true;
        }
        
        if (([self.nametextField.text length] == 1) && [string isEqualToString:@""]) {
            self.nameImageView.hidden = false;
            return false;
        }
        
        if(_nametextField.text.length >= 28 ){
            
            
            [[NotificationManager notificationManager] displayMessage:@"Name must be of length 2 to 28." withType:Negative];
            
            return false;
        }
        return true;
    }
    
    if(textField == self.emailTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.emailImageView.hidden = true;
        }
        
        if (([self.emailTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.emailImageView.hidden = false;
        }
    }
    
    if(textField == self.ssnNumberTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.ssnNumberImageview.hidden = true;
        }
        
        if (([self.ssnNumberTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.ssnNumberImageview.hidden = false;
            return false;
        }
        return true;
    }
    if(textField == self.passwordtextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.passwordImageView.hidden = true;
        }
        
        if (([self.passwordtextField.text length] == 1) && [string isEqualToString:@""]) {
            self.passwordImageView.hidden = false;
        }
        
        if(_passwordtextField.text.length >= 16 ){
            
            
            [[NotificationManager notificationManager] displayMessage:@"Password must be of length 6 to 16." withType:Negative];
            
            return false;
        }
    }
    
    if(textField == self.confirmPasswordTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.confirmPasswordImageview.hidden = true;
        }
        
        if (([self.confirmPasswordTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.confirmPasswordImageview.hidden = false;
        }
    }
    
    if(textField == _phoneTextField ) {
        
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.phoneImageView.hidden = true;
        }
        
        if (([self.phoneTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.phoneImageView.hidden = false;
        }
        
        NSCharacterSet *validCharSet;
        if (range.location == 0)
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        else
            validCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        if ([[string stringByTrimmingCharactersInSet:validCharSet] length] > 0 ) {
            [[NotificationManager notificationManager] displayMessage:NSLocalizedString(@"Only Numbers are allowed.", @"") withType:Negative];
            return false;  //not allowable char
        }

        
        int length = (int)[sharedUtils getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [sharedUtils formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [sharedUtils formatNumber:textField.text];

            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    if(textField == self.refferalCodeTextField)
    {
        if ([string length]> 0 || [string isEqualToString:@""]) {
            self.refferalCodeImageview.hidden = true;
        }
        
        if (([self.refferalCodeTextField.text length] == 1) && [string isEqualToString:@""]) {
            self.refferalCodeImageview.hidden = false;
        }
        _registerButton.enabled = false;
    }
    return YES;
}


-(void)cancelNumberPad{
    [_phoneTextField resignFirstResponder];
    _phoneTextField.text = @"";
}

-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard = _phoneTextField.text;
    [_phoneTextField resignFirstResponder];
}
// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
}

// start updating location
- (void)startLocationUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (!sharedUtils.locationManager) {
        sharedUtils.locationManager = [[CLLocationManager alloc] init];
    }
    [sharedUtils.locationManager requestWhenInUseAuthorization];
    [sharedUtils.locationManager startUpdatingLocation];
}


- (IBAction)verifyRefferralCode:(id)sender  {

    [_refferalCodeTextField resignFirstResponder];
    
    if(![LSUtils checkNilandEmptyString:_refferalCodeTextField.text]){
        [[NotificationManager notificationManager] displayMessage:@"Please enter referral code." withType:Negative];
        return;
    }
    if([LSUtils isNetworkConnected])
    {
        self.view.userInteractionEnabled = NO;
        
        NSString *params = [NSString stringWithFormat:@"%@&refrence_code=%@",validateRefrenceCode,_refferalCodeTextField.text];
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self];
                                              self.view.userInteractionEnabled = YES;
                                              _registerButton.enabled= true;
                                              
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:0
                                                                                                                      error:NULL];
                                                  if(responseDictionary == nil){
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  UIAlertController *controller ;
                                                  
                                                  
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      UIAlertController *controller = [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      if([[[responseDictionary objectForKey:@"data"] objectForKey:@"valid"]intValue]== 0){
                                                          _refferalCodeTextField.text = @"";
                                                      }
                                                      
                                                  }
                                                  else {
                                                      
                                                      controller =   [sharedUtils showAlert:@"Sorry" withMessage:[responseDictionary objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}

@end
