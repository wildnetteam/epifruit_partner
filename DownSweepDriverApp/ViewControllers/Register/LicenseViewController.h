//
//  LicenseViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 01/08/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserManager.h"
#import "NotificationManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>

@interface LicenseViewController : UIViewController<UIActionSheetDelegate , UIImagePickerControllerDelegate , UITextFieldDelegate ,  UINavigationControllerDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *licenseImageView;
@property (weak, nonatomic) IBOutlet UITextField *licensetextField;
@property (weak, nonatomic) IBOutlet UIButton *licenseImageButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) NSString* paramsForRegister;

@end
