//
//  LicenseViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 01/08/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "LicenseViewController.h"
#import "WaitingViewController.h"

@interface LicenseViewController ()
{
    BOOL isLicenseImageAvailable;
}
@end

@implementation LicenseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Params: %@", _paramsForRegister);
    [self setUpUIElementsValues];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{

    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}
-(void)dealloc{
    
    _paramsForRegister = nil;
    _licenseImageView = nil;
    
}
-(void)setUpUIElementsValues{
    
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    self.title = @"";
    
    //Add padding to text field
    [LSUtils addPaddingToTextField:_licensetextField];
   
     if(!([_licensetextField.text length]>0))
    {
        [sharedUtils placeholderSize:_licensetextField :@"Enter your license number..."];
    }

}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

//Give user a option if he wants photo from gallery or take fro camera
-(void)openImageOption {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self goToCameraView];
                                                       
                                                   }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self getPhotoLibrary];
                                                      }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert.popoverPresentationController setPermittedArrowDirections:0];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(sharedUtils.screenWidth / 2.0 + 20, sharedUtils.screenHeight / 2.0 , 1.0, 1.0);
    
    [self presentViewController:alert animated:YES completion:nil];
}

//take a snap from cemera
//go to another viewcontroller for clicking photo
-(void)goToCameraView{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIViewController *controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Device has no camera"];
        [self presentViewController:controller animated:NO completion:nil];
        
        
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = (id)self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}

//choose photo from gallery
-(void)getPhotoLibrary {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.allowsEditing = NO;
    imagePicker.delegate = self;
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage ];
    imagePicker.navigationBar.translucent = false;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark UIImpagePicker Delegate

//this delegate is called when photo is selected from gallery
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ( [mediaType isEqualToString:@"public.image" ]) {
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        //UIImage *resizedImage =  [image scaleToSize:ProfileImageResizeScale];
       // _profileImageView.image= [resizedImage fixOrientation];
        _licenseImageView.image = nil;
        _licenseImageView.image = image;
        isLicenseImageAvailable = true;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)backButtonClicked:(id)sender{
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ImageButtonClicked:(id)sender {
    
    [self openImageOption];
}

#pragma mark - Validation method

-(BOOL)validatetextField{
    
//    if(isLicenseImageAvailable == false){
//        
//        [[NotificationManager notificationManager] displayMessage:@"Please select license image." withType:Negative];
//        return false;
//    }
    if(![LSUtils checkNilandEmptyString:_licensetextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"License can not be blank." withType:Negative];
        return false;
    }
    return true;
}
- (IBAction)registerButtonClicked:(id)sender {
    
    if([self validatetextField]){
    [self registerUserOnserver];
    }
}

-(void)registerUserOnserver {

    if([LSUtils isNetworkConnected]){
        
        _registerButton.enabled = false;
        
        [sharedUtils  startAnimator:self];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        
         NSString *imgURL=@"";
        if(isLicenseImageAvailable)
        imgURL =  [UIImagePNGRepresentation(_licenseImageView.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        NSString* params = [NSString stringWithFormat:@"%@&deliveryboy_licenceno=%@&licence_image=%@",_paramsForRegister,[LSUtils removeLeadingSpacesfromString:_licensetextField.text],imgURL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData* data, NSURLResponse* response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              _registerButton.enabled = true;
                                              [sharedUtils stopAnimator:self];

                                              if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                 options:0
                                                                                                                   error:NULL];
#ifdef DEBUG
                                              NSLog(@"Response:%@",responseDictionary);
                                              
#endif
                                              UIAlertController *controller;
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  [UserManager saveUserCredentials:[responseDictionary valueForKey:@"delboy_id"] withvar:k_LoggedInUserID];
                                                  WaitingViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"WaitingViewController"];
                                                  vc.waitingmessage = @"Thankyou for registering with us!";
                                                  vc.swiftySideMenu.centerViewController = self.navigationController;
                                                  
                                                  [self.navigationController pushViewController:vc animated:YES];
                                                  
                                              }
                                              else
                                              {
                                                  if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Email not verified."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"211"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Mobile no not verified."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"212"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Your account is not active."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"213"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Email id not verified."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"214"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"No record found."];
                                                      [self presentViewController:controller animated:YES completion:nil];}
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"217"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"This email address is already registered."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"218"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Mobile number Already Registered."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"300"]) {
                                                      controller = [sharedUtils showAlert:@"Sorry" withMessage:@"Could not register at this moment."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                          }
                                              
                                          });
                                      }];
        [task resume];
        
    }else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view  endEditing:true];
}
@end
