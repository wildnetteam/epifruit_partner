//
//  ModeOfTransportViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 15/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "ModeOfTransportViewController.h"
#import "LSUtils.h"
#import "TransportModel.h"
#import "WaitingViewController.h"
#import "NotificationManager.h"
@interface ModeOfTransportViewController ()
{
    UIAlertAction *okAction;
}

@end

@implementation ModeOfTransportViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _selectedIndexPathArray =  [[NSMutableArray alloc]init];

    [self setUpUIElementsValues];
    [self getModeOfTransport];
}

-(void)setUpUIElementsValues{
    
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    
    self.title = @"";
}

-(void)backButtonClicked:(id)sender{
    
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modeOfTransportArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ModeOftransportTableViewCell *cell = (ModeOftransportTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    if (_modeOfTransportArray
        == nil || [_modeOfTransportArray count] <= 0) {
        //do something
    }
    else{
        
        TransportModel* object = [_modeOfTransportArray objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellEditingStyleNone;

        cell.modeLabel.text = [object.tMode capitalizedString];
        
        for (TransportModel*obj1 in _selectedIndexPathArray) {
            
            if(obj1.tId == object.tId){
                
                cell.selectionlabel.text = @"check";
                cell.modeLabel.textColor = [UIColor colorWithHexString:@"#23aac2"];
            }
            else{
                cell.selectionlabel.text = @"";
                cell.modeLabel.textColor = [UIColor colorWithHexString:@"#7d7d7d"];
                
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // Navigation logic may go here. Create and push another view controller.
    
        TransportModel* object = [_modeOfTransportArray objectAtIndex:indexPath.row];
    
//    for (TransportModel*obj1 in _selectedIndexPathArray) {
//        
//        if(obj1.tId == object.tId){
//            
//            [_selectedIndexPathArray removeObject:object];
//        }
//        else{
//                [_selectedIndexPathArray addObject:object];
//            }
//        }
    
    if([_selectedIndexPathArray containsObject:object]){
        
                   [_selectedIndexPathArray removeObject:object];

    }else
        [_selectedIndexPathArray addObject:object];

    [_tableView deselectRowAtIndexPath:indexPath animated:true];

    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];

}

-(BOOL)validateFields{
    
    if(_selectedIndexPathArray.count ==0){
        
        [[NotificationManager notificationManager] displayMessage:@"Please select atleast one mode of transportation." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        return false;
    }
    return true;
}
- (IBAction)continueButtonClicked:(id)sender {
    
    NSAttributedString *alertmessage =    [[NSAttributedString alloc]
                                           initWithString:@"You have selected heavy vehicle"
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#2a2a2a"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Regular" size:13], NSFontAttributeName, nil]];
    
    NSAttributedString *alertTitle =    [[NSAttributedString alloc]
                                           initWithString:@"Enter License Number"
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#232323"],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Semibold" size:16], NSFontAttributeName, nil]];
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       
                                   }];
    
    okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   UITextField *temp = alertController.textFields.firstObject;
                                   
                                   NSLog(@"OK action :%@", temp.text);
                                   [self saveModeOfTransport :temp.text];
    
                               }];
    
    [alertController setValue:alertmessage forKey:@"attributedMessage"];
    [alertController setValue:alertTitle forKey:@"attributedTitle"];

    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter license Number";
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeASCIICapable;
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    okAction.enabled = NO;
    
    BOOL flag = false;
    for (TransportModel*obj in _selectedIndexPathArray) {
        if(obj.licenseneeded){
            flag = true;
            break;
        }else{
            flag = false;
        }
    }
    if(flag == true){
        
        [self  presentViewController:alertController animated:NO completion:Nil];

    }else{
        
        [self saveModeOfTransport :@""];
    }
    
}
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [UIMenuController sharedMenuController].menuVisible = NO;
    return NO;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [okAction setEnabled:(finalString.length >= 1)];
    
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        
        return false;
    }
        return true;
}
#pragma mark - Get Added Card Detail
-(void)getTransportMode:(void (^) (BOOL success, NSDictionary* result, NSError* error))completion {
    
    _modeOfTransportArray =  [[NSMutableArray alloc]init];

    NSString *params = [NSString stringWithFormat:@"%@",GetModeOfTransport];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              NSLog(@"Response:%@",responseDictionary);
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSMutableArray *arrayOfAddress;
                                                  arrayOfAddress = [responseDictionary valueForKey:@"data"];
                                                  
                                                  for(int index =0 ; index<arrayOfAddress.count ; index++){
                                                      
                                                      NSDictionary * arrayOfdata = [arrayOfAddress objectAtIndex:index];
                                                      
                                                      //Set address Model
                                                     
                                                     TransportModel* modeOfTransport =[[TransportModel alloc]initWithModeName:[arrayOfdata valueForKey:@"transportation_mode"] ModeID:[arrayOfdata valueForKey:@"id"] :[[arrayOfdata valueForKey:@"licence_required"] boolValue]];
                                                      
                                                      [_modeOfTransportArray addObject:modeOfTransport];
                                                      
                                                  }
                                                  completion(true, responseDictionary ,nil);
                                                  
                                              }else if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                  
                                                  completion(true,responseDictionary,error);
                                              }
                                          }else{
                                              completion(false,nil,error);
                                              
                                          }
                                          
                                      });
                                  }];
    [task resume];
}

-(void)getModeOfTransport{
    
    if([LSUtils isNetworkConnected]) {
        
        [sharedUtils startAnimator:self];
        
        [self getTransportMode:^(BOOL success, NSDictionary *result, NSError *error) {
            [sharedUtils stopAnimator:self];
            if (error != nil) {
                // handel error part here
                UIViewController*  controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }else{
                if(success){
                    [_tableView reloadData];
                }
                else{
                    UIViewController*  controller;
                    if(result == nil){
                        
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There is no response."];
                    }else
                        controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"There are some problem occured."];
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }
        }];
    }else{
        UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)saveModeOfTransport :(NSString*)LicenseNo{
    
    if([self validateFields]){
        
        NSMutableString *idString = [[NSMutableString alloc]init];
        
        for ( TransportModel *model in _selectedIndexPathArray ) {
            [idString appendFormat:@"%@,", model.tId];
        }
        if([LSUtils isNetworkConnected]) {
            
            [sharedUtils startAnimator:self];

            NSString *params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&transport_id=%@&is_save=1",SaveModeOfTransport,[UserManager getUserID],idString ];
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:60.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  [sharedUtils stopAnimator:self];

                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                        options:0
                                                                                                                          error:NULL];
                                                      NSLog(@"Response:%@",responseDictionary);
                                                      
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          WaitingViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"WaitingViewController"];
                                                          vc.waitingmessage =@"Thankyou for registering with us!";
                                                          [self.navigationController pushViewController:vc animated:true];
                                                          
                                                      }else if([[responseDictionary objectForKey:@"code"]isEqualToString:@"210"]){
                                                          
                                                      }
                                                  }else{
                                                      
                                                  }
                                                  
                                              });
                                          }];
            [task resume];
        }else{
            UIViewController*  controller =   [sharedUtils showAlert:@"Failed" withMessage:@"Internet not connected."];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}


//REQUEST :
//{"token":"ecbcd7eaee29848978134beeecdfbc7c","methodname":"savetransportmode","deliveryboy_id":"1","transport_id":"1,2,3","is_save":"0"}
//is_save= 0=> TO EDIT , 1 = SAVE
@end
