//
//  CalenderView.h
//  DownSweepDriverApp
//
//  Created by Arpana on 20/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"

@interface CalenderView : UIView <UIScrollViewDelegate,JTCalendarDataSource,UIGestureRecognizerDelegate>

#pragma mark - Calender Components
@property (nonatomic, strong) JTCalendar *monthCalendar;
@property (nonatomic, strong) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (nonatomic, strong)IBOutlet JTCalendarContentView *calendarContentView;
@property BOOL isDateSelectedRecentlyOnMonthView;
@property (nonatomic, strong) NSLayoutConstraint *calendarContentViewHeight;
@property (nonatomic, strong) NSDate *selectedDateForDayWeekMonthList; //Date variable to store date for Day/Month/List/Week

@end
