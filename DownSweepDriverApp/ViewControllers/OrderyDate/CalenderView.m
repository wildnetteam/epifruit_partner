//
//  CalenderView.m
//  DownSweepDriverApp
//
//  Created by Arpana on 20/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "CalenderView.h"

@implementation CalenderView

-(id)init{
    
    self = [super init];
    if(self){
    [self setUPUIElements];
    [self performSelector:@selector(performBackgroundTask) withObject:nil];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUPUIElements];
    [self performSelector:@selector(performBackgroundTask) withObject:nil];
}

-(void )setUPUIElements{
    
    _selectedDateForDayWeekMonthList = [NSDate date];
    
    ////////// Save the couurent month in NSUserDefault Basically used in Month View Table View scroll //////////////
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:_selectedDateForDayWeekMonthList];
    NSInteger currentMonth = [components month];
    NSString *currentMonthString = [NSString stringWithFormat:@"%ld",(long)currentMonth];
    // [Functions saveToUserDefaults:currentMonthString keys:@"Key_For_The_Month_String"];
    
    self.monthCalendar = [JTCalendar new];
    // For a better performance, define the appearance just after the JTCalendar initialization.
    self.monthCalendar.calendarAppearance.calendar.firstWeekday = 1; // Sunday == 1, Saturday == 7
    self.monthCalendar.calendarAppearance.dayCircleRatio = 9. / 10.;
    self.monthCalendar.calendarAppearance.ratioContentMenu = 1.0;
    self.monthCalendar.calendarAppearance.dayDotRatio = 1.0 / 9.;
    self.monthCalendar.calendarAppearance.dayTextColorSelected = [UIColor whiteColor];
    self.monthCalendar.calendarAppearance.focusSelectedDayChangeMode = true;
    
    // Data cache
    //By default, a cache is activated, so you don't have to call calendarHaveEvent intensively. To clean the cache, you just have to call reloadData. If you don't want to use this cache you can disable it with: self.calendar.calendarAppearance.useCacheSystem = false;
    
    self.monthCalendar.calendarAppearance.useCacheSystem = true;
    
    //You may also want to open your calendar on a specific date. By default, it is [NSDate date].
    [self.monthCalendar setCurrentDate:self.selectedDateForDayWeekMonthList];
    [self.monthCalendar setCurrentDateSelected:self.selectedDateForDayWeekMonthList];
    [[NSNotificationCenter defaultCenter] postNotificationName:kJTCalendarDaySelected object:[NSDate date]];
    
    // WARNING: The currentDate is used for indicate the month and the week visible. When you change the currentDate, the calendar moves to the correct week and month. The currentDateSelected is the last date touched by an user. Currently, the only way to set the currentDateSelected is by calling [NSNotificationCenter defaultCenter] postNotificationName:@"kJTCalendarDaySelected" object:date];
    
    [self.monthCalendar setContentView:self.calendarContentView];
    
    //    self.calendarMenuView = [[JTCalendarMenuView alloc]init];
    //    if(is_iPhone)self.calendarMenuView.hidden = true;
    self.calendarMenuView.backgroundColor = [UIColor clearColor];
    [self.monthCalendar setMenuMonthsView:self.calendarMenuView];
    
    
    [self.monthCalendar setCurrentDate:self.selectedDateForDayWeekMonthList];
    [self.monthCalendar setCurrentDateSelected:self.selectedDateForDayWeekMonthList];
    self.monthCalendar.calendarAppearance.isWeekMode  = true;
    [[NSNotificationCenter defaultCenter] postNotificationName:kJTCalendarDaySelected object:self.selectedDateForDayWeekMonthList];
    

}
- (void)performBackgroundTask {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //Do background work
        [self.monthCalendar setDataSource:self];
        dispatch_async(dispatch_get_main_queue(), ^{
            //Update UI
            if (self.monthCalendar)[self.monthCalendar reloadData]; // Must be call in viewDidAppear
            // [self stopSpinner];
            //  [self performSelector:@selector(enableAllTouches) withObject:nil afterDelay:0.30];
        });
    });
}

#pragma mark - JTCalender Buttons callback

- (void)didChangeModeTouch {
    // MTD to change month calender to weekly calender or voice versa.
    // Note: This method is not used currently, but may be in future we may need this MTD.
    self.monthCalendar.calendarAppearance.isWeekMode = !self.monthCalendar.calendarAppearance.isWeekMode;
    [self transitionExample];
}

#pragma mark - JTCalender Transition examples
- (void)transitionExample {
    // Animation MTD if month calender changed to weekly calendar or vice versa.
    // Note: This method is not used currently, but may be in future we may need this MTD.
    CGFloat newHeight = 300;
    if(self.monthCalendar.calendarAppearance.isWeekMode){
        newHeight = 75.;
    }
    [UIView animateWithDuration:.5
                     animations:^{
                         self.calendarContentViewHeight.constant = newHeight;
                         [self layoutIfNeeded];
                     }];
    [UIView animateWithDuration:.25
                     animations:^{
                         self.calendarContentView.layer.opacity = 0;
                     }
                     completion:^(BOOL finished) {
                         [self.monthCalendar reloadAppearance];
                         
                         [UIView animateWithDuration:.25
                                          animations:^{
                                              self.calendarContentView.layer.opacity = 1;
                                          }];
                     }];
}

#pragma mark - JTCalendarDataSource

-(void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date {
    _selectedDateForDayWeekMonthList = date;
}

-(void)calendar:(JTCalendar *)calendar monthDidChangeToPreviousPage:(NSDate*)date {
    NSLog(@"Previous page loaded");
    // NSLog(@"MonthDidChange Date Selected: %@",[Utility getFormatedDateStringFromDate:date inFormat:@"dd MMMM yyyy"]);
}

-(void)calendar:(JTCalendar *)calendar monthDidChangeToNextPage:(NSDate*)date {
    NSLog(@"Next page loaded");
}

-(void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
    [self setUPUIElements];
}

@end
