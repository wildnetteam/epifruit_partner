//
//  ForgotpasswordViewController.h
//  PODRetailerApp
//
//  Created by Arpana on 24/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotpasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopSpace;
@property (weak, nonatomic) IBOutlet UIButton *sendInstructionButton;
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *forgotPassTopViewSpace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EmailLableAndEmailTextFieldSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *EmailTextFieldAndSendinstructionSpace;
@property (weak, nonatomic) IBOutlet UIView *emailbackView;

@end
