//
//  ForgotpasswordViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 24/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "ForgotpasswordViewController.h"
#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>
#import "NotificationManager.h"

@interface ForgotpasswordViewController ()
{
    BOOL isWorkInProgress;
       __weak IBOutlet UIButton *emailButton;
}
@end

@implementation ForgotpasswordViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpUIElement];
    isWorkInProgress= false;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self placeholderSize:_emailTextField :@"Enter your email or mobile number..."];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backButtonClicked:(id)sender{
    if (isWorkInProgress== false){
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)setUpUIElement{
    
    self.title = @"";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                      [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                      FONT_NavigationBar, NSFontAttributeName, nil]];

    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_white"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    barButtonItem.tintColor = [UIColor whiteColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];    
    [self.sendInstructionButton.layer setShadowOffset:CGSizeMake(0, 4)];
    [self.sendInstructionButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.sendInstructionButton.layer setShadowOpacity:0.2];
    
      //Add padding to text field
    [LSUtils addPaddingToTextField:_emailTextField];
    
    if (!IS_IPHONE5) {
        self.topViewHeight.constant = 245;
        self.forgotPassTopViewSpace.constant = 20;
    }
    if (IS_IPHONE_6P) {
        self.EmailLableAndEmailTextFieldSpace.constant = 10;
        self.EmailTextFieldAndSendinstructionSpace.constant = 40;
        self.forgotPassTopViewSpace.constant = 30;
    }
}

- (IBAction)forgotpasswordButtonClicked:(id)sender {
    
    if([self ValidateTextField]){
       
        if([LSUtils isNetworkConnected])
        {
            isWorkInProgress = true;

            self.view.userInteractionEnabled = NO;

            NSString *params = [NSString stringWithFormat:@"%@&deliveryBoyEmail=%@",ForgotPassword_Service,_emailTextField.text];
            
            [sharedUtils  startAnimator:self];
            
            NSURL *url = [NSURL URLWithString:Base_URL];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:30.0];
            
            NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  [sharedUtils stopAnimator:self];

                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                        options:0
                                                                                                                          error:NULL];
                                                      
#ifdef DEBUG
                                                      isWorkInProgress = false;
                                                      self.view.userInteractionEnabled = YES;
                                                      
#endif
                                                      UIAlertController *controller ;
                                                      
                                                      
                                                      if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Request sent"  message:@"Please check your email and follow the instruction to reset your password."   preferredStyle:    UIAlertControllerStyleAlert];
                                                          
                                                          UIAlertAction *closeAction = [UIAlertAction
                                                                                        actionWithTitle:NSLocalizedString(@"Ok", @"")
                                                                                        style:UIAlertActionStyleCancel
                                                                                        handler:^(UIAlertAction *action)
                                                                                        {
                                                                                            [self.navigationController popViewControllerAnimated:true];
                                                                                        }];
                                                          
                                                          [alertController addAction:closeAction];
                                                          
                                                          [self presentViewController:alertController animated:YES completion:nil];
                                                      }
                                                      else if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"211"]) {
                                                          
                                                          controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"Email is not verified."];
;
                                                          _emailTextField.text =@"";
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }
                                                  }
                                              });
                                          }];
            [task resume];
            
        }else{
            
            UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

// Method over ridden to relese keyboard when user taps anywhere on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:true];
}

-(BOOL)ValidateTextField{
    
    if(![LSUtils checkNilandEmptyString:_emailTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Email id can not be blank." withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        return false;
    }
    
    if(![LSUtils validateEmail:_emailTextField.text]){
        
        [[NotificationManager notificationManager] displayMessage:@"Email id is wrong. " withType:Negative];
        [self dismissViewControllerAnimated:YES completion:nil];
        _emailTextField.text = @"";
        return false;
    }
    
    return true;
}

-(void)placeholderSize:(UITextField*)textfield :(NSString*)text{
    UIColor *color = [UIColor lightGrayColor];
    textfield.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Light" size:13.0]
                                                 }
     ];
}

#pragma mark - TextFieldDelegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==_emailTextField) {
        
        _emailTextField.placeholder = @"";
        emailButton.enabled = true;
        
        [self.emailImageView setImage:[UIImage imageNamed:@"cross_grey"]];
        _emailImageView.contentMode = UIViewContentModeCenter;

        textField.returnKeyType = UIReturnKeyNext;
    }
        int height =IS_IPHONE5 ?_emailTextField.frame.size.height + 120:_emailTextField.frame.size.height + 60;
            [UIView animateWithDuration:0.3 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - height, self.view.frame.size.width, self.view.frame.size.height);
            }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{

    if(!([_emailTextField.text length]>0))
    {
        [self placeholderSize:_emailTextField :@"Enter your email or mobile number..."];
    }
    
    [self.emailImageView setImage:[UIImage imageNamed:@"email"]];
    _emailImageView.contentMode = UIViewContentModeScaleAspectFit;

    emailButton.enabled = false;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Always allow back space
    if ([string isEqualToString:@""]) {
        return YES;
    }
    // Disable emoji input
    if (![string canBeConvertedToEncoding:NSASCIIStringEncoding]){
        return false;
    }

    return true;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    [_sendInstructionButton becomeFirstResponder];
    return true;
}

- (IBAction)emailTextClear:(id)sender {
    
    _emailTextField.text =@"";
}


@end
