//
//  MyProposalViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 02/06/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "MyProposalViewController.h"
#import "LSUtils.h"
#import "NewJobMapViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OrderDetailViewController.h"
#import "UIView+XIB.h"
#import "Orderhistory.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ApplyBidsViewController.h"
#import <Crashlytics/Crashlytics.h>

@interface MyProposalViewController ()
{
    NSString *todaysDateString;
    __weak IBOutlet NSLayoutConstraint *headingLabelTopConstraint;
    UIButton *dimView;
    NSMutableArray *orderArray;
    NSMutableArray *myproposalrArray;
    AppDelegate *del;
    UIRefreshControl *btmefreshControl;
    //BOOL isRefreshing;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLabelLeadingConstraint;
@end

@implementation MyProposalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUIElementsValues];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(viewDidAppear:) name:NSdidGetUnReadNotificationCount object:nil];
    _tbleBViewLeadingConstraint.constant = self.view.frame.size.width;
    _tbleViewTrailingConstraint.constant = +self.view.frame.size.width;
     _tableView.tag = 5; // by default new JobsList
    [self listenForLocations];
    [self setWelcomeText];
    //get driver's current location
    if([LSUtils isNetworkConnected]){
        
        MBProgressHUD* progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText=@"Finding your location...";
        [sharedUtils getCurrentLocationWithCompletion:^(BOOL success, NSDictionary *result, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if(error!=nil){
                    
                    UIAlertController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Could not find your location Please turn on location settings."];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }
                if(success){
                    //loction fetched
                    [self getNewOrders ];
                }
            });
        }];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    
    if(_isFromNotificationScreen){
        
        // call the method after 0.5 sec
        // Delay 2 seconds
        [orderArray removeAllObjects];
        [myproposalrArray removeAllObjects];
        [_tableView reloadData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [self getNewOrders];
        });
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    _tbleBViewLeadingConstraint.constant =  0 ;//- _headingLabel.frame.size.height;
    _tbleViewTrailingConstraint.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

-(void)dealloc{
    
    orderArray=nil;
    myproposalrArray= nil;
}

-(void)checkLocationPermission{
    
    if([LSUtils checkIfLocationServicesAreEnabledOrNot]){
        
        //Already enabled, not needed to dispaly any mesage
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""  message:@"Please enable location service from settings."preferredStyle:    UIAlertControllerStyleAlert];
        
        UIAlertAction *closeAction = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"Ok", @"")
                                      style:UIAlertActionStyleCancel
                                      handler:^(UIAlertAction *action)
                                      {
                                      }];
        [alertController addAction:closeAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    
    orderArray =nil;
    myproposalrArray = nil;
    [super didReceiveMemoryWarning];
}

-(void)setUpUIElementsValues{
    
    self.title = @"";
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    [self.segmentControl.layer setShadowOffset:CGSizeMake(0.5, 4)];
    [self.segmentControl.layer setShadowColor:[[UIColor blackColor] CGColor]];
    self.segmentControl.layer.masksToBounds = false;
    [self.segmentControl.layer setShadowOpacity:0.1];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;
    btmefreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview: btmefreshControl];
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    self.tableView.alwaysBounceVertical = YES;
}

-(void)refresh:(id)sender{
    
    if([LSUtils isNetworkConnected]){
        
        _tableView.tag == 5 ?[self getNewOrders]:[self getMyProposedOrders];
    }else{
        
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        } ];
    }
}
-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat {
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"%@ %@",onlyDate,[df stringFromDate:date]];
}

-(NSString*)getFormatedDate:(NSString*)dateString {
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(void)setWelcomeText{
    
    todaysDateString= [self getFormatedDateStringFromDate:[NSDate date] inFormat:@"MMM yy, EEEE"];
    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    NSAttributedString *welcomeSubString ;
    NSAttributedString *dateSubString ;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    
    welcomeSubString = [[NSAttributedString alloc]
                        initWithString:@"EpiFruit"
                        attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Semibold" size:20], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
    [completeTitlestring appendAttributedString:welcomeSubString];
    
    [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@" \n"]];
    
    dateSubString = [[NSAttributedString alloc]
                     initWithString:todaysDateString
                     attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Light" size:16], NSFontAttributeName, nil]];
    
    [completeTitlestring appendAttributedString:dateSubString];
    [_headingLabel setAttributedText:completeTitlestring];
}

- (void)mapClicked:(UIButton*)mapButton :(int)tableTag{
    
    if([LSUtils isNetworkConnected]){
        
        Orderhistory* orderObjectAtIndex ;
        orderObjectAtIndex =  tableTag == 5?[orderArray objectAtIndex:mapButton.tag]:[myproposalrArray objectAtIndex:mapButton.tag];
        NewJobMapViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NewJobMapViewController"];
        vc.orderObj = orderObjectAtIndex;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)withdrawClicked:(UIButton*)withdrawButton :(int)tableTag {
    
    if([LSUtils isNetworkConnected]){
        
        if( tableTag == 5){
            return;
        }else{
            Orderhistory* orderObjectAtIndex =
            [myproposalrArray objectAtIndex:withdrawButton.tag];
            
            [self withdrawProposal:orderObjectAtIndex :(int)withdrawButton.tag :^(BOOL success, NSDictionary *result, NSError *error) {
              
                if(error!=nil){
                    
                    UIAlertController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                
                if(result == nil){
                    
                    UIAlertController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                    [self presentViewController:controller animated:YES completion:nil];
                }
                if(success){
                   
                    if([[result objectForKey:@"code"]isEqualToString:@"200"]){
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:withdrawButton.tag inSection:0];
                        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                        [_tableView beginUpdates];
                        [myproposalrArray removeObjectAtIndex:indexPath.row];
                        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                        [_tableView endUpdates];
                    }
                    UIAlertController*  controller =   [sharedUtils showAlert:@"" withMessage:[result objectForKey:@"message"]];
                    [self presentViewController:controller animated:YES completion:nil];
                }

            } ];
        }
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(btmefreshControl.isRefreshing  == true){
        [btmefreshControl endRefreshing ];
    }
    NSString *identifier =  tableView.tag ==6?@"CellIdentifierForOrder":@"CellIdentifier";
    
    orderHistoryTableViewCell *cell = (orderHistoryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:identifier];
    
    Orderhistory* orderObjectAtIndex ;
    cell.cellDelagte = self;
    cell.mapButton.tag = indexPath.row;
    cell.withDrawButton.tag = indexPath.row;
    
    switch (_tableView.tag) {
        case 5:{
            if(orderArray.count > 0 ) {
                
                orderObjectAtIndex  = [orderArray objectAtIndex:indexPath.row];
                [cell setBidsOrderDetails:orderObjectAtIndex];
                cell.tTag = 5;
                cell.pickUpTimeLabel.text =[NSString stringWithFormat:@"%@",[self getFormatedDate:orderObjectAtIndex.pickupTime]];
            }else
                return cell;
            
        }
            break;
        case 6:{
            if(myproposalrArray.count > 0 ) {
                
                orderObjectAtIndex  = [myproposalrArray objectAtIndex:indexPath.row];
                [cell setOrderDetails:orderObjectAtIndex];
                cell.tTag = 6;
                cell.pickUpTimeLabel.text = [NSString stringWithFormat:@"%@",[self getFormatedDate:orderObjectAtIndex.pickupTime]];
            }else
                return cell;
        }
            break;
            
        default:
            break;
    }
    
    [cell.activityIndicator startAnimating];
    // Here we use the new provided sd_setImageWithURL: method to load the web image
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",orderObjectAtIndex.imageBaseURL, orderObjectAtIndex.randomImageString ]]  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    [cell.activityIndicator stopAnimating];
    cell.productImageView.layer.cornerRadius = 10;
    cell.productImageView.layer.masksToBounds = true;
    
    //[NSArray arrayWithObjects:@"28.535517", @"77.391029"
    
  /*  calculate distace in KM
    [sharedUtils getDistanceFromPickUpAddress:[orderObjectAtIndex.pickUpCordinate valueForKey:@"pi_lat"]:[orderObjectAtIndex.pickUpCordinate  valueForKey:@"pi_long"] :^(BOOL success, NSDictionary *result, NSError *error) {
        if(error!=nil){
            
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
            [self presentViewController:controller animated:YES completion:nil];
        }
        
        if(result == nil){
            
            UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
            [self presentViewController:controller animated:YES completion:nil];
        }
        if(success){

        cell.distanceLabel.text = [[result valueForKey:@"distance"] valueForKey:@"text"];
        }
    }];*/
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

-(void)calculateCostAndOrderNumber{
    
    double total = 0;
    int count = 0;
    for(Orderhistory* obj in orderArray){
        if(obj.orderStatus == COMPLETED){
            total = total+[obj.price doubleValue];
            count++;
        }
    }
    _totalEarninglabel.text = [NSString stringWithFormat:@"$%.02f",total];
    _noOfOrderCompletedLable.text = [NSString stringWithFormat:@"%d", count ];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return tableView.tag == 6 ?myproposalrArray.count:orderArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([LSUtils isNetworkConnected]){
        
        //Tag 5 - new jobs , 6 - my proposed jobs
        Orderhistory* orderObjectAtIndex ;
        switch (_tableView.tag) {
            case 5:{
                orderObjectAtIndex  = [orderArray objectAtIndex:indexPath.row];
                ApplyBidsViewController* vc = (ApplyBidsViewController*) [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"ApplyBidsViewController"];
                vc.orderID = orderObjectAtIndex.productId;
                vc.swiftySideMenu.centerViewController = self.navigationController;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            case 6:{
                orderObjectAtIndex  = [myproposalrArray objectAtIndex:indexPath.row];
                
                OrderDetailViewController*  vc = (OrderDetailViewController*) [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
                vc.orderID = orderObjectAtIndex.productId;
                vc.orderStatus = orderObjectAtIndex.orderStatus;
                vc.swiftySideMenu.centerViewController = self.navigationController;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
                
            default:
                break;
        }
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    headingLabelTopConstraint.constant =  - 60 ;//- _headingLabel.frame.size.height;
    _headingLabelLeadingConstraint.constant = + 60;
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y == 0){
        
        [UIView animateWithDuration:.5
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromTop
                         animations:^{
                             headingLabelTopConstraint.constant =  0;
                             _headingLabelLeadingConstraint.constant = 4;
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                         }];
        [UIView commitAnimations];
        
    }
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)segmentControlClicked:(id)sender {
    
    //Tag 5 - new jobs , 6 - my proposed jobs
    UISegmentedControl *control = (UISegmentedControl*)sender;
    
    _tbleBViewLeadingConstraint.constant = 0;
    _tbleViewTrailingConstraint.constant = 0;
    
    if(control.selectedSegmentIndex == 1){
     // My proposal
        _tableView.tag = 6;

        if(myproposalrArray.count == 0){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self getMyProposedOrders];
            });
        }else {
          //  [sharedUtils.noResultsView removeFromSuperview];
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
            }];
        }
    }else {
        //New JOb
        _tableView.tag = 5;
        
        if(orderArray.count == 0){
    
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self getNewOrders];
            });
        }
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
            }];
        //}
    }
    [_tableView reloadData];
}

-(void)menuButtonClicked{
    
    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
    
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)getNewOrders{
    
    if([LSUtils isNetworkConnected])
    {
        // perform task on background thread
        if(btmefreshControl.isRefreshing ==false)
            [sharedUtils startAnimator:self ];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            orderArray = [[NSMutableArray alloc]init];
         
            NSURL *url = [NSURL URLWithString:Base_URL];
            NSString *param1 = [NSString stringWithFormat:@"%@&driver_id=%@&driver_lat=%@&driver_long=%@&driver_radius=20",newJobList,[UserManager getUserID],[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.latitude],[NSString stringWithFormat:@"%f",sharedUtils.CURRENTLOC.longitude]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:20.0];
            NSData *postData = [param1 dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [sharedUtils stopAnimator:self ];

                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                   options:0
                                                                                                                     error:NULL];
                                                      
                                                      if(responseDict == nil){
                                                          UIAlertController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                      if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          NSMutableArray *arrayOfDict;
                                                          arrayOfDict = [responseDict valueForKey:@"data"];
                                                          
                                                          for(int index =0 ; index<arrayOfDict.count ; index++){
                                                              
                                                              NSDictionary * orderDictionary= [arrayOfDict objectAtIndex:index];
                                                              
                                                              //Set OrderHistory Model
                                                              
                                                              Orderhistory *model = [[Orderhistory alloc]init];
                                                              model.pickupAddress = [orderDictionary objectForKey:@"pickup_address"];
                                                              model.productName = [orderDictionary objectForKey:@"product_title"];
                                                              model.orderNumber = [orderDictionary objectForKey:@"order_number"];
                                                              model.maxrate = [orderDictionary objectForKey:@"max_rate"];
                                                              model.minRate = [orderDictionary objectForKey:@"min_rate"];
                                                              model.jumpPrice = [[orderDictionary objectForKey:@"jump_price"] intValue];
                                                              model.pickupTime = [orderDictionary objectForKey:@"created_on"];
                                                              model.pickUpCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"pi_lat"], @"pi_lat",[orderDictionary valueForKey:@"pi_long"], @"pi_long",nil];
                                                              model.dropOffCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"dr_lat"], @"dr_lat",[orderDictionary valueForKey:@"dr_long"], @"dr_long",nil];
                                                              model.imageCount = [[orderDictionary objectForKey:@"image_count"] intValue];
                                                              model.randomImageString = [orderDictionary objectForKey:@"product_image"];
                                                              model.imageBaseURL = [responseDict valueForKey:@"imagepath"];
                                                              model.productId = [orderDictionary objectForKey:@"id"];
                                                              model.productDistance = [orderDictionary objectForKey:@"distance"];
                                                              model.productDesc = [NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_quantity"]]?[orderDictionary objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_type"]]?[orderDictionary objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]?[orderDictionary objectForKey:@"product_weight"]:@"" ];
                                                              
                                                              if([ model.productDesc containsString:@"( )"]||![LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]){
                                                                  model.productDesc = [[model.productDesc stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                              }
                                                              
                                                              
                                                              [orderArray addObject:model] ;
                                                              model= nil;
                                                          }
                                                          
                                                      }else{
                                                          UIAlertController *controller =  [sharedUtils showAlert:@"" withMessage:[responseDict objectForKey:@"message"]];
                                                          [self presentViewController:controller animated:true completion:nil];
                                                      }
                                                  }
                                                  else{
                                                      //data is nil
                                                      if(error != nil){
                                                          UIAlertController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }else{
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }
                                                  }
                                                  if(btmefreshControl.isRefreshing == true){
                                                      [btmefreshControl endRefreshing ];
                                                  }
                                                  [_tableView setContentOffset:CGPointZero animated:YES];
                                                  [_tableView reloadData];

                                              });
                                          }];
            [task resume];
        });
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)getMyProposedOrders{
    
    if([LSUtils isNetworkConnected])
    {
        // perform task on background thread

        if(btmefreshControl.isRefreshing ==false)[sharedUtils startAnimator:self ];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            myproposalrArray = [[NSMutableArray alloc]init];
            NSURL *url = [NSURL URLWithString:Base_URL];
            NSString *param1 = [NSString stringWithFormat:@"%@&driver_id=%@",myProposalList,[UserManager getUserID]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:20.0];
            NSData *postData = [param1 dataUsingEncoding:NSUTF8StringEncoding];
            [request setHTTPBody:postData];
            request.HTTPMethod = @"POST";
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                     [sharedUtils stopAnimator:self ];
                                                  if (data.length > 0 && error == nil)
                                                  {
                                                      NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                   options:0
                                                                                                                     error:NULL];
                                                      
                                                      if(responseDict == nil){
                                                          UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                      if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                          
                                                          NSMutableArray *arrayOfDict;
                                                          arrayOfDict = [responseDict valueForKey:@"data"];
                                                          
                                                          for(int index =0 ; index<arrayOfDict.count ; index++){
                                                              
                                                              NSDictionary * orderDictionary= [arrayOfDict objectAtIndex:index];
                                                              
                                                              //Set OrderHistory Model
                                                              Orderhistory *model = [[Orderhistory alloc]init];
                                                              model.pickupAddress = [orderDictionary objectForKey:@"pickup_address"];
                                                              model.productName = [orderDictionary objectForKey:@"product_title"];
                                                              model.orderNumber = [orderDictionary objectForKey:@"order_number"];
                                                              model.price = [orderDictionary objectForKey:@"bid_amount"];
                                                              model.jumpPrice = [[orderDictionary objectForKey:@"jump_price"] intValue];
                                                              model.pickupTime = [orderDictionary objectForKey:@"bid_time"];
                                                              model.pickUpCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"pi_lat"], @"pi_lat",[orderDictionary valueForKey:@"pi_long"], @"pi_long",nil];
                                                              model.dropOffCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"dr_lat"], @"dr_lat",[orderDictionary valueForKey:@"dr_long"], @"dr_long",nil];
                                                              model.imageCount = [[orderDictionary objectForKey:@"image_count"] intValue];
                                                              model.randomImageString = [orderDictionary objectForKey:@"product_image"];
                                                              model.imageBaseURL = [responseDict valueForKey:@"imagepath"];
                                                              model.productId = [orderDictionary objectForKey:@"id"];
                                                              model.productBidId = [orderDictionary objectForKey:@"orderBid_id"];
                                                              model.productDesc = [NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_quantity"]]?[orderDictionary objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_type"]]?[orderDictionary objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]?[orderDictionary objectForKey:@"product_weight"]:@"" ];
                                                              model.productDistance = [orderDictionary objectForKey:@"distance"];
                                                              if([ model.productDesc containsString:@"( )"]||![LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]){
                                                                  model.productDesc = [[model.productDesc stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                              }
                                                              [myproposalrArray addObject:model] ;
                                                              model = nil;
                                                          }
                                                      }else   {
                                                          UIAlertController*  controller =  [sharedUtils showAlert:@"" withMessage:[responseDict objectForKey:@"message"]];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                      }
                                                  }
                                                  else{
                                                      //data is nil
                                                      if(error != nil){
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }else{
                                                          
                                                          UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                          [self presentViewController:controller animated:YES completion:nil];
                                                          
                                                      }
                                                  }
                                                  if(btmefreshControl.isRefreshing  == true){
                                                      [btmefreshControl endRefreshing ];
                                                  }
                                                  [_tableView setContentOffset:CGPointZero animated:YES];
                                                  [_tableView reloadData];
                                              });
                                          }];
            [task resume];
        });
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

// setup firebase listerners to handle other users' locations
- (void)listenForLocations
{
    // if the user has logged in, update firebase with the new location
    LocationModel *newmodel = [[LocationModel alloc]init];
    newmodel.userToken = [UserManager getUserID];
    newmodel.email = [UserManager getEmail];
    newmodel.currentLat = [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.latitude];
    newmodel.currentLog = [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.longitude];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
    //Create the date assuming the given string is in GMT
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    NSDictionary *value = @{
                            @"userToken" : [UserManager getUserID],
                            @"currentLat" : [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.latitude],
                            @"currentLog" : [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.longitude],
                            @"timestamp" : [dateFormatter stringFromDate:[NSDate date]]
                            };
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"ISLOGGEDIN"]){
        
        FIRDatabaseReference  *newref = [del.rootRef child:[UserManager getUserID]];
        
        [newref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            if(snapshot.childrenCount == 0){
                
                [del.newref setValue:value];
                
            }else
                [ newref updateChildValues:value];
            
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
    }
}

-(void)withdrawProposal:(Orderhistory*)orderDetail :(int)indexOfCell :(void (^)(BOOL success, NSDictionary *result, NSError *error))completion  {
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&driver_id=%@&orderBid_id=%@",withdrawBid,orderDetail.productId ,[UserManager getUserID], orderDetail.productBidId];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              if(responseDictionary == nil){
                                                  
                                                  completion(true, nil, nil);
                                              }
                                              else  {
                                                  completion (true,responseDictionary , nil);
                                              }
                                          }
                                          else {
                                              
                                              if(error != nil){
                                                  
                                                  completion(true, nil, error);
                                              }else
                                                  completion(false, nil, nil);
                                          }
                                      });
                                  }];
    [task resume];
    
}

@end
