//
//  FullImageViewController.m
//  PODRetailerApp
//
//  Created by Arpana on 25/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "FullImageViewController.h"
#import "UIImage+Scale.h"
@interface FullImageViewController ()

@end

@implementation FullImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Photos";
    [self setLeftBarBtnItem];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    if(_image) {
        //UIImage *resizedImage = [_image scaleToSize:CGSizeMake(400, 400)];

        [_selectedImageView setImage:_image];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
}


-(void)viewWillDisappear:(BOOL)animated{
    
    _image = nil;
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setLeftBarBtnItem
{
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_camera"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
     self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:59.0f/255 green:135.0f/255 blue:242.0f/255 alpha:1];
}


-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
