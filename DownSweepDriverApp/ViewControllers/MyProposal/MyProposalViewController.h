//
//  MyProposalViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 02/06/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "orderHistoryTableViewCell.h"

@interface MyProposalViewController : UIViewController <UITableViewDelegate, UITableViewDataSource , MapDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UILabel *noOfOrderCompletedLable;
@property (weak, nonatomic) IBOutlet UILabel *totalEarninglabel;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (nonatomic) BOOL  isFromNotificationScreen;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbleBViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbleViewTrailingConstraint;

@end
