//
//  SelectedAddressView.h
//  PODRetailerApp
//
//  Created by Arpana on 02/01/17.
//  Copyright © 2017 Arpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedAddressView : UIView

@property (weak, nonatomic) IBOutlet UILabel *completelabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineMidConstraint;
@property (weak, nonatomic) IBOutlet UILabel *earninLabel;
@property (weak, nonatomic) IBOutlet UILabel *declinelabel;
@property (weak, nonatomic) IBOutlet UIButton *declinedOrderButton;
@property (weak, nonatomic) IBOutlet UIButton *completedOrderButton;

@end
