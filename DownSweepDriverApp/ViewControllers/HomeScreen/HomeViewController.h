//
//  HomeViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "orderHistoryTableViewCell.h"
#import "UIBarButtonItem+Badge.h"

@interface HomeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource , MapDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UILabel *noOfOrderCompletedLable;
@property (weak, nonatomic) IBOutlet UILabel *totalEarninglabel;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIView *orderCompletedView;
@property (nonatomic) BOOL  isFromNotificationScreen;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbleBViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbleViewTrailingConstraint;


@end
