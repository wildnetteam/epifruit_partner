//
//  HomeViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "HomeViewController.h"
#import "LSUtils.h"
#import "AddressViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OrderDetailViewController.h"
#import "SelectedAddressView.h"
#import "UIView+XIB.h"
#import "Orderhistory.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CompletedListViewController.h"
#import "DeclineOrdersViewController.h"

@interface HomeViewController ()
{
    NSString *todaysDateString;
    __weak IBOutlet NSLayoutConstraint *headingLabelTopConstraint;
    SelectedAddressView *view  ;
    UIButton *dimView;
    NSMutableArray *orderArray;
    NSString* imageBaseURL;
    NSDictionary* countDict;
    AppDelegate *del;
    UIRefreshControl *btmefreshControl;

}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLabelLeadingConstraint;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(viewDidAppear:) name:NSdidGetUnReadNotificationCount object:nil];
    [UserManager saveToBoolDefaults:YES withvar:@"HasLaunchedOnce"];
    _tbleBViewLeadingConstraint.constant = self.view.frame.size.width;
    _tbleViewTrailingConstraint.constant = +self.view.frame.size.width;
    [self setWelcomeText];
    _isFromNotificationScreen = false;
    [self getAllOrders ];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
   // [self listenForLocations ];
    [self getOrderCount];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self segmentControlClicked:self.segmentControl];
    [self setUpUIElementsValues];
    if(_isFromNotificationScreen){
        [self getAllOrders];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
  
    }else
    return UIStatusBarStyleDefault;
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];
    _tbleBViewLeadingConstraint.constant =  0 ;//- _headingLabel.frame.size.height;
    _tbleViewTrailingConstraint.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
    self.navigationItem.rightBarButtonItem.badgeValue= [UserManager getNotificationCount];

}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}
-(void)checkLocationPermission{
    
    if([LSUtils checkIfLocationServicesAreEnabledOrNot]){
        
        //Already enabled, not needed to dispaly any mesage
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""  message:@"Please enable location service from settings."preferredStyle:    UIAlertControllerStyleAlert];
        
        UIAlertAction *closeAction = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"Ok", @"")
                                      style:UIAlertActionStyleCancel
                                      handler:^(UIAlertAction *action)
                                      {
                                      }];
        [alertController addAction:closeAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

-(void)setUpUIElementsValues{

    self.title = @"";
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
   self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"notification"] style:UIBarButtonItemStylePlain target:self action:@selector(notificatioBtnClick)];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    [self.segmentControl.layer setShadowOffset:CGSizeMake(0.5, 4)];
    [self.segmentControl.layer setShadowColor:[[UIColor blackColor] CGColor]];
    self.segmentControl.layer.masksToBounds = false;
    [self.segmentControl.layer setShadowOpacity:0.1];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;
    btmefreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview: btmefreshControl];
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    self.tableView.alwaysBounceVertical = YES;
    
}

-(void)refresh:(id)sender{
    
    if([LSUtils isNetworkConnected]){
        [self getAllOrders ];
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        } ];
    }
}
-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
   NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
    
}

-(void)setWelcomeText{
    
    todaysDateString= [self getFormatedDateStringFromDate:[NSDate date] inFormat:@"MMM yy, EEEE"];
    
    NSMutableAttributedString * completeTitlestring =  [[NSMutableAttributedString alloc] initWithString:@""];
    
    NSAttributedString *welcomeSubString ;
    NSAttributedString *dateSubString ;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    
    
    welcomeSubString = [[NSAttributedString alloc]
                        initWithString:@"EpiFruit"
                        attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Semibold" size:20], NSFontAttributeName,paragraphStyle,NSParagraphStyleAttributeName, nil]];
    [completeTitlestring appendAttributedString:welcomeSubString];
    
    [completeTitlestring appendAttributedString:[[NSAttributedString alloc] initWithString:@" \n"]];
    
    
    dateSubString = [[NSAttributedString alloc]
                     initWithString:todaysDateString
                     attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName,[UIFont fontWithName:@"Lato-Light" size:16], NSFontAttributeName, nil]];
    
    [completeTitlestring appendAttributedString:dateSubString];
    [_headingLabel setAttributedText:completeTitlestring];
    countDict = [[NSDictionary alloc]init];
    
}

- (void)mapClicked:(UIButton*)mapButton :(int)tableTag{
    
    Orderhistory* orderObjectAtIndex = [orderArray objectAtIndex:mapButton.tag];
    AddressViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AddressViewController"];
    vc.orderObj = orderObjectAtIndex;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    orderHistoryTableViewCell *cell = (orderHistoryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    cell.cellDelagte = self;
    cell.mapButton.tag = indexPath.row;
    Orderhistory* orderObjectAtIndex = [orderArray objectAtIndex:indexPath.row];
    [cell setOrderDetails:orderObjectAtIndex];
    cell.pickUpTimeLabel.text =[self getFormatedDate:orderObjectAtIndex.pickupTime];

    [cell.activityIndicator startAnimating];
    // Here we use the new provided sd_setImageWithURL: method to load the web image
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",orderObjectAtIndex.imageBaseURL, orderObjectAtIndex.randomImageString ]]  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    [cell.activityIndicator stopAnimating];
    cell.productImageView.layer.cornerRadius = 10;
    cell.productImageView.layer.masksToBounds = true;
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    [cell updateConstraintsIfNeeded];
    return cell;
}

-(void)calculateCostAndOrderNumber{
    
    double total = 0;
    int count = 0;
    for(Orderhistory* obj in orderArray){
        if(obj.orderStatus == COMPLETED){
        total = total+[obj.price doubleValue];
            count++;
        }
    }
    _totalEarninglabel.text = [NSString stringWithFormat:@"$%.02f",total];
    _noOfOrderCompletedLable.text = [NSString stringWithFormat:@"%d", count ];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  orderArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([LSUtils isNetworkConnected]){
        Orderhistory*  orderObjectAtIndex  = [orderArray objectAtIndex:indexPath.row];
        OrderDetailViewController *vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"OrderDetailViewController"];
        vc.orderID = orderObjectAtIndex.productId;
        vc.orderStatus = orderObjectAtIndex.orderStatus;
        vc.swiftySideMenu.centerViewController = self.navigationController;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    headingLabelTopConstraint.constant =  - 60 ;//- _headingLabel.frame.size.height;
    _headingLabelLeadingConstraint.constant = + 60;
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y == 0){
        
        if(view == nil){
            
            NSString *nibName =  @"SelectedAddressView";
            view =  [SelectedAddressView initWithNib:nibName bundle:nil];
            view.frame = CGRectMake(self.view.frame.origin.x,IS_IPHONE5?CGRectGetMinY(_orderCompletedView.frame):CGRectGetMinY(_orderCompletedView.frame)+20, self.view.frame.size.width, _tableView.frame.size.height + _orderCompletedView.frame.size.height+10);
        }
        
        [UIView animateWithDuration:.5
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromTop
                         animations:^{
                             headingLabelTopConstraint.constant =  0;
                             _headingLabelLeadingConstraint.constant = 4;
                             
                             view.frame = CGRectMake(self.view.frame.origin.x,CGRectGetMaxY(_orderCompletedView.frame), self.view.frame.size.width, _tableView.frame.size.height + _orderCompletedView.frame.size.height+10);
                             [self.view layoutIfNeeded];

                             
                         }
                         completion:^(BOOL finished){
                         }];
                [UIView commitAnimations];

    }
}
//return dynamic label size
-(int)getLabelDynamicHeight:(NSString *)yourString andLabel:(UILabel *)yourLabel
{
    //Calculate the expected size based on the font and linebreak mode of your label
    
    CGSize constraint = CGSizeMake(yourLabel.frame.size.width,9999);
    
    NSDictionary *attributes = @{NSFontAttributeName: yourLabel.font};
    
    CGRect rect = [yourString boundingRectWithSize:constraint
                                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                        attributes:attributes
                                           context:nil];
    return rect.size.height;
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

 - (IBAction)segmentControlClicked:(id)sender {
    UISegmentedControl *control = (UISegmentedControl*)sender;
    if(control.selectedSegmentIndex == 1){
        
        [sharedUtils.noResultsView setHidden:true];

        if(view == nil){
            
            NSString *nibName =  @"SelectedAddressView";
            view =  [SelectedAddressView initWithNib:nibName bundle:nil];
           // view.lineMidConstraint.constant = 90;
            view.frame = CGRectMake(self.view.frame.origin.x,IS_IPHONE5?CGRectGetMinY(_orderCompletedView.frame):CGRectGetMinY(_orderCompletedView.frame)+20, self.view.frame.size.width, _tableView.frame.size.height + _orderCompletedView.frame.size.height+10);
            [view.declinedOrderButton addTarget:self action:@selector(goToDeclineViewController) forControlEvents:UIControlEventTouchUpInside];
            [view.completedOrderButton addTarget:self action:@selector(goToCompletedViewController) forControlEvents:UIControlEventTouchUpInside];
        
        }

        view.frame = CGRectMake( [UIScreen mainScreen].bounds.size.width + view.frame.size.width,CGRectGetMinY(_orderCompletedView.frame), self.view.frame.size.width, _tableView.frame.size.height + _orderCompletedView.frame.size.height+10);
        
        [UIView animateWithDuration:.5
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromTop
                         animations:^{
                           view.frame = CGRectMake(self.view.frame.origin.x,CGRectGetMinY(_orderCompletedView.frame), self.view.frame.size.width, _tableView.frame.size.height + _orderCompletedView.frame.size.height+10);
                             
                         }
                         completion:^(BOOL finished){
                             [self.view layoutIfNeeded];
                         }];
        [UIView commitAnimations];
        [self.view addSubview:view];
        
        view.hidden = false;
        [self.view bringSubviewToFront:view ];
        self.tableView.hidden = true;
        _orderCompletedView.hidden = true;
        
        //change table constraint
        _tbleBViewLeadingConstraint.constant = self.view.frame.size.width;
        _tbleViewTrailingConstraint.constant = +self.view.frame.size.width;
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];
        if(countDict!=nil){
            
        view.completelabel.text = [countDict valueForKey:@"compleated_order_count"];
        view.earninLabel.text = [NSString stringWithFormat:@"$%.02f",[ [countDict valueForKey:@"earned_cost"] doubleValue]];
        view.declinelabel.text =  [countDict valueForKey:@"rejected_order_count"];
        }else{
            [self getOrderCount];
        }

    }else {

         _orderCompletedView.hidden = false;
          view.hidden= true;
        _tableView.hidden = false;
        
        _tbleBViewLeadingConstraint.constant = 0;
        _tbleViewTrailingConstraint.constant = 0;
        
        if(orderArray.count == 0){
            [sharedUtils.noResultsView setHidden:false];
            return;
        }else {
            [sharedUtils.noResultsView removeFromSuperview];
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];
        
       
        [self.view sendSubviewToBack:view ];
        [self.view bringSubviewToFront:_tableView ];
        [self.view bringSubviewToFront:_orderCompletedView ];
        }
    }
}

-(void)menuButtonClicked{

    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];

}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

-(void)getAllOrders{
    
    if([LSUtils isNetworkConnected])
    {
        orderArray = [[NSMutableArray alloc]init];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [df setDateFormat:@"yyyy-MM-dd"];
        NSString* todayDate = [df stringFromDate:[NSDate date]];//@"2017-06-14";/
         if(btmefreshControl.isRefreshing ==false) [sharedUtils startAnimator:self ];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *param1 = [NSString stringWithFormat:@"%@&driver_id=%@&orderDate=%@",GetCompletdOrders,[UserManager getUserID],todayDate];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [param1 dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [sharedUtils stopAnimator:self ];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  
                                                  if(responseDict == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSMutableArray *arrayOfDict;
                                                      imageBaseURL = [responseDict valueForKey:@"productimgurl"];
                                                      arrayOfDict = [responseDict valueForKey:@"data"];
                                                      
                                                      for(int index =0 ; index<arrayOfDict.count ; index++){
                                                          
                                                          NSDictionary * orderDictionary= [arrayOfDict objectAtIndex:index];
                                                          
                                                          //Set OrderHistory Model
                                                          
                                                          Orderhistory *model = [[Orderhistory alloc]init];
                                                          model.pickupAddress = [orderDictionary objectForKey:@"pickup_address"];
                                                         // model.delAddress = [orderDictionary valueForKey:@"dropoff_address"];
                                                          model.productName = [orderDictionary objectForKey:@"product_title"];
                                                          model.productDesc = [orderDictionary objectForKey:@"product_description"];
                                                          
                                                            if(![LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"accepted_final_order_price"]] || [[orderDictionary objectForKey:@"accepted_final_order_price"] isEqualToString:@"(null)"]){
                                                                 model.price = @"";
                                                            }
                                                          else
                                                          model.price = [orderDictionary objectForKey:@"accepted_final_order_price"];
                                                        
                                                          model.productDesc = [NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_quantity"]]?[orderDictionary objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_type"]]?[orderDictionary objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]?[orderDictionary objectForKey:@"product_weight"]:@"" ];
                                                          
                                                          if([ model.productDesc containsString:@"( )"]||![LSUtils checkNilandEmptyString:[orderDictionary objectForKey:@"product_weight"]]){
                                                              model.productDesc = [[model.productDesc stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                          }
                                                          model.productDistance = [orderDictionary objectForKey:@"distance"];
                                                          model.pickupTime = [orderDictionary objectForKey:@"created_on"];
                                                              //to show status completed status
                                                          model.orderStatus = [[orderDictionary objectForKey:@"order_status"] intValue];
                                                          model.orderNumber = [orderDictionary objectForKey:@"order_number"];
                                                          model.productId = [orderDictionary objectForKey:@"id"];
                                                          model.pickUpCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"pi_lat"], @"pi_lat",[orderDictionary valueForKey:@"pi_long"], @"pi_long",nil];
                                                          model.dropOffCordinate = [NSDictionary dictionaryWithObjectsAndKeys:[orderDictionary valueForKey:@"dr_lat"], @"dr_lat",[orderDictionary valueForKey:@"dr_long"], @"dr_long",nil];
                                                          model.imageCount = [[orderDictionary objectForKey:@"image_count"] intValue];
                                                          model.randomImageString = [orderDictionary objectForKey:@"product_image"];
                                                          model.imageBaseURL = [responseDict valueForKey:@"imagepath"];
                                                          [orderArray addObject:model] ;
                                                      }
                                                      [self calculateCostAndOrderNumber];
                                                  }else {
                                                      
                                                      UIViewController *controller =  [sharedUtils showAlert:@"" withMessage:[responseDict objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:true completion:nil];
                                                  }
                                              }
                                              else{
                                                  //data is nil
                                                  if(error != nil){
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                      
                                                  }
                                              }
                                              if(btmefreshControl.isRefreshing == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                              [_tableView reloadData];
                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)getOrderCount{
    
    if([LSUtils isNetworkConnected])
    {
        
        NSURL *url = [NSURL URLWithString:Base_URL];//"2016-11-21
        NSString *params = [NSString stringWithFormat:@"%@&deliveryboy_id=%@",GetTodaysOrdercount,[UserManager getUserID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{ if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:0
                                                                                                             error:NULL];
                                              
                                              if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  
                                                  NSArray* orderArrayForCount = [responseDict objectForKey:@"data"];
                                                  
                                                  NSDictionary *dict = [orderArrayForCount objectAtIndex:0];
                                    
                                                  countDict= dict;
                                                  
                                              }else  if ([[responseDict objectForKey:@"code"]isEqualToString:@"210"]) {
                                                  
                                                  if(orderArray.count == 0){
                                                      
                                                      [sharedUtils showNoResultsViewWithOptionalText:NSLocalizedString(@"You do not have any order for today.", @"You do not have any order for today.") xPosition:0 yPosition:IS_IPHONE5?CGRectGetMinY(_orderCompletedView.frame):CGRectGetMinY(_orderCompletedView.frame)+20 screenWidth:0 screenHeight:CGRectGetHeight(_orderCompletedView.frame) +CGRectGetHeight(_segmentControl.frame)+ CGRectGetHeight(_headingLabel.frame)+64 ];
                                                      sharedUtils.noResultsView.backgroundColor = [UIColor whiteColor];
                                                      [self.view addSubview:sharedUtils.noResultsView];
                                                      return;
                                                  }
                                              }
                                          }
                                          else{
                                          }
                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }

}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

// setup firebase listerners to handle other users' locations
- (void)listenForLocations
{
    // if the user has logged in, update firebase with the new location
    LocationModel *newmodel = [[LocationModel alloc]init];
    newmodel.userToken = [UserManager getUserID];
    newmodel.email = [UserManager getEmail];
    newmodel.currentLat = [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.latitude];
    newmodel.currentLog = [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.longitude];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT];
    //Create the date assuming the given string is in GMT
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    NSDictionary *value = @{
                            @"userToken" : [UserManager getUserID],
                            @"currentLat" : [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.latitude],
                            @"currentLog" : [NSNumber numberWithDouble:sharedUtils.CURRENTLOC.longitude],
                            @"timestamp" : [dateFormatter stringFromDate:[NSDate date]]
                            };
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"ISLOGGEDIN"]){
        
        FIRDatabaseReference  *newref = [del.rootRef child:[UserManager getUserID]];
        [ newref updateChildValues:value];
        
    }
}

- (void)goToCompletedViewController {

   CompletedListViewController* vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"CompletedListViewController"];
    vc.priviousScreen = 1;
    [self.navigationController pushViewController:vc animated:YES];

}
- (void)goToDeclineViewController {
    
    DeclineOrdersViewController*  vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DeclineOrdersViewController"];
    vc.priviousScreen = 1;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)notificatioBtnClick
{
    UIViewController*  vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"NotificationVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
