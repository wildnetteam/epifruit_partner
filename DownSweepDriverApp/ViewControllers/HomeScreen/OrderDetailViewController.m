//
//  OrderDetailViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 28/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "LSUtils.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface OrderDetailViewController ()
{
    NSMutableArray *imagesArray;
    NSString *productImageBaseUrl;
    NSDictionary *responseDict;
    UIRefreshControl *btmefreshControl;
}
@property (weak, nonatomic) IBOutlet UIView *dimView;

@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUIElements];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    //Set default Image on scrollView
    UIImageView* imgView = [[UIImageView alloc] init];
    CGRect frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size = CGSizeMake(sharedUtils.screenWidth, _imageScrollView.frame.size.height + 64);
    imgView.frame = frame;
    imgView.alpha = .6;
    [imgView setBackgroundColor:[UIColor whiteColor]];
    imgView.contentMode = UIViewContentModeScaleToFill;
    [_imageScrollView addSubview:imgView];
    imgView.image = [UIImage imageNamed:@"noimage"];
    
    [self getOrderDetail];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
    
}

-(void)setTextFields {
    
    _pickUpAddresslabel.text = _orderDetail.pickupAddress;
    _deliveryAddressLabel.text = _orderDetail.delAddress;
    _productDescriptionLabel.text = _orderDetail.productDesc;
    _productTitleLabel.text = _orderDetail.productName;
    _pickUpTimeLabel.text= [self getFormatedDate:_orderDetail.pickupTime];
    _VehicleLabel.text = _orderDetail.modeOfTransport;
    _orderNoLabel.text = [NSString stringWithFormat:@"Order no. %@",_orderDetail.orderNumber];
    _priceLabel.text = [NSString stringWithFormat:@"$%.02f",[_orderDetail.price floatValue]];
    [_progressLabel setAttributedText: [sharedUtils getOrderStatus:_orderDetail.orderStatus]];
}

-(NSString*)getFormatedDate:(NSString*)dateString{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSDate *startTimeValue = [LSUtils getDateFromDateString:dateString byFormat:DATE_TIME_ZONE_FORMAT_LOCAL];
    
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:@"MMM"];
    NSString* timeString = [LSUtils getFormatedDateStringFromDate:startTimeValue inFormat:SHOWING_HOURS_MINUTES_TIME_FORMAT];
    NSString* pickUpTimeString = [NSString stringWithFormat:@"Bid on%@ at %@",finaldateString,timeString];
    return pickUpTimeString;
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString* onlyDate = [sharedUtils getDateWithOrdinalValue:[df stringFromDate:date]];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [NSString stringWithFormat:@"  %@ %@",onlyDate,[df stringFromDate:date]];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Set UI (EX: navigation button ,color, padding in textfield and labels
-(void)setUIElements{
    
    self.title = @"";
    [self.navigationItem addLeftBarButtonItem:[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_arrow_register"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked:)]];
    UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(0, 0,80, 50)];
    [cancelButton addTarget:self action:@selector(withdrawProposal:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Withdraw" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    self.view.backgroundColor = [UIColor colorWithRed:244.0f/254 green:244.0f/254 blue:244.0f/254 alpha:1];
    btmefreshControl = [[UIRefreshControl alloc]init];
    [self.scrollView insertSubview:btmefreshControl atIndex:0];
    self.scrollView.alwaysBounceVertical = true;
    [btmefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

-(void)refresh:(id)sender{
    if([LSUtils isNetworkConnected]){
        [self getOrderDetail];
    }
    else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:^{
            [btmefreshControl endRefreshing ];
        }];
    }
}

-(void)backButtonClicked:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 METHOD NAME:- withdrawBid
 DESC:- DELIVERY BOY WITHDRAWS ITS BID AMOUNT
*/
-(void)withdrawProposal:(id)sender {
    
    NSString *params = [NSString stringWithFormat:@"%@&order_id=%@&driver_id=%@&orderBid_id=%@",withdrawBid,_orderID ,[UserManager getUserID], _orderDetail.productBidId];
    
    NSURL *url = [NSURL URLWithString:Base_URL];
    [sharedUtils startAnimator:self];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    request.HTTPMethod = @"POST";
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          [sharedUtils stopAnimator:self];
                                          
                                          if (data.length > 0 && error == nil)
                                          {
                                              NSDictionary *responseDictionary= [NSJSONSerialization JSONObjectWithData:data
                                                                                                                options:0
                                                                                                                  error:NULL];
                                              
                                              if(responseDictionary == nil){
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"There is no response from server please try again."];
                                                  
                                                  [self presentViewController:controller animated:YES completion:^{
                                                      [self.navigationController popViewControllerAnimated:YES];
                                                  }];
                                                  return ;
                                                  
                                              }
                                              
                                              if ([[responseDictionary objectForKey:@"code"]isEqualToString:@"200"]) {
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:^{
                                                      
                                                      [self.navigationController popViewControllerAnimated:YES];
                                                  }];
                                              }
                                              else {
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[responseDictionary objectForKey:@"message"]];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                          }else{
                                              //data is nil
                                              if(error != nil){
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }else{
                                                  
                                                  UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:@"Somthing went wrong."];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                                  
                                              }
                                          }
                                      });
                                  }];
    [task resume];

}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    CGFloat x = _imageScrollView.contentOffset.x;
    float width  = _imageScrollView.frame.size.width;
    float value =x/width;
    self.pageControl.currentPage = round(value);
}

- (IBAction)changePage:(UIPageControl*)pageControll{
    [_imageScrollView setDelegate:nil];
    [_imageScrollView setContentOffset:CGPointMake(_imageScrollView.frame.size.width * self.pageControl.currentPage, 0) animated:YES];
    [_imageScrollView setDelegate:self];
}

-(void)animateScrollView
{
    int x = _imageScrollView.contentOffset.x;
    int max = _imageScrollView.contentSize.width;
    if (max>(x+self.view.frame.size.width)) {
        [UIView animateWithDuration:0.7 animations:^{
            _imageScrollView.contentOffset = CGPointMake(x+self.view.frame.size.width,_imageScrollView.contentOffset.y);
        }];
    } else {
        _imageScrollView.contentOffset = CGPointMake(0,_imageScrollView.contentOffset.y);
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma  setUI for image on scroll View
-(void)setImages:(NSMutableArray*)imgArray{
    
    for(UIImageView *imgView in _imageScrollView.subviews){
        [imgView removeFromSuperview];
    }
    _arrayOFImages = [imgArray copy];
    _imageScrollView.pagingEnabled = true;
    _pageControl.numberOfPages = [imgArray count];
    _imageScrollView.contentSize = CGSizeMake(_imageScrollView.frame.size.width * imgArray.count, 0);

    for (int i = 0; i < imgArray.count; i++) {
        CGRect frame;
        frame.origin.x = _imageScrollView.frame.size.width*i;
        frame.origin.y = 0;
        frame.size = CGSizeMake(sharedUtils.screenWidth, _imageScrollView.frame.size.height + 64);
        UIImageView* imgView = [[UIImageView alloc] init];
        imgView.frame = frame;
        imgView.alpha = .6;
        [imgView setBackgroundColor:[UIColor whiteColor]];
        imgView.contentMode = UIViewContentModeScaleToFill;
       // imgView.clipsToBounds = YES;
        [_imageScrollView addSubview:imgView];
      //  UIImage *newimage=  [self imageByScalingToSize:frame.size forImage: [imgArray objectAtIndex:i]];
        imgView.image = [imgArray objectAtIndex:i];//newimage;
    }

    // Create the colors
    UIColor *topColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
    UIColor *bottomColor = [UIColor colorWithRed:56.0/255.0 green:56.0/255.0 blue:56.0/255.0 alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    theViewGradient.frame =  CGRectMake(0, 0, _imageScrollView.contentSize.width, _imageScrollView.bounds.size.height);
    //Add gradient to view
    [_imageScrollView.layer insertSublayer:theViewGradient atIndex:0];
}

//to resize image
- (UIImage *) imageByScalingToSize:(CGSize)targetSize forImage:(UIImage *)sourceImage
{
    UIImage *generatedImage = nil;
    UIGraphicsBeginImageContextWithOptions(targetSize,NO,1.0);
    
    [sourceImage drawInRect:CGRectMake(0, 0,targetSize.width,targetSize.height-64)];
    generatedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return generatedImage;
}

-(void) loadImage:(NSString *)imageLink{
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [manager loadImageWithURL:[NSURL URLWithString:imageLink] options:SDWebImageDelayPlaceholder progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        
        if(image!=nil && ![image isKindOfClass:[NSNull class]]){
            [imagesArray addObject:image];
        }
        [self setImages:imagesArray];
    }];
}

 -(void)getOrderDetail {
    
    if([LSUtils isNetworkConnected]){
        
        imagesArray =[[NSMutableArray alloc]init];
        if(btmefreshControl.isRefreshing ==false)[sharedUtils startAnimator:self];
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *params = _orderStatus == PENDING? [NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=myProposalDetail&order_id=%@&driver_id=%@",_orderID ,[UserManager getUserID]]:[NSString stringWithFormat:@" token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getorderdetail&order_id=%@&driver_id=%@",_orderID ,[UserManager getUserID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [params dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *Response = [NSJSONSerialization JSONObjectWithData:data
                                                                                                           options:0
                                                                                                             error:NULL];
#ifdef DEBUG
                                                  
                                                  NSLog(@"Response:%@",Response);
#endif
                                                  if ([[Response objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSDictionary *arrayOforderDetail;
                                                      arrayOforderDetail = [[Response valueForKey:@"orderdata"]  objectAtIndex:0];
                                                      
                                                      responseDict  = arrayOforderDetail ;
                                                      
                                                      _orderDetail = [[Orderhistory alloc]init];
                                                      _orderDetail.pickupAddress = [arrayOforderDetail objectForKey:@"pickup_address"];
                                                      _orderDetail.delAddress = [arrayOforderDetail objectForKey:@"dropoff_address"];
                                                      _orderDetail.productName = [arrayOforderDetail objectForKey:@"product_title"];
                                                      _orderDetail.productDesc = [NSString stringWithFormat:@" %@ %@  (%@)",[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_quantity"]]?[arrayOforderDetail objectForKey:@"product_quantity"]:@"" ,[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_type"]]?[arrayOforderDetail objectForKey:@"product_type"]:@"",[LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_weight"]]?[arrayOforderDetail objectForKey:@"product_weight"]:@"" ]
                                                      ;
                                                      //If product description is blank or weight is not mentioned , Remove ()

                                                      if([_orderDetail.productDesc containsString:@"( )"]||![LSUtils checkNilandEmptyString:[arrayOforderDetail objectForKey:@"product_weight"]]){
                                                         _orderDetail.productDesc = [[_orderDetail.productDesc stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                      }
                                                      if([_orderDetail.productDesc isEqualToString:@""]){
                                                        _orderDetail.productDesc = @"not mentioned";
                                                      }
                                                      _orderDetail.modeOfTransport = [arrayOforderDetail objectForKey:@"transportation_mode"];
                                                      _orderDetail.orderNumber = [arrayOforderDetail objectForKey:@"order_number"];
                                                      _orderDetail.orderStatus = [[arrayOforderDetail objectForKey:@"order_status"] intValue];
                                                      _orderDetail.pickupTime = [arrayOforderDetail objectForKey:@"bid_time"];//@"created_on"];
                                                      //for track View
                                                      _orderDetail.jumpPrice = [[arrayOforderDetail objectForKey:@"jump_price"] intValue];
                                                      _orderDetail.minRate = [arrayOforderDetail objectForKey:@"min_rate"];
                                                      _orderDetail.maxrate = [arrayOforderDetail objectForKey:@"max_rate"];
                                                      _orderDetail.price = [arrayOforderDetail objectForKey:@"bid_amount"];
                                                      _orderDetail.productId = [arrayOforderDetail objectForKey:@"id"];
                                                      _orderDetail.productBidId = [arrayOforderDetail objectForKey:@"orderBid_id"];
                                                      
                                                      NSArray *imageArray =[Response valueForKey:@"imagedata"];
                                                      NSMutableArray* imageNameArray = [[NSMutableArray alloc]init];
                                                      
                                                      for(int index = 0; index < imageArray.count; index++){
                                                          
                                                          NSDictionary *imageDict =[imageArray objectAtIndex:index];
                                                          [imageNameArray addObject:[imageDict objectForKey:@"product_image"]];
                                                      }
                                                      if(btmefreshControl.isRefreshing == false){
                                                          _orderDetail.productImageArray = imageNameArray;
                                                          _orderDetail.imageBaseURL= [Response valueForKey:@"productImagepath"];
                                                          [self GetProductImage ];
                                                      }
                                                      
                                                      [self setTextFields];
                                                      
                                                  }else{
                                                      
                                                      UIViewController*  controller =   [sharedUtils showAlert:@"" withMessage:[Response objectForKey:@"message"]];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                              }
                                              else{
                                                  UIViewController*  controller ;
                                                  if(error != nil){
                                                      
                                                      controller =   [sharedUtils showAlert:@"" withMessage:error.localizedDescription];
                                                  }else  controller =   [sharedUtils showAlert:@"Somthing went wrong." withMessage:error.localizedDescription];
                                                  [self presentViewController:controller animated:YES completion:nil];
                                              }
                                              
                                              if(btmefreshControl.isRefreshing  == true){
                                                  [btmefreshControl endRefreshing ];
                                              }
                                          });
                                      }];
        [task resume];
        
    }else{
        UIAlertController*  controller = [sharedUtils showAlert:@"" withMessage:@"No internet connection."];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)GetProductImage {
    
    for (int index =0 ; index< _orderDetail.productImageArray.count; index++) {
        [self loadImage:[NSString stringWithFormat:@"%@/%@", _orderDetail.imageBaseURL, [_orderDetail.productImageArray objectAtIndex:index]]];
    }
}
@end
