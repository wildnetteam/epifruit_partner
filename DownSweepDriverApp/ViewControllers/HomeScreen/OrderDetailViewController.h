//
//  OrderDetailViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 28/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orderhistory.h"
@interface OrderDetailViewController : UIViewController<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *pickUpTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *VehicleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddresslabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (retain, nonatomic) Orderhistory* orderDetail;
@property(nonatomic, retain) NSString* imagebaseURL;
@property (retain, nonatomic) NSArray* arrayOFImages;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) NSString *orderID;
@property (nonatomic) int orderStatus;

@end
