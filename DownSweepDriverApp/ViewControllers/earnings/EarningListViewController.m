//
//  EarningListViewController.m
//  DownSweepDriverApp
//
//  Created by Arpana on 01/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "EarningListViewController.h"
#import "EarningTableViewCell.h"

static NSString *SectionHeaderViewIdentifier    = @"SectionHeaderViewIdentifier";
static NSString *DemoTableViewIdentifier        = @"DemoTableViewIdentifier";

@interface EarningListViewController ()<FTFoldingTableViewDelegate>{
    
    NSArray *monthArray;
    NSArray *yearArray;
    int currentYear;
    NSMutableArray *earningYearArray;
    UITableView *popoverTable;
    NSDictionary* monthWithDict;
    NSMutableArray *monthlyEarningArray;
    NSMutableArray *monthlyEarningArrayForCellDisplay;

}

@property (weak, nonatomic) IBOutlet FTFoldingTableView *ft_tableView;

@property (nonatomic, assign)FTFoldingSectionHeaderArrowPosition arrowPosition;
@end

@implementation EarningListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadYears];
    monthWithDict = [NSDictionary dictionaryWithObjectsAndKeys: @"January" ,@"01",@"February", @"02",@"March", @"03", @"April" ,@"04", @"May" ,@"05", @"June" ,@"06",@"July" ,@"07", @"August" ,@"08", @"September" ,@"09", @"October" ,@"10",@"November",@"11", @"December" ,@"12",nil];//
    NSArray *shorteredArray   = [monthWithDict allKeys];
    NSSortDescriptor *sorter=[[NSSortDescriptor alloc]initWithKey:@"self" ascending:YES];//Shorting the array using nssortdescriptor
    monthArray= [shorteredArray sortedArrayUsingDescriptors:@[sorter]];
    [self setUpUIElements];
}

+(NSMutableArray*)SortArrayByOrderValue:(NSMutableArray*)arrayToSort :(NSString*)sortExpression
{
    NSSortDescriptor *mySorter = [[NSSortDescriptor alloc] initWithKey:sortExpression ascending:true];
    [arrayToSort sortUsingDescriptors:[NSMutableArray arrayWithObject:mySorter]];
    return arrayToSort;
}
-(void)setUpUIElements{
    
    self.title = @"";
    
    UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"three_bar_navigation"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked)];
    barButtonItem.tintColor = [UIColor blackColor];
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    self.swiftySideMenu.centerViewController = self.navigationController;
    self.swiftySideMenu.enableRightSwipeGesture = false;
    self.swiftySideMenu.enableLeftSwipeGesture = false;
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = 5;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"filter"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction:)] , nil]];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.arrowPosition = FTFoldingSectionHeaderArrowPositionLeft;
    
    self.ft_tableView.foldingDelegate = self;

}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
    [self getAmountForYear];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self setNeedsStatusBarAppearanceUpdate];

}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];
    [super viewWillDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.swiftySideMenu.isLeftMenuOpened){
        return UIStatusBarStyleLightContent;
        
    }else
        return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    
    Reachability *reachability = (Reachability *)[notification object];
    
    if ([reachability isReachable]) {
        [LSUtils updateNetworkStatus:true];
    } else {
        [LSUtils updateNetworkStatus:false];
        
    }
}

-(void)cancelAction:(id)sender {
    
    [[self view] endEditing:true];
    UIViewController* contentViewController = [[UIViewController alloc]init];
    if ([contentViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
        contentViewController.preferredContentSize = CGSizeMake(100,200);
    }
    UIView* popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 200)];
    popoverTable= [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 100, 200) style:UITableViewStylePlain];
    popoverTable.tag = 5;
    popoverView.backgroundColor= [UIColor purpleColor];;
    [popoverTable setDataSource:self];
    [popoverTable setDelegate:self];
    [popoverTable setRowHeight:44];
    //remove Table view Separator Offset
    popoverTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [popoverView addSubview:popoverTable];
    
    [contentViewController.view addSubview:popoverView];
    
    contentViewController.modalInPopover = false;
    
    if (_yearListPopoverView) {
        _yearListPopoverView = nil;
    }
    
    _yearListPopoverView = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    _yearListPopoverView.delegate = self;
    _yearListPopoverView.popoverLayoutMargins = UIEdgeInsetsMake(22, 22, 22, 22);
    _yearListPopoverView.wantsDefaultContentAppearance = false;
    int currentDayIndex = [self getCurrentYearIndex:currentYear];
    
    NSIndexPath *currentIndexPath=[NSIndexPath indexPathForItem:currentDayIndex inSection:0];
    [popoverTable scrollToRowAtIndexPath:currentIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

    
    CGRect  frame = CGRectMake(self.view.frame.size.width, -150, 100, 200);
    
    [_yearListPopoverView presentPopoverFromRect:frame
                                          inView:self.view
                        permittedArrowDirections:WYPopoverArrowDirectionUp
                                        animated:true
                                         options:WYPopoverAnimationOptionFadeWithScale];
    
}

-(void)loadYears {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:now];
    int year = (int) [comps year];
    currentYear = year;
    //set year to the labe
    _yearlabel.text = [NSString stringWithFormat:@"%d", year];
    NSMutableArray *years = [NSMutableArray array];
    for (int y = year - 10; y < year + 10; y++) {
        [years addObject:@(y)];
    }
    yearArray = [NSArray arrayWithArray:years];
}
- (IBAction)changeStyle:(UIBarButtonItem *)sender
{
    self.arrowPosition = [NSNumber numberWithBool:(![NSNumber numberWithInteger:self.arrowPosition].boolValue)].integerValue;
    
    [self.ft_tableView reloadData];
}

#pragma mark - FTFoldingTableViewDelegate / required

- (FTFoldingSectionHeaderArrowPosition)perferedArrowPositionForFTFoldingTableView:(FTFoldingTableView *)ftTableView
{
    return self.arrowPosition;
}
- (NSInteger )numberOfSectionForFTFoldingTableView:(FTFoldingTableView *)ftTableView
{
    return monthArray.count;
}

- (NSInteger )ftFoldingTableView:(FTFoldingTableView *)ftTableView numberOfRowsInSection:(NSInteger )section
{
    NSInteger count = 0;
    for(NSDictionary *dict in monthlyEarningArray){
        
        if([[monthArray objectAtIndex:section] isEqualToString:[dict valueForKey:@"month"]]){
            count ++;
        }
    }
    return count;
}
- (CGFloat )ftFoldingTableView:(FTFoldingTableView *)ftTableView heightForHeaderInSection:(NSInteger )section
{
    return 60;
}
- (CGFloat )ftFoldingTableView:(FTFoldingTableView *)ftTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (NSString *)ftFoldingTableView:(FTFoldingTableView *)ftTableView titleForHeaderInSection:(NSInteger)section
{
  return  [monthWithDict objectForKey:[monthArray objectAtIndex:section]];

}

- (UITableViewCell *)ftFoldingTableView:(FTFoldingTableView *)ftTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EarningTableViewCell *cell = (EarningTableViewCell*)[self.ft_tableView dequeueReusableCellWithIdentifier:DemoTableViewIdentifier forIndexPath:indexPath];
    
    monthlyEarningArrayForCellDisplay = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in monthlyEarningArray){
        
        if([[monthArray objectAtIndex:indexPath.section] isEqualToString:[dict valueForKey:@"month"]]){
            [monthlyEarningArrayForCellDisplay addObject:dict];
        }
    }
    NSDictionary *dictTest = [[monthlyEarningArrayForCellDisplay objectAtIndex:indexPath.row] mutableCopy];

        cell.moneyLabel.text = [dictTest objectForKey:@"date"];
        cell.countLabel.text =  [NSString stringWithFormat:@"$%.02f",[[dictTest objectForKey:@"amount"] floatValue]];
    
    return cell;
}
- (void )ftFoldingTableView:(FTFoldingTableView *)ftTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [ftTableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark - FTFoldingTableViewDelegate / optional delegates, change almost everything using delegate

- (void)ftFoldingTableView:(FTFoldingTableView *)ftTableView willChangeToSectionState:(FTFoldingSectionState)sectionState section:(NSInteger)section
{
    if( sectionState == FTFoldingSectionStateShow)
        [self getAmountForMont:[monthArray objectAtIndex:section] completion:^(BOOL success, NSDictionary *result, NSError *error) {
            if(result == nil){
                
                UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                [self presentViewController:controller animated:YES completion:nil];
            }
            if(error!=nil){
                
                UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:error.localizedDescription];
                [self presentViewController:controller animated:YES completion:nil];
            }
            if(success){
                
                //   make sure to reload in order to update the custom menu index path for each row
                NSMutableArray *rowsToReload = [NSMutableArray array];
                for (int i = 0; i < monthlyEarningArray.count; i++) {
                    [rowsToReload addObject:[NSIndexPath indexPathForRow:i inSection:section]];
                }
                [_ft_tableView reloadData];
            }
        }];
}

- (NSString *)ftFoldingTableView:(FTFoldingTableView *)ftTableView descriptionForHeaderInSection:(NSInteger )section
{
    NSString * amount = @"$0.0";
    for(NSDictionary *dict in earningYearArray ){
        if([[dict objectForKey:@"month"] isEqualToString:[monthArray objectAtIndex:section]]){
            
            amount =  [NSString stringWithFormat:@"$%.02f",[[dict objectForKey:@"amount"] floatValue]];//@"$%.02f"
            break;
        }else
            amount= @"$0.0";
    }
    return amount;
}

- (UIImage *)ftFoldingTableView:(FTFoldingTableView *)ftTableView arrowImageForSection:(NSInteger )section
{
    return [UIImage imageNamed:@"right_arrow"];
}
- (UIColor *)ftFoldingTableView:(FTFoldingTableView *)ftTableView backgroundColorForHeaderInSection:(NSInteger )section
{
    return [UIColor whiteColor];
}
- (UIFont *)ftFoldingTableView:(FTFoldingTableView *)ftTableView fontForTitleInSection:(NSInteger )section
{
    return [UIFont fontWithName:@"Lato-Regular" size:16];
}
- (UIFont *)ftFoldingTableView:(FTFoldingTableView *)ftTableView fontForDescriptionInSection:(NSInteger )section
{
    return [UIFont fontWithName:@"Lato-Regular" size:12];
}
- (UIColor *)ftFoldingTableView:(FTFoldingTableView *)ftTableView textColorForTitleInSection:(NSInteger )section
{
    return [UIColor blackColor];
}
- (UIColor *)ftFoldingTableView:(FTFoldingTableView *)ftTableView textColorForDescriptionInSection:(NSInteger )section
{

    return [UIColor blackColor];
}
-(void)menuButtonClicked{

    [self.swiftySideMenu toggleSideMenu];
    if(self.swiftySideMenu.isLeftMenuOpened){
        [self.view endEditing:true];
        self.view.userInteractionEnabled = false;
           [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewInteractionChanged) name:kUserUnteractionChangedNotification object:nil];
    }else{
        self.view.userInteractionEnabled = true;
        [[NSNotificationCenter defaultCenter] removeObserver:kUserUnteractionChangedNotification];
    }
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewInteractionChanged{
    self.view.userInteractionEnabled = true;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (tableView.tag == 5)
    {
        return yearArray.count;
    }else
     return 0;
}

static NSString *cellIdentifier = @"CellIdentifier";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @try
    {
        if (tableView.tag == 5)
        {
            cellIdentifier = [NSString stringWithFormat:@"Cell%@%li",indexPath,(long)(int)indexPath.row];
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = [NSString stringWithFormat:@"%@", [yearArray objectAtIndex:indexPath.row]];
            return cell;
        }
        
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exeption Description:%@",exception.description);
    }
}

-(NSString*)getFormatedDateStringFromDate:(NSDate*)date inFormat:(NSString*)strFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM"];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:strFormat];
    return [df stringFromDate:date];
}

-(NSString*)getFormatedDate:(NSString*)dateString :(NSString*)neededFormat{
    //2017-03-29 15:50:18"
    // 24 HR DATE FORMATTER (EVEN IF THE PHONE IS SET TO 12HR)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.000000"]; // e.g. "2015-09-01 23:30:00.000000"
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    // exact time
    NSDate* placeDate = [dateFormatter dateFromString:dateString];
    NSString *finaldateString = [self getFormatedDateStringFromDate:placeDate inFormat:neededFormat];
    return finaldateString;
    
}

-(void)getAmountForYear {
    
    earningYearArray = [[NSMutableArray alloc]init];
    monthlyEarningArray = [[NSMutableArray alloc]init];

        if([LSUtils isNetworkConnected])
    {
      
        [sharedUtils startAnimator:self ];
        
        NSURL *url = [NSURL URLWithString:Base_URL];//"2016-11-21
        NSString *param = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&ismonthly=2&earning_month=99&earning_year=%d",myearning,[UserManager getUserID],currentYear];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self ];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  if(responseDict == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {
                                                      
                                                      NSArray *ListOfMonthWithAmount = [responseDict objectForKey:@"data"];
                                                      for(int index = 0; index < ListOfMonthWithAmount.count; index++)
                                                      {
                                                          NSDictionary *dataDict =  [ListOfMonthWithAmount objectAtIndex:index];
                                                          NSString *monthName  = [self getFormatedDate:[dataDict objectForKey:@"created_on"] :@"MM"];
                                                          NSMutableDictionary *newDict = [dataDict mutableCopy];
                                                          [newDict setObject:monthName forKey:@"month"];
                                                          [earningYearArray addObject:newDict];

                                                      }
                                                      
                                                      NSSortDescriptor *sorter=[[NSSortDescriptor alloc]initWithKey:@"month" ascending:YES];
                                                       NSArray *shorteredArray = [earningYearArray sortedArrayUsingDescriptors:@[sorter]];
                                                      earningYearArray =[shorteredArray mutableCopy];

                                                      }
                                                  [_ft_tableView reloadData];
                                                  }
                                              else{
                                              }
                                            
                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)getAmountForMont:(NSString*)month completion:(void (^) (BOOL success, NSDictionary* result,NSError* error))completion {
    
    
    if([LSUtils isNetworkConnected])
    {
        [sharedUtils startAnimator:self ];
        
        NSURL *url = [NSURL URLWithString:Base_URL];
        NSString *param = [NSString stringWithFormat:@"%@&deliveryboy_id=%@&ismonthly=1&earning_month=%@&earning_year=%d",myearning,[UserManager getUserID],month,currentYear];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        request.HTTPMethod = @"POST";
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [sharedUtils stopAnimator:self ];
                                              if (data.length > 0 && error == nil)
                                              {
                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:NULL];
                                                  
                                                  if(responseDict == nil){
                                                      UIViewController*   controller =   [sharedUtils showAlert:@"Sorry" withMessage:@"No response, Please try again."];
                                                      [self presentViewController:controller animated:YES completion:nil];
                                                  }
                                                  if ([[responseDict objectForKey:@"code"]isEqualToString:@"200"]) {

                                                          NSArray *ListOfDateWithAmount = [responseDict objectForKey:@"data"];
                                                          for(int index = 0; index < ListOfDateWithAmount.count; index++)
                                                          {
                                                              NSDictionary *dataDict =  [ListOfDateWithAmount objectAtIndex:index];
                                                              NSString *dateOfearn  = [self getFormatedDate:[dataDict objectForKey:@"created_on"] :@"dd"];
                                                              NSString *monthOfearn  = [self getFormatedDate:[dataDict objectForKey:@"created_on"] :@"MM"];
                                                              NSMutableDictionary *newDict = [dataDict mutableCopy];
                                                              [newDict setObject:dateOfearn forKey:@"date"];
                                                              [newDict setObject:monthOfearn forKey:@"month"];
                                                              [monthlyEarningArray addObject:newDict];
                                                              
                                                          }
                                                          
                                                          NSSortDescriptor *sorter=[[NSSortDescriptor alloc]initWithKey:@"month" ascending:YES];
                                                          NSArray *shorteredArray = [monthlyEarningArray sortedArrayUsingDescriptors:@[sorter]];
                                                      
                                                      //To remove duplicate datas from array
                                                      NSOrderedSet *uniqueMakes = [[NSOrderedSet alloc] initWithArray:shorteredArray];
                                                      monthlyEarningArray = [[NSMutableArray alloc] initWithArray:[uniqueMakes array]];
                                                      
                                                      completion(true, responseDict, error);

                                                  }
                                              }
                                              else if(error != nil){
                                                  
                                                  completion(true, nil, error);
                                              }else
                                                  completion(false, nil, nil);
                                              
                                          });
                                      }];
        [task resume];
    }else{
        
        UIAlertController *controller = [sharedUtils showAlert:@"No network" withMessage:@"Please check your internet connection."];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void )tableView:(UITableView *)ftTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_yearListPopoverView.isPopoverVisible) {
        
        [_yearListPopoverView dismissPopoverAnimated:true];
        _yearListPopoverView.delegate = nil;
    }
    currentYear = [[yearArray objectAtIndex:indexPath.row] intValue];
    _yearlabel.text = [NSString stringWithFormat:@"%d", currentYear];
    [self getAmountForYear];
}

//Return the selected year indx to display it at middle of tableview
-(int)getCurrentYearIndex:(int)dt{
    
    int newindex = 0;
    if([yearArray containsObject:[NSNumber numberWithInt:dt]]){
        return (int) [yearArray indexOfObject:[NSNumber numberWithInt:dt]];
    }
    return newindex;
}
    
@end

