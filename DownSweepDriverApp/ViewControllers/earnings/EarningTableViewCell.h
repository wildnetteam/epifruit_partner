//
//  EarningTableViewCell.h
//  DownSweepDriverApp
//
//  Created by Arpana on 01/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EarningTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@end
