//
//  EarningListViewController.h
//  DownSweepDriverApp
//
//  Created by Arpana on 01/04/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationManager.h"
#import "FTFoldingTableView.h"
#import "WYPopoverController.h"

@interface EarningListViewController : UIViewController <WYPopoverControllerDelegate , UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) WYPopoverController *yearListPopoverView;
@property (weak, nonatomic) IBOutlet UILabel *yearlabel;

@end
