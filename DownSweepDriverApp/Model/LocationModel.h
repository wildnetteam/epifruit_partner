//
//  LocationModel.h
//  DownSweepDriverApp
//
//  Created by Arpana on 17/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationModel : NSObject

@property (nonatomic , strong) NSString* userToken;
@property (nonatomic , strong) NSString* name;
@property (nonatomic , strong) NSString* email;
@property (nonatomic , strong) NSNumber* currentLat;
@property (nonatomic , strong) NSNumber* currentLog;
-(id)initWithUserId:(NSString*)uid :(NSString*)uame :(NSString*)ueamil :(NSNumber*)cLat :(NSNumber*)clong;
@end
