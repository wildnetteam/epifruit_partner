//
//  LocationModel.m
//  DownSweepDriverApp
//
//  Created by Arpana on 17/03/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "LocationModel.h"

@implementation LocationModel
-(id)initWithUserId:(NSString*)uid :(NSString*)uame :(NSString*)ueamil :(NSNumber*)cLat :(NSNumber*)clong{
    self = [super init];
    if(self){
    _userToken =uid;
    _name =uame;
    _email = ueamil;
    _currentLat = cLat;
    _currentLog = clong;
    }
    return self;
}

@end
