//
//  TransportModel.h
//  DownSweepDriverApp
//
//  Created by Arpana on 15/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransportModel : NSObject
@property(nonatomic ,retain)NSString* tMode;
@property(nonatomic ,retain)NSString* tId;
@property(nonatomic ,retain)NSString* tlecense;

@property(nonatomic)BOOL licenseneeded;


-(id)initWithModeName:(NSString*)mode ModeID:(NSString*)modeId :(BOOL)isLicenseRequired;
@end
