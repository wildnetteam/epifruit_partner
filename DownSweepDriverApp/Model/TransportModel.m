//
//  TransportModel.m
//  DownSweepDriverApp
//
//  Created by Arpana on 15/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import "TransportModel.h"

@implementation TransportModel

-(id)initWithModeName:(NSString*)mode ModeID:(NSString*)modeId :(BOOL)isLicenseRequired{
    
    self =[super init];
    
    if(self){
        
        _tMode = mode;
        _tId = modeId;
        _licenseneeded =isLicenseRequired;
        return self;
    }
    return self;
}
@end
