//
//  Orderhistory.h
//  DownSweepDriverApp
//
//  Created by Arpana on 14/02/17.
//  Copyright © 2017 Wildnet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Orderhistory : NSObject
typedef enum {
    Accepted = 1,
    Rejected,
    Ongoing,
    Completed
}StatusType;

@property (nonatomic, retain) NSString *orderNumber;
@property (nonatomic) int orderStatus;
@property (nonatomic, retain) NSString *pickupTime;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *modeOfTransport;
@property (nonatomic, retain) NSString *productName;
@property (nonatomic, retain) NSString *productDesc;
@property (nonatomic, retain) NSString *pickupAddress;
@property (nonatomic, retain) NSString *delAddress;
@property (nonatomic, retain) NSDictionary* pickUpCordinate;
@property (nonatomic, retain) NSDictionary* dropOffCordinate;
@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *productDistance;
//For bidding
@property (nonatomic, retain) NSString *productBidId;
@property (nonatomic) int imageCount;
@property (nonatomic, retain) NSString* randomImageString;
@property (nonatomic, retain)NSString* imageBaseURL;
@property (nonatomic, retain) NSString* minRate;
@property (nonatomic, retain) NSString* maxrate;
@property (nonatomic, retain) NSString* proposalCount;
@property (nonatomic) int driverCount;
////////////////////////
@property (nonatomic) int jumpPrice;

@property (nonatomic) NSMutableArray* productImageArray;

@property (nonatomic ) StatusType orderStatusType;








@end
