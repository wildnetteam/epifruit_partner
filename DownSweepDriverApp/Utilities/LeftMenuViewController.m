//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Arpana on 07/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "LoginViewController.h"
#import "sideMenuCell.h"
#import "PrivaceViewController.h"
#import "AboutUsViewController.h"

@implementation LeftMenuViewController

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = NO;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftMenu.jpg"]];
    
    self.tableView.backgroundView = imageView;
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    currentlyExpanded = true;
    UINib *nib = [UINib nibWithNibName:@"sideMenuCell" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:@"sideMenuCell"];
    
      sharedUtils.mainMenu = [[NSArray alloc]initWithObjects:@"View jobs",@"View current job", @"View All Orders", @"Payment Details", @"Earnings",@"My Rewards", @"My profile", @"Help & Faq",@"Contact us",@"Logout",nil];
    
      _menuSubItem = [[NSArray alloc]initWithObjects:@"Legal" , @"        Privacy policies", @"        Terms & Conditions",nil];
    

   [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                     FONT_NavigationBar, NSFontAttributeName, nil]];
}

#pragma -mark TableViewdelegates/Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE5){
        return 40;
    }else return 49;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return _menuSubItem.count;
        }
        return 1;
    }
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    
    if (section == 7)
        return YES;
    
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sharedUtils.mainMenu.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _cell =  [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
    
    _cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (currentlyExpanded) {
            _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
            _cell.imageWidth.constant = 12;
            _cell.imageHeight.constant = 9;
        }
        else
        {
            _cell.arrowImage.image = [UIImage imageNamed:@"right_arrow"];
            _cell.imageWidth.constant = 9;
            _cell.imageHeight.constant = 12;
            
        }
        
        if (indexPath.row)
        {
            
            [_cell.arrowImage removeFromSuperview];
        }
        
        _cell.titleLabel.text = [self.menuSubItem objectAtIndex:indexPath.row];
    }
    else{
        _cell.titleLabel.text = [sharedUtils.mainMenu objectAtIndex:indexPath.section];
        _cell.arrowImage.hidden = true;
    }
    _cell.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:16];
    _cell.titleLabel.textColor= [UIColor blackColor];
    _cell.backgroundColor = [UIColor clearColor];
    return _cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *vc ;
    
  AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    vc= nil;
    
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            NSInteger section = indexPath.section;
            currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                
                rows = [self tableView:tableView numberOfRowsInSection:section];
                _cell.arrowImage.image = [UIImage imageNamed:@"right_arrow"];
                [expandedSections removeIndex:section];
            }
            else
            {
                
                [expandedSections addIndex:section];
                _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            if (currentlyExpanded)
            {
                 _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationFade];
                NSArray *index = [[NSArray alloc]initWithObjects:indexPath, nil];
                [tableView reloadRowsAtIndexPaths:index withRowAnimation:UITableViewRowAnimationFade];
                
            }
            else
            {
                _cell.arrowImage.image = [UIImage imageNamed:@"down_arrow"];
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationFade];
                NSArray *index = [[NSArray alloc]initWithObjects:indexPath, nil];
                [tableView reloadRowsAtIndexPaths:index withRowAnimation:UITableViewRowAnimationFade];

            }
        }else{
            
            if (indexPath.row == 1) {
                
                vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"PrivaceViewController"];
            }else  if (indexPath.row == 2){
                vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"AboutUsViewController"];
            }
        }
    }

    switch (indexPath.section)
    {
        case 0:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"MyProposalViewController"];
        }
            break;
            
        case 1:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
        }
            break;
            
        case 2:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"CompletedListViewController"];
        }
            break;
            
//        case 3:{
//             vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"DeclineOrdersViewController"];
//         }
//            break;
            
        case 3:{
            
            NSString* controllerName = sharedUtils.bankDetailDictionary == nil?@"AddAccountViewController":@"BankDetailsViewController";
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                  controllerName];
        }
            break;
            
        case 4:{
             vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"EarningListViewController"];
        }
            break;
        case 5:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"MyRewardListViewController"];
        }
            break;
        case 6:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier: @"SettingsViewController"];
        }
            break;
        case 8:{
            vc = [sharedUtils.mainStoryboard instantiateViewControllerWithIdentifier:
                  @"ContactUSViewController"];
            
        }
            break;
        case 9: {
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Logout"
                                         message:@"Are you sure, you want to log out?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"No"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"ISLOGGEDIN"];
                                            [self deauthToFirebase:[UserManager getUserID]];
                                            [UserManager removeUserCredential];
                                            LoginViewController *login = (LoginViewController*)[sharedUtils.mainStoryboard
                                                                                                instantiateViewControllerWithIdentifier: @"LoginViewController"];
                                          //  appDelegate.navigationController.viewControllers = NULL;
                                            [ appDelegate.navigationController pushViewController:login animated:YES];
                                            self.swiftySideMenu.centerViewController = appDelegate.navigationController;
                                            [self.swiftySideMenu toggleSideMenu];
                                            
                                        }];
            
            [alert addAction:noButton];
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
            break;
    }
    if(vc!=nil) {
        
        [ appDelegate.navigationController pushViewController:vc animated:YES];
        self.swiftySideMenu.centerViewController = appDelegate.navigationController;
        [self.swiftySideMenu toggleSideMenu];
    }
    
}
// notify firebase that user has logged out
- (void)deauthToFirebase :(NSString*)userId
{
    AppDelegate *del = (AppDelegate *) [[UIApplication sharedApplication] delegate];

      FIRDatabaseReference  *newref = [del.rootRef child:[UserManager getUserID]];
    [newref removeValue];
//    [[del.rootRef child:userId] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        
//        [snapshot.ref removeValue];
//        
//    } withCancelBlock:^(NSError * _Nonnull error) {
//        NSLog(@"%@", error.localizedDescription);
//    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)crossButtonTapped:(id)sender {
    
    [self.swiftySideMenu toggleSideMenu ];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserUnteractionChangedNotification object:nil];
    [self preferredStatusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}



@end
