//
//  Enum.h
//  PODApplication
//
//  Created by Arpana on 08/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#ifndef Enum_h
#define Enum_h

typedef enum _ControllerName
{
    DASHBOARD = 0,
    MYHISTORY = 1,
    MANAGEADDRESS = 3,
    FAQUES = 4,
    LOGOUT = 5
} ControllerName;

typedef NS_ENUM(NSUInteger, NotificationType) {
    Positive,
    Negative,
    Normal,
    Loading
};

#endif /* Enum_h */
