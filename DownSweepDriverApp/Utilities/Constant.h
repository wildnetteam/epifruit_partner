//
//  Created by Arpana on 09/11/16.
//  Copyright © 2016 Arpana. All rights reserved.
//

#ifndef Constant_h
#define Constant_h
#define directoryImage @"ImageReferenceDirectory"

#define kUserUnteractionChangedNotification  @"kUserUnteractionChangedNotification"

#define Base_URL         @"http://site4demo.com/deliveryapp/deliveryboyapi/"

#define  SignUp_Service          @"methodname=register&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  Login_Service           @"methodname=logindeliveryboy&token=ecbcd7eaee29848978134beeecdfbc7c"

#define ForgotPassword_Service @"methodname=forgotPassword&token=ecbcd7eaee29848978134beeecdfbc7c"

#define RemovePickUpAddress_Service @"methodname=removepickupaddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define GetPickUpAddress_Service  @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getpickupaddresslist"

#define AddPicUpAddress_Service     @"methodname=addpickupaddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  UpdatePicUpAddress_Service @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=updatepickupaddresslist"

#define GetConsumerAddress_Service  @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=listconsumeraddress"

#define AddConsumerAddress_Service     @"methodname=saveconsumeraddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define  UpdateConsumerAddress_Service @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=updateconsumeraddress"

#define getUserPoints @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getUserPoints"

#define RemoveConsumerAddress_Service @"methodname=removeconsumeraddress&token=ecbcd7eaee29848978134beeecdfbc7c"

#define GetCardDetail_Service @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getpaymentcardinfo"

#define withdrawBid @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=withdrawBid"

#define PostCode_Validation_Link  @"http://maps.googleapis.com/maps/api/geocode/json?"

#define GetModeOfTransport @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=deliveryboytranspotationmode"
#define GettransMode @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=transMode"
#define driverBid @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=driverBid"
#define SaveModeOfTransport @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=savetransportmode"
#define GetCompletedOrders @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getcompletedorder"
//#define GetCurrentDaydOrders @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=myOrderList"
#define newJobList @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=newJobList"
#define myProposalList @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=myProposalList"
#define GetTodaysOrdercount @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getallorder"
#define GetRejectedDaydOrders @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getrejectedorder"
#define GetCompletdOrders @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=myOrderList"
#define UpdateProfile @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=updatedelieryboyprofile"
#define UpdatePassword @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=chengepassword"
#define contactus @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=contactus"

#define pickupOrder @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=pickupOrder"
#define dropoffOrder @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=dropoffOrder"
#define dropoffesignature @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=savecustomersignature"
#define getdeliveryboyorderstatus @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getOrderStatus"

#define myearning @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=myearning"
#define getdeliveryboyprofiledetail @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getdeliveryboyprofiledetail"
#define setnotificationstatus @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=setnotificationstatus"

#define createsubmerchant @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=createsubmerchant"
#define updatesubmerchant @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=updatesubmerchant"
#define getdeliveryboyaccountdetail @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=getdeliveryboyaccountdetail"
#define PushToken  [NSString stringWithFormat:@"PushToken"]
#define validateRefrenceCode  @"token=ecbcd7eaee29848978134beeecdfbc7c&methodname=validateRefrenceCode"

#define kJTCalendarDaySelected @"kJTCalendarDaySelected"

// Constants for User Login Credentials Info.
#define K_USERCREDENTIAL   [NSString stringWithFormat:@"USER_CREDENTIALS"]
#define k_NotificationCount       [NSString stringWithFormat:@"NotificationCount"]
#define k_UserName         [NSString stringWithFormat:@"UserName"]
#define k_FirstName         [NSString stringWithFormat:@"FirstName"]
#define k_LastName         [NSString stringWithFormat:@"LastName"]
#define k_LoggedInUserID   [NSString stringWithFormat:@"id"] //LoggedInUserID
#define k_LiscenseNo        [NSString stringWithFormat:@"Liscence"]
#define k_LoginTokenID     [NSString stringWithFormat:@"LoginTokenID"]
#define k_UserImageURL     [NSString stringWithFormat:@"userImageURL"]
#define k_UserEmail        [NSString stringWithFormat:@"UserEmail"]
#define k_UserPhoneNumber  [NSString stringWithFormat:@"UserPhoneNumber"]
#define k_UserReferenceCode  [NSString stringWithFormat:@"UserReferenceCode"]

#define AppDefaultBlueColor [UIColor colorWithRed:119/255.0f green:209/255.0f blue:226/255.0f alpha:1]
//#define k_Token            [NSString stringWithFormat:@"UserEmail"]
#define itunesMapLink  @"https://itunes.apple.com/us/app/google-maps-navigation-transit/id585027354?mt=8"
#define privacyLink  @"http://site4demo.com/deliveryapp/terms-and-conditions"
#define FAQLink  @"http://site4demo.com/deliveryapp/FAQs"
#define TermsAndConditionLink  @"http://site4demo.com/deliveryapp/about-us"

#define  NSdidGetUnReadNotificationCount @"didGetUnReadNotificationCount"
#define  NSdidRecieveNotification        @"didRecieveNotification"

//------- Macro's - Check String -------
#define k_EmptyLength      0
#define k_EmptyString      @""
#define ProfileImageResizeScale CGSizeMake(250, 250)

#define HEIGHT_NAVIGATIONBAR 64.0f
#define SHOWING_YEAR_MONTH_DATE_SERVER_FORMAT               @"yyyy-MM-dd HH:mm:ss" // 2015-06-03 for 24 hour format
#define SHOW_YEAR_MONTH_DATE_FORMAT                         @"yyyy-MM-dd" // "2015-06-03"
#define SHOWING_HOURS_MINUTES_TIME_FORMAT                   @"HH:mm" // for 24 hour format
#define DATE_TIME_ZONE_FORMAT                               @"yyyy-MM-dd HH:mm:ss.000000" // "2015-06-03 13:31:56.000000"
#define DATE_TIME_ZONE_FORMAT_LOCAL                         @"yyyy-MM-dd HH:mm:ss +0000" //// "2015-06-03 13:31:56 +0000
#define SHOWING_MONTH_AND_DATE_FORMAT                       @"dd MMM"

// Constants for URL schemes and arguments defined by Google Maps and Chrome.
static NSString * const kGoogleMapsScheme = @"comgooglemaps://";
static NSString * const kGoogleMapsCallbackScheme = @"comgooglemaps-x-callback://";

typedef void (^completionHandler)(); // A completion handler that returns nothing

#define  k_LOCATIONSERVICEENABLED

//Constant for Colors
#define APP_BACKGROUND_COLOR [UIColor whiteColor]
//[UIColor colorWithRed:236/255.0f green:236/255.0f blue:236/255.0f alpha:1.0f]

#define REGISTERBUTTON_BACKGROUND_COLOR [UIColor colorWithRed:63/255.0f green:203/255.0f blue:228/255.0f alpha:1.0f]

#define IS_IPHONE5     (([[UIScreen mainScreen] bounds].size.height-568)?NO:true)
#define IS_IPHONE_6 (([[UIScreen mainScreen] bounds].size.height-667.0)?NO:true)
#define IS_IPHONE_6P (([[UIScreen mainScreen] bounds].size.height-736.0)?NO:true)

#define IS_OS_6_OR_LATER    (([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)?true:false)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_9_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)

#define LEFTMENU_BACKGROUND_COLOR [UIColor colorWithRed:159/255.0f green:159/255.0f blue:159/255.0f alpha:1.0f]

#define  NAVIGATIONBAR_COLOR [UIColor whiteColor]
//[UIColor colorWithRed:223/255.0f green:223/255.0f blue:223/255.0f alpha:1.0f]
#define ColourConstants_LightBackground  [UIColor colorWithHex:0xf7faff] // The default background colour, blueish white



#define FONT_Label_PlaceHolder  [UIFont fontWithName:@"Lato-Light" size:6];
#define FONT_TextField [UIFont fontWithName:@"Lato-Regular" size:16];
#define FONT_Title_Label [UIFont fontWithName:@"Lato-Regular" size:17];

#define FONT_TextField_6P [UIFont fontWithName:@"Lato-Light" size:17];
#define FONT_Title_Label_6P [UIFont fontWithName:@"Lato-Regular" size:19];


#define COLOR_Title_Label [UIColor colorWithHexString:@"#7D7D7D"]
#define COLOR_Border [UIColor colorWithHexString:@"#C4C4C4"]

#define FONT_NavigationBar                   [UIFont systemFontOfSize:16.0f]
#define FONT_TOP_HEADING                     [UIFont boldSystemFontOfSize:16.0f]
#define FONT_LABEL_REGULAR                   [UIFont systemFontOfSize:15.0f]
#define FONT_LABEL_BOLD                       [UIFont boldSystemFontOfSize:15.0f]


#define COLOUR_HELPTEXT_GREEN                       [UIColor colorWithRed:(float)5/255.0f green:(float)152/255.0f blue:(float)17/255.0f alpha:1.0f]

#define  getNotificationList_Service   @"methodname=getdeliveryboynoification&token=ecbcd7eaee29848978134beeecdfbc7c"
#define  getNotificationCount_Service  @"methodname=unreadnotificationcount&token=ecbcd7eaee29848978134beeecdfbc7c"
#define  setReadNotification_Service @"methodname=setnotificationread&token=ecbcd7eaee29848978134beeecdfbc7c"
#define  NSdidGetUnReadNotificationCount @"didGetUnReadNotificationCount"
#define  NSdidRecieveNotification        @"didRecieveNotification"

#endif /* Constant_h */



